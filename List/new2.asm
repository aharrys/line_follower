
;CodeVisionAVR C Compiler V2.05.0 Professional
;(C) Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega32L
;Program type             : Application
;Clock frequency          : 12,000000 MHz
;Memory model             : Small
;Optimize for             : Size
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 512 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : Yes
;'char' is unsigned       : Yes
;8 bit enums              : Yes
;global 'const' stored in FLASH: No
;Enhanced core instructions    : On
;Smart register allocation     : On
;Automatic register allocation : On

	#pragma AVRPART ADMIN PART_NAME ATmega32L
	#pragma AVRPART MEMORY PROG_FLASH 32768
	#pragma AVRPART MEMORY EEPROM 1024
	#pragma AVRPART MEMORY INT_SRAM SIZE 2143
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x60

	#define CALL_SUPPORTED 1

	.LISTMAC
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCR=0x35
	.EQU GICR=0x3B
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0060
	.EQU __SRAM_END=0x085F
	.EQU __DSTACK_SIZE=0x0200
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	CALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	CALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	CALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	CALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _VARKANAN=R4
	.DEF _VARKIRI=R6
	.DEF _x=R8
	.DEF _counter=R10
	.DEF _terus=R12

	.CSEG
	.ORG 0x00

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	JMP  __RESET
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00
	JMP  0x00

_char0:
	.DB  0x60,0x18,0x6,0x7F,0x7F,0x6,0x18,0x60
_tbl10_G100:
	.DB  0x10,0x27,0xE8,0x3,0x64,0x0,0xA,0x0
	.DB  0x1,0x0
_tbl16_G100:
	.DB  0x0,0x10,0x0,0x1,0x10,0x0,0x1,0x0

_0x6:
	.DB  0x64
_0x7:
	.DB  0x28
_0x26D:
	.DB  0x0,0x0,0x0,0x0
_0x0:
	.DB  0x3E,0x0,0x25,0x64,0x20,0x20,0x0,0x61
	.DB  0x6B,0x73,0x69,0x3D,0x0,0x53,0x43,0x4E
	.DB  0x20,0x6B,0x65,0x20,0x0,0x4A,0x75,0x6D
	.DB  0x6C,0x61,0x68,0x20,0x3D,0x20,0x0,0x41
	.DB  0x52,0x45,0x41,0x20,0x3D,0x0,0x31,0x0
	.DB  0x32,0x0,0x33,0x0,0x34,0x0,0x4D,0x4F
	.DB  0x44,0x45,0x20,0x3D,0x0,0x41,0x0,0x42
	.DB  0x0,0x43,0x0,0x44,0x0,0x48,0x0,0x50
	.DB  0x0,0x44,0x65,0x66,0x3A,0x0,0x20,0x4B
	.DB  0x70,0x3A,0x20,0x4B,0x69,0x3A,0x20,0x4B
	.DB  0x64,0x3A,0x0,0x20,0x41,0x44,0x43,0x20
	.DB  0x57,0x52,0x4E,0x20,0x4D,0x41,0x58,0x20
	.DB  0x4D,0x49,0x4E,0x0,0x7C,0x7C,0x7C,0x0
	.DB  0x44,0x20,0x45,0x20,0x4D,0x20,0x45,0x20
	.DB  0x4E,0x20,0x54,0x20,0x4F,0x20,0x52,0x0
	.DB  0x20,0x44,0x45,0x42,0x20,0x50,0x49,0x44
	.DB  0x20,0x58,0x20,0x50,0x4C,0x41,0x4E,0x0
	.DB  0x20,0x50,0x61,0x72,0x74,0x20,0x4D,0x6F
	.DB  0x64,0x65,0x20,0x41,0x72,0x65,0x61,0x20
	.DB  0x0,0x53,0x74,0x61,0x74,0x75,0x73,0x3A
	.DB  0x0,0x2D,0x0,0x20,0x43,0x4F,0x4E,0x46
	.DB  0x49,0x47,0x20,0x7C,0x20,0x53,0x54,0x41
	.DB  0x52,0x54,0x0,0x4C,0x3A,0x31,0x0,0x4C
	.DB  0x3A,0x30,0x0,0x52,0x3A,0x31,0x0,0x52
	.DB  0x3A,0x30,0x0,0x4C,0x3A,0x25,0x64,0x20
	.DB  0x20,0x0,0x52,0x3A,0x25,0x64,0x20,0x20
	.DB  0x0,0x7A,0x25,0x64,0x20,0x20,0x0,0x20
	.DB  0x41,0x57,0x41,0x4C,0x20,0x41,0x4B,0x48
	.DB  0x49,0x52,0x20,0x53,0x4B,0x45,0x4E,0x0
	.DB  0x41,0x6C,0x6C,0x61,0x68,0x75,0x61,0x6B
	.DB  0x62,0x61,0x72,0x2E,0x2E,0x2E,0x0,0x42
	.DB  0x69,0x73,0x6D,0x69,0x6C,0x6C,0x61,0x68
	.DB  0x0
_0x2020060:
	.DB  0x1
_0x2020000:
	.DB  0x2D,0x4E,0x41,0x4E,0x0,0x49,0x4E,0x46
	.DB  0x0
_0x2040003:
	.DB  0x80,0xC0

__GLOBAL_INI_TBL:
	.DW  0x01
	.DW  _deledele
	.DW  _0x6*2

	.DW  0x04
	.DW  0x08
	.DW  _0x26D*2

	.DW  0x01
	.DW  __seed_G101
	.DW  _0x2020060*2

	.DW  0x02
	.DW  __base_y_G102
	.DW  _0x2040003*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  GICR,R31
	OUT  GICR,R30
	OUT  MCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;CLEAR R2-R14
	LDI  R24,(14-2)+1
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(__CLEAR_SRAM_SIZE)
	LDI  R25,HIGH(__CLEAR_SRAM_SIZE)
	LDI  R26,__SRAM_START
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	JMP  _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x260

	.CSEG
;/*****************************************************
;This program was produced by the
;CodeWizardAVR V2.03.9 Standard
;Automatic Program Generator
;� Copyright 1998-2008 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com
;
;Project :
;Version :
;Date    : 28/11/2011
;Author  : Akhmad Harry Susanto
;Company : Unila Robotika Dan Otomasi
;Comments:
;
;
;Chip type               : ATmega32
;Program type            : Application
;AVR Core Clock frequency: 12,000000 MHz
;Memory model            : Small
;External RAM size       : 0
;Data Stack size         : 512
;*****************************************************/
;
;#include <mega32.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include <stdio.h>
;#include <stdlib.h>
;#include <delay.h>
;// Alphanumeric LCD Module functions
;#asm
   .equ __lcd_port=0x15 ;PORTC
; 0000 001F #endasm
;#include <lcd.h>
;
;//---------GAMBAR PANAH------------------------//
;typedef unsigned char byte;
;flash byte char0[8]={
;0b1100000,
;0b0011000,
;0b0000110,
;0b1111111,
;0b1111111,
;0b0000110,
;0b0011000,
;0b1100000};
;
;void define_char(byte flash *pc,byte char_code)
; 0000 002F {

	.CSEG
_define_char:
; 0000 0030 byte i,a;
; 0000 0031 a=(char_code<<3) | 0x40;
	ST   -Y,R17
	ST   -Y,R16
;	*pc -> Y+3
;	char_code -> Y+2
;	i -> R17
;	a -> R16
	LDD  R30,Y+2
	LSL  R30
	LSL  R30
	LSL  R30
	ORI  R30,0x40
	MOV  R16,R30
; 0000 0032 for (i=0; i<8; i++) lcd_write_byte(a++,*pc++);
	LDI  R17,LOW(0)
_0x4:
	CPI  R17,8
	BRSH _0x5
	ST   -Y,R16
	INC  R16
	LDD  R30,Y+4
	LDD  R31,Y+4+1
	ADIW R30,1
	STD  Y+4,R30
	STD  Y+4+1,R31
	SBIW R30,1
	LPM  R30,Z
	ST   -Y,R30
	CALL _lcd_write_byte
	SUBI R17,-1
	RJMP _0x4
_0x5:
; 0000 0033 }
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,5
	RET
;//-----------------------------------------------//
;//---------------Debug switch---------------------------//
;#define sw_ok     PINB.0
;#define sw_cancel PINB.4
;#define sw_kiri   PINB.1
;#define sw_kanan  PINB.3
;#define sw_start  PINB.2
;
;//-----------------port motor----------------------------//
;#define kiri    PORTD.3
;#define kanan   PORTD.6
;
;//------------------enable motor-------------------------//
;#define PWMKIRI       OCR1A
;#define PWMKANAN      OCR1B
;
;//-------------sensor tambahan---------------------------//
;
;#define maju    0
;#define mundur  1
;//-------------------------------------------------------//
;
;
;
;void menu();
;void trace_garis();
;unsigned int lihat_adc();
;
;
;
;
;//-----------------Variabel Global----------------------------------//
;
;eeprom unsigned char Kp = 10;
;eeprom unsigned char z = 9;
;eeprom unsigned char Ki = 0;
;eeprom unsigned char Kd = 30;
;eeprom unsigned int  batas_sensor=120;
;//eeprom unsigned int  X=4;
;eeprom unsigned int  ADC0=120;
;eeprom unsigned int  ADC1=120;
;eeprom unsigned int  ADC2=120;
;eeprom unsigned int  ADC3=120;
;eeprom unsigned int  ADC4=120;
;eeprom unsigned int  ADC5=120;
;eeprom unsigned int  ADC6=120;
;eeprom unsigned int  ADC7=120;
;eeprom int MAXPWM  = 230;
;eeprom int MINPWM = 0;
;//eeprom int skenario=1;
;eeprom int warna=0; //0:hitam; 1:putih;
;eeprom unsigned char arr[150];
;
;
;signed int VARKANAN;
;signed int VARKIRI;
;int x=0b00000000;
;int counter=0,terus;
;int intervalPWM;
;int deledele=100;

	.DSEG
;int start=0;
;unsigned char buf[33];
;unsigned char dtadc;
;int kondisidahdiset=0;
;char countZ;
;unsigned char kondisiZ = 0;
;byte PWMZ = 40;
;int DCPWM;
;
;//-------------------------ADC GENERATOR-------------------------------//
;
;#define ADC_VREF_TYPE 0x60
;// Read the 8 most significant bits
;// of the AD conversion result
;unsigned char read_adc(unsigned char adc_input)
; 0000 007F {

	.CSEG
_read_adc:
; 0000 0080 ADMUX=adc_input | (ADC_VREF_TYPE & 0xff);
;	adc_input -> Y+0
	LD   R30,Y
	ORI  R30,LOW(0x60)
	OUT  0x7,R30
; 0000 0081 // Delay needed for the stabilization of the ADC input voltage
; 0000 0082 delay_us(10);
	__DELAY_USB 40
; 0000 0083 // Start the AD conversion
; 0000 0084 ADCSRA|=0x40;
	SBI  0x6,6
; 0000 0085 // Wait for the AD conversion to complete
; 0000 0086 while ((ADCSRA & 0x10)==0);
_0x8:
	SBIS 0x6,4
	RJMP _0x8
; 0000 0087 ADCSRA|=0x10;
	SBI  0x6,4
; 0000 0088 return ADCH;
	IN   R30,0x5
	RJMP _0x20C0008
; 0000 0089 }
;
;//----------------MOTOR DIRECTION-------------------------------------//
;//--------ma=motor atas(kemudi) mb=motor bawah------------------------//
;
;    void motor(char xka,char xki,unsigned char Vkanan,unsigned char vkiri)
; 0000 008F     {
_motor:
; 0000 0090      kanan=xka;
;	xka -> Y+3
;	xki -> Y+2
;	Vkanan -> Y+1
;	vkiri -> Y+0
	LDD  R30,Y+3
	CPI  R30,0
	BRNE _0xB
	CBI  0x12,6
	RJMP _0xC
_0xB:
	SBI  0x12,6
_0xC:
; 0000 0091      kiri=xki;
	LDD  R30,Y+2
	CPI  R30,0
	BRNE _0xD
	CBI  0x12,3
	RJMP _0xE
_0xD:
	SBI  0x12,3
_0xE:
; 0000 0092      PWMKANAN=Vkanan;
	LDD  R30,Y+1
	LDI  R31,0
	OUT  0x28+1,R31
	OUT  0x28,R30
; 0000 0093      PWMKIRI=vkiri;
	CALL SUBOPT_0x0
	OUT  0x2A+1,R31
	OUT  0x2A,R30
; 0000 0094 
; 0000 0095     }
	ADIW R28,4
	RET
;
;
;
;//------------------------M.E.N.U------------------------------//
;//-------------------------------------------------------------//
;//-----------------SETTING VARIABEL--------------------//
;
;
;  void set_def()
; 0000 009F   {
_set_def:
; 0000 00A0       lcd_gotoxy(8,0);
	CALL SUBOPT_0x1
; 0000 00A1       lcd_putsf(">");
	CALL SUBOPT_0x2
; 0000 00A2       dtadc=batas_sensor;
	CALL SUBOPT_0x3
; 0000 00A3 
; 0000 00A4       lcd_gotoxy(13,0);
	CALL SUBOPT_0x4
; 0000 00A5       sprintf(buf,"%d  ",dtadc);
; 0000 00A6       lcd_puts(buf);
; 0000 00A7          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0xF
; 0000 00A8          {
; 0000 00A9               delay_ms(deledele);
	CALL SUBOPT_0x5
; 0000 00AA               batas_sensor--;
	CALL SUBOPT_0x6
; 0000 00AB          }
; 0000 00AC          if(!sw_kanan)
_0xF:
	SBIC 0x16,3
	RJMP _0x10
; 0000 00AD          {
; 0000 00AE               delay_ms(deledele);
	CALL SUBOPT_0x5
; 0000 00AF               batas_sensor++;
	CALL SUBOPT_0x7
; 0000 00B0          }
; 0000 00B1   }
_0x10:
	RET
;   void set_ADC0()
; 0000 00B3   {
_set_ADC0:
; 0000 00B4       lcd_gotoxy(0,1);
	CALL SUBOPT_0x8
; 0000 00B5       lcd_putsf(">");
; 0000 00B6       dtadc=ADC0;
	CALL SUBOPT_0x9
; 0000 00B7 
; 0000 00B8       lcd_gotoxy(1,1);
	CALL SUBOPT_0xA
; 0000 00B9       sprintf(buf,"%d  ",dtadc);
; 0000 00BA       lcd_puts(buf);
; 0000 00BB          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x11
; 0000 00BC          {
; 0000 00BD               delay_ms(deledele);
	CALL SUBOPT_0xB
; 0000 00BE               ADC0--;
	CALL SUBOPT_0x6
; 0000 00BF          }
; 0000 00C0          if(!sw_kanan)
_0x11:
	SBIC 0x16,3
	RJMP _0x12
; 0000 00C1          {
; 0000 00C2               delay_ms(deledele);
	CALL SUBOPT_0xB
; 0000 00C3               ADC0++;
	CALL SUBOPT_0x7
; 0000 00C4          }
; 0000 00C5   }
_0x12:
	RET
;    void set_ADC1()
; 0000 00C7   {
_set_ADC1:
; 0000 00C8       lcd_gotoxy(4,1);
	CALL SUBOPT_0xC
; 0000 00C9       lcd_putsf(">");
; 0000 00CA       dtadc=ADC1;
	CALL SUBOPT_0xD
; 0000 00CB 
; 0000 00CC       lcd_gotoxy(5,1);
	CALL SUBOPT_0xE
; 0000 00CD       sprintf(buf,"%d  ",dtadc);
; 0000 00CE       lcd_puts(buf);
; 0000 00CF          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x13
; 0000 00D0          {
; 0000 00D1               delay_ms(deledele);
	CALL SUBOPT_0xF
; 0000 00D2               ADC1--;
	CALL SUBOPT_0x6
; 0000 00D3          }
; 0000 00D4          if(!sw_kanan)
_0x13:
	SBIC 0x16,3
	RJMP _0x14
; 0000 00D5          {
; 0000 00D6               delay_ms(deledele);
	CALL SUBOPT_0xF
; 0000 00D7               ADC1++;
	CALL SUBOPT_0x7
; 0000 00D8          }
; 0000 00D9   }
_0x14:
	RET
;     void set_ADC2()
; 0000 00DB   {
_set_ADC2:
; 0000 00DC       lcd_gotoxy(8,1);
	CALL SUBOPT_0x10
; 0000 00DD       lcd_putsf(">");
; 0000 00DE       dtadc=ADC2;
	CALL SUBOPT_0x11
; 0000 00DF 
; 0000 00E0       lcd_gotoxy(9,1);
	CALL SUBOPT_0x12
; 0000 00E1       sprintf(buf,"%d  ",dtadc);
; 0000 00E2       lcd_puts(buf);
; 0000 00E3          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x15
; 0000 00E4          {
; 0000 00E5               delay_ms(deledele);
	CALL SUBOPT_0x13
; 0000 00E6               ADC2--;
	CALL SUBOPT_0x6
; 0000 00E7          }
; 0000 00E8          if(!sw_kanan)
_0x15:
	SBIC 0x16,3
	RJMP _0x16
; 0000 00E9          {
; 0000 00EA               delay_ms(deledele);
	CALL SUBOPT_0x13
; 0000 00EB               ADC2++;
	CALL SUBOPT_0x7
; 0000 00EC          }
; 0000 00ED   }
_0x16:
	RET
;     void set_ADC3()
; 0000 00EF   {
_set_ADC3:
; 0000 00F0       lcd_gotoxy(12,1);
	CALL SUBOPT_0x14
; 0000 00F1       lcd_putsf(">");
; 0000 00F2       dtadc=ADC3;
	CALL SUBOPT_0x15
; 0000 00F3 
; 0000 00F4       lcd_gotoxy(13,1);
	CALL SUBOPT_0x16
; 0000 00F5       sprintf(buf,"%d  ",dtadc);
; 0000 00F6       lcd_puts(buf);
; 0000 00F7          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x17
; 0000 00F8          {
; 0000 00F9               delay_ms(deledele);
	CALL SUBOPT_0x17
; 0000 00FA               ADC3--;
	CALL SUBOPT_0x6
; 0000 00FB          }
; 0000 00FC          if(!sw_kanan)
_0x17:
	SBIC 0x16,3
	RJMP _0x18
; 0000 00FD          {
; 0000 00FE               delay_ms(deledele);
	CALL SUBOPT_0x17
; 0000 00FF               ADC3++;
	CALL SUBOPT_0x7
; 0000 0100          }
; 0000 0101   }
_0x18:
	RET
;    void set_ADC4()
; 0000 0103   {
_set_ADC4:
; 0000 0104       lcd_gotoxy(0,1);
	CALL SUBOPT_0x8
; 0000 0105       lcd_putsf(">");
; 0000 0106       dtadc=ADC4;
	CALL SUBOPT_0x18
; 0000 0107 
; 0000 0108       lcd_gotoxy(1,1);
	CALL SUBOPT_0xA
; 0000 0109       sprintf(buf,"%d  ",dtadc);
; 0000 010A       lcd_puts(buf);
; 0000 010B          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x19
; 0000 010C          {
; 0000 010D               delay_ms(deledele);
	CALL SUBOPT_0x19
; 0000 010E               ADC4--;
	CALL SUBOPT_0x6
; 0000 010F          }
; 0000 0110          if(!sw_kanan)
_0x19:
	SBIC 0x16,3
	RJMP _0x1A
; 0000 0111          {
; 0000 0112               delay_ms(deledele);
	CALL SUBOPT_0x19
; 0000 0113               ADC4++;
	CALL SUBOPT_0x7
; 0000 0114          }
; 0000 0115   }
_0x1A:
	RET
;  void set_ADC5()
; 0000 0117   {
_set_ADC5:
; 0000 0118       lcd_gotoxy(4,1);
	CALL SUBOPT_0xC
; 0000 0119       lcd_putsf(">");
; 0000 011A       dtadc=ADC5;
	CALL SUBOPT_0x1A
; 0000 011B 
; 0000 011C       lcd_gotoxy(5,1);
	CALL SUBOPT_0xE
; 0000 011D       sprintf(buf,"%d  ",dtadc);
; 0000 011E       lcd_puts(buf);
; 0000 011F          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x1B
; 0000 0120          {
; 0000 0121               delay_ms(deledele);
	CALL SUBOPT_0x1B
; 0000 0122               ADC5--;
	CALL SUBOPT_0x6
; 0000 0123          }
; 0000 0124          if(!sw_kanan)
_0x1B:
	SBIC 0x16,3
	RJMP _0x1C
; 0000 0125          {
; 0000 0126               delay_ms(deledele);
	CALL SUBOPT_0x1B
; 0000 0127               ADC5++;
	CALL SUBOPT_0x7
; 0000 0128          }
; 0000 0129   }
_0x1C:
	RET
;  void set_ADC6()
; 0000 012B   {
_set_ADC6:
; 0000 012C       lcd_gotoxy(8,1);
	CALL SUBOPT_0x10
; 0000 012D       lcd_putsf(">");
; 0000 012E       dtadc=ADC6;
	CALL SUBOPT_0x1C
; 0000 012F 
; 0000 0130       lcd_gotoxy(9,1);
	CALL SUBOPT_0x12
; 0000 0131       sprintf(buf,"%d  ",dtadc);
; 0000 0132       lcd_puts(buf);
; 0000 0133          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x1D
; 0000 0134          {
; 0000 0135               delay_ms(deledele);
	CALL SUBOPT_0x1D
; 0000 0136               ADC6--;
	CALL SUBOPT_0x6
; 0000 0137          }
; 0000 0138          if(!sw_kanan)
_0x1D:
	SBIC 0x16,3
	RJMP _0x1E
; 0000 0139          {
; 0000 013A               delay_ms(deledele);
	CALL SUBOPT_0x1D
; 0000 013B               ADC6++;
	CALL SUBOPT_0x7
; 0000 013C          }
; 0000 013D 
; 0000 013E   }
_0x1E:
	RET
;  void set_ADC7()
; 0000 0140   {
_set_ADC7:
; 0000 0141       lcd_gotoxy(12,1);
	CALL SUBOPT_0x14
; 0000 0142       lcd_putsf(">");
; 0000 0143       dtadc=ADC7;
	CALL SUBOPT_0x1E
; 0000 0144 
; 0000 0145       lcd_gotoxy(13,1);
	CALL SUBOPT_0x16
; 0000 0146       sprintf(buf,"%d  ",dtadc);
; 0000 0147       lcd_puts(buf);
; 0000 0148          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x1F
; 0000 0149          {
; 0000 014A               delay_ms(deledele);
	CALL SUBOPT_0x1F
; 0000 014B               ADC7--;
	CALL SUBOPT_0x6
; 0000 014C          }
; 0000 014D          if(!sw_kanan)
_0x1F:
	SBIC 0x16,3
	RJMP _0x20
; 0000 014E          {
; 0000 014F               delay_ms(deledele);
	CALL SUBOPT_0x1F
; 0000 0150               ADC7++;
	CALL SUBOPT_0x7
; 0000 0151          }
; 0000 0152 
; 0000 0153   }
_0x20:
	RET
;
;    void set_Kp()
; 0000 0156   {
_set_Kp:
; 0000 0157       lcd_gotoxy(0,1);
	CALL SUBOPT_0x8
; 0000 0158       lcd_putsf(">");
; 0000 0159       dtadc=Kp;
	CALL SUBOPT_0x20
	STS  _dtadc,R30
; 0000 015A 
; 0000 015B       lcd_gotoxy(1,1);
	CALL SUBOPT_0xA
; 0000 015C       sprintf(buf,"%d  ",dtadc);
; 0000 015D       lcd_puts(buf);
; 0000 015E          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x21
; 0000 015F          {
; 0000 0160               delay_ms(deledele);
	CALL SUBOPT_0x21
; 0000 0161               Kp--;
	CALL SUBOPT_0x22
; 0000 0162          }
; 0000 0163          if(!sw_kanan)
_0x21:
	SBIC 0x16,3
	RJMP _0x22
; 0000 0164          {
; 0000 0165               delay_ms(deledele);
	CALL SUBOPT_0x21
; 0000 0166               Kp++;
	CALL SUBOPT_0x23
; 0000 0167          }
; 0000 0168   }
_0x22:
	RET
;   void set_Ki()
; 0000 016A   {
_set_Ki:
; 0000 016B       lcd_gotoxy(4,1);
	CALL SUBOPT_0xC
; 0000 016C       lcd_putsf(">");
; 0000 016D       dtadc=Ki;
	CALL SUBOPT_0x24
	STS  _dtadc,R30
; 0000 016E 
; 0000 016F       lcd_gotoxy(5,1);
	CALL SUBOPT_0xE
; 0000 0170       sprintf(buf,"%d  ",dtadc);
; 0000 0171       lcd_puts(buf);
; 0000 0172          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x23
; 0000 0173          {
; 0000 0174               delay_ms(deledele);
	CALL SUBOPT_0x25
; 0000 0175               Ki--;
	CALL SUBOPT_0x22
; 0000 0176          }
; 0000 0177          if(!sw_kanan)
_0x23:
	SBIC 0x16,3
	RJMP _0x24
; 0000 0178          {
; 0000 0179               delay_ms(deledele);
	CALL SUBOPT_0x25
; 0000 017A               Ki++;
	CALL SUBOPT_0x23
; 0000 017B          }
; 0000 017C   }
_0x24:
	RET
;  void set_Kd()
; 0000 017E   {
_set_Kd:
; 0000 017F       lcd_gotoxy(8,1);
	CALL SUBOPT_0x10
; 0000 0180       lcd_putsf(">");
; 0000 0181       dtadc=Kd;
	CALL SUBOPT_0x26
	CALL SUBOPT_0x27
; 0000 0182 
; 0000 0183       lcd_gotoxy(9,1);
; 0000 0184       sprintf(buf,"%d  ",dtadc);
; 0000 0185       lcd_puts(buf);
; 0000 0186          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x25
; 0000 0187          {
; 0000 0188               delay_ms(deledele);
	CALL SUBOPT_0x28
; 0000 0189               Kd--;
	CALL SUBOPT_0x22
; 0000 018A          }
; 0000 018B          if(!sw_kanan)
_0x25:
	SBIC 0x16,3
	RJMP _0x26
; 0000 018C          {
; 0000 018D               delay_ms(deledele);
	CALL SUBOPT_0x28
; 0000 018E               Kd++;
	CALL SUBOPT_0x23
; 0000 018F          }
; 0000 0190   }
_0x26:
	RET
;
;  void aksi(char px)
; 0000 0193   {
_aksi:
; 0000 0194    terus=1;
;	px -> Y+0
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	MOVW R12,R30
; 0000 0195    while(terus==1)
_0x27:
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	CP   R30,R12
	CPC  R31,R13
	BRNE _0x29
; 0000 0196    {
; 0000 0197     lcd_gotoxy(5,1);
	CALL SUBOPT_0x29
; 0000 0198     lcd_putsf("aksi=");
	__POINTW1FN _0x0,7
	CALL SUBOPT_0x2A
; 0000 0199     lcd_gotoxy(11,1);
	CALL SUBOPT_0x2B
; 0000 019A        sprintf(buf,"%d  ",arr[px+50]);
	LDD  R30,Y+4
	CALL SUBOPT_0x2C
	CALL SUBOPT_0x2D
; 0000 019B        lcd_puts(buf);
; 0000 019C     if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x2A
; 0000 019D      {
; 0000 019E        delay_ms(150);
	CALL SUBOPT_0x2E
; 0000 019F        arr[px+50] = 1;
	__ADDW1MN _arr,50
	MOVW R26,R30
	LDI  R30,LOW(1)
	CALL SUBOPT_0x2F
; 0000 01A0        lcd_gotoxy(11,1);
; 0000 01A1        sprintf(buf,"%d  ",1);
	CALL SUBOPT_0x30
; 0000 01A2        lcd_puts(buf);
; 0000 01A3        terus=0;
	CLR  R12
	CLR  R13
; 0000 01A4      }
; 0000 01A5      if(!sw_kiri)
_0x2A:
	SBIC 0x16,1
	RJMP _0x2B
; 0000 01A6      {
; 0000 01A7        delay_ms(150);
	CALL SUBOPT_0x2E
; 0000 01A8        arr[px+50] = 2;
	__ADDW1MN _arr,50
	MOVW R26,R30
	LDI  R30,LOW(2)
	CALL SUBOPT_0x2F
; 0000 01A9        lcd_gotoxy(11,1);
; 0000 01AA        sprintf(buf,"%d  ",2);
	CALL SUBOPT_0x31
; 0000 01AB        lcd_puts(buf);
; 0000 01AC        terus=0;
	CLR  R12
	CLR  R13
; 0000 01AD      }
; 0000 01AE      if(!sw_start)
_0x2B:
	SBIC 0x16,2
	RJMP _0x2C
; 0000 01AF      {
; 0000 01B0        delay_ms(150);
	CALL SUBOPT_0x2E
; 0000 01B1        arr[px+50] = 3;
	__ADDW1MN _arr,50
	MOVW R26,R30
	LDI  R30,LOW(3)
	CALL SUBOPT_0x2F
; 0000 01B2        lcd_gotoxy(11,1);
; 0000 01B3        sprintf(buf,"%d  ",3);
	CALL SUBOPT_0x32
; 0000 01B4        lcd_puts(buf);
; 0000 01B5        terus=0;
	CLR  R12
	CLR  R13
; 0000 01B6      }
; 0000 01B7 
; 0000 01B8      if(!sw_kanan)
_0x2C:
	SBIC 0x16,3
	RJMP _0x2D
; 0000 01B9      {
; 0000 01BA         delay_ms(100);
	CALL SUBOPT_0x33
; 0000 01BB          terus = 0;
; 0000 01BC      }
; 0000 01BD 
; 0000 01BE 
; 0000 01BF    }
_0x2D:
	RJMP _0x27
_0x29:
; 0000 01C0   }
	RJMP _0x20C0008
;  void set_SKN( char px)
; 0000 01C2   {
_set_SKN:
; 0000 01C3     terus=1;
;	px -> Y+0
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	MOVW R12,R30
; 0000 01C4     while(terus==1)
_0x2E:
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	CP   R30,R12
	CPC  R31,R13
	BREQ PC+3
	JMP _0x30
; 0000 01C5     {
; 0000 01C6     dtadc=px;
	LD   R30,Y
	STS  _dtadc,R30
; 0000 01C7     delay_ms(150);
	CALL SUBOPT_0x34
; 0000 01C8     lcd_clear();
	CALL SUBOPT_0x35
; 0000 01C9     lcd_gotoxy(0,0);
; 0000 01CA     lcd_putchar(0);
; 0000 01CB     lcd_gotoxy(1,0);
; 0000 01CC     lcd_putsf("SCN ke ");
	__POINTW1FN _0x0,13
	CALL SUBOPT_0x2A
; 0000 01CD     lcd_gotoxy(7,0);
	LDI  R30,LOW(7)
	CALL SUBOPT_0x36
; 0000 01CE     sprintf(buf,"%d  ",px);
	CALL SUBOPT_0x37
	LDD  R30,Y+4
	CALL SUBOPT_0x2D
; 0000 01CF     lcd_puts(buf);
; 0000 01D0     lcd_gotoxy(1,1);
	CALL SUBOPT_0x38
; 0000 01D1        sprintf(buf,"%d  ",arr[px]);
	LDD  R26,Y+4
	CALL SUBOPT_0x39
	CALL SUBOPT_0x3A
; 0000 01D2        lcd_puts(buf);
; 0000 01D3      if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x31
; 0000 01D4      {
; 0000 01D5        delay_ms(150);
	CALL SUBOPT_0x34
; 0000 01D6        arr[px] = 1;
	LD   R26,Y
	CALL SUBOPT_0x39
	LDI  R30,LOW(1)
	CALL SUBOPT_0x3B
; 0000 01D7        lcd_gotoxy(1,1);
; 0000 01D8        sprintf(buf,"%d  ",1);
	CALL SUBOPT_0x30
; 0000 01D9        lcd_puts(buf);
; 0000 01DA        aksi(px);
	CALL SUBOPT_0x3C
; 0000 01DB        terus=0;
; 0000 01DC      }
; 0000 01DD      if(!sw_kiri)
_0x31:
	SBIC 0x16,1
	RJMP _0x32
; 0000 01DE      {
; 0000 01DF        delay_ms(150);
	CALL SUBOPT_0x34
; 0000 01E0        arr[px] = 2;
	LD   R26,Y
	CALL SUBOPT_0x39
	LDI  R30,LOW(2)
	CALL SUBOPT_0x3B
; 0000 01E1        lcd_gotoxy(1,1);
; 0000 01E2        sprintf(buf,"%d  ",2);
	CALL SUBOPT_0x31
; 0000 01E3        lcd_puts(buf);
; 0000 01E4        aksi(px);
	CALL SUBOPT_0x3C
; 0000 01E5        terus=0;
; 0000 01E6      }
; 0000 01E7      if(!sw_start)
_0x32:
	SBIC 0x16,2
	RJMP _0x33
; 0000 01E8      {
; 0000 01E9        delay_ms(150);
	CALL SUBOPT_0x34
; 0000 01EA        arr[px] = 3;
	LD   R26,Y
	CALL SUBOPT_0x39
	LDI  R30,LOW(3)
	CALL SUBOPT_0x3B
; 0000 01EB        lcd_gotoxy(1,1);
; 0000 01EC        sprintf(buf,"%d  ",3);
	CALL SUBOPT_0x32
; 0000 01ED        lcd_puts(buf);
; 0000 01EE        aksi(px);
	CALL SUBOPT_0x3C
; 0000 01EF        terus=0;
; 0000 01F0      }
; 0000 01F1      if(!sw_kanan)
_0x33:
	SBIC 0x16,3
	RJMP _0x34
; 0000 01F2      {
; 0000 01F3        delay_ms(150);
	CALL SUBOPT_0x34
; 0000 01F4        arr[px] = 4;
	LD   R26,Y
	CALL SUBOPT_0x39
	LDI  R30,LOW(4)
	CALL SUBOPT_0x3B
; 0000 01F5        lcd_gotoxy(1,1);
; 0000 01F6        sprintf(buf,"%d  ",4);
	__GETD1N 0x4
	CALL __PUTPARD1
	LDI  R24,4
	CALL _sprintf
	ADIW R28,8
; 0000 01F7        lcd_puts(buf);
	CALL SUBOPT_0x3D
	CALL _lcd_puts
; 0000 01F8        aksi(px);
	CALL SUBOPT_0x3C
; 0000 01F9        terus=0;
; 0000 01FA      }
; 0000 01FB 
; 0000 01FC      if(!sw_ok)
_0x34:
	SBIC 0x16,0
	RJMP _0x35
; 0000 01FD      {
; 0000 01FE         delay_ms(100);
	CALL SUBOPT_0x33
; 0000 01FF          terus = 0;
; 0000 0200      }
; 0000 0201 
; 0000 0202      }
_0x35:
	RJMP _0x2E
_0x30:
; 0000 0203 
; 0000 0204     }
_0x20C0008:
	ADIW R28,1
	RET
;  void Zsken()
; 0000 0206   {
_Zsken:
; 0000 0207     int i;
; 0000 0208     delay_ms(100);
	ST   -Y,R17
	ST   -Y,R16
;	i -> R16,R17
	CALL SUBOPT_0x3E
; 0000 0209     dtadc=z;
	CALL SUBOPT_0x3F
	STS  _dtadc,R30
; 0000 020A     lcd_clear();
	CALL SUBOPT_0x35
; 0000 020B     lcd_gotoxy(0,0);
; 0000 020C     lcd_putchar(0);
; 0000 020D     lcd_gotoxy(1,0);
; 0000 020E     lcd_putsf("Jumlah = ");
	__POINTW1FN _0x0,21
	CALL SUBOPT_0x2A
; 0000 020F     lcd_gotoxy(1,1);
	CALL SUBOPT_0xA
; 0000 0210     sprintf(buf,"%d  ",dtadc);
; 0000 0211     lcd_puts(buf);
; 0000 0212     arr[100] = z;
	__POINTW1MN _arr,100
	MOVW R0,R30
	CALL SUBOPT_0x3F
	MOVW R26,R0
	CALL __EEPROMWRB
; 0000 0213     arr[99] = 3;
	__POINTW2MN _arr,99
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
; 0000 0214         if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x36
; 0000 0215          {
; 0000 0216               delay_ms(deledele);
	CALL SUBOPT_0x40
; 0000 0217               z--;
	CALL SUBOPT_0x22
; 0000 0218          }
; 0000 0219          if(!sw_kanan)
_0x36:
	SBIC 0x16,3
	RJMP _0x37
; 0000 021A          {
; 0000 021B               delay_ms(deledele);
	CALL SUBOPT_0x40
; 0000 021C               z++;
	CALL SUBOPT_0x23
; 0000 021D          }
; 0000 021E           if(!sw_start)
_0x37:
	SBIC 0x16,2
	RJMP _0x38
; 0000 021F          {
; 0000 0220               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0221               for(i=0;i<z;i++)
	__GETWRN 16,17,0
_0x3A:
	CALL SUBOPT_0x3F
	MOVW R26,R16
	LDI  R31,0
	CP   R26,R30
	CPC  R27,R31
	BRGE _0x3B
; 0000 0222                 {
; 0000 0223                     set_SKN(i);
	ST   -Y,R16
	RCALL _set_SKN
; 0000 0224                 }
	__ADDWRN 16,17,1
	RJMP _0x3A
_0x3B:
; 0000 0225               lcd_clear();
	CALL _lcd_clear
; 0000 0226 
; 0000 0227           }
; 0000 0228 
; 0000 0229   }
_0x38:
	LD   R16,Y+
	LD   R17,Y+
	RET
;
;   void menu_area()
; 0000 022C   {
_menu_area:
; 0000 022D       delay_ms(150);
	CALL SUBOPT_0x34
; 0000 022E       lcd_clear();
	CALL _lcd_clear
; 0000 022F       lcd_gotoxy(2,0);
	LDI  R30,LOW(2)
	CALL SUBOPT_0x36
; 0000 0230       lcd_putsf("AREA =");
	__POINTW1FN _0x0,31
	CALL SUBOPT_0x2A
; 0000 0231       lcd_gotoxy(2,1);
	CALL SUBOPT_0x41
; 0000 0232       lcd_putsf("1");
	__POINTW1FN _0x0,38
	CALL SUBOPT_0x2A
; 0000 0233       lcd_gotoxy(5,1);
	CALL SUBOPT_0x29
; 0000 0234       lcd_putsf("2");
	__POINTW1FN _0x0,40
	CALL SUBOPT_0x2A
; 0000 0235       lcd_gotoxy(8,1);
	CALL SUBOPT_0x42
; 0000 0236       lcd_putsf("3");
	CALL SUBOPT_0x43
; 0000 0237       lcd_gotoxy(12,1);
	LDI  R30,LOW(12)
	CALL SUBOPT_0x44
; 0000 0238       lcd_putsf("3");
	CALL SUBOPT_0x43
; 0000 0239 
; 0000 023A 
; 0000 023B       lcd_gotoxy(8,0);
	CALL SUBOPT_0x1
; 0000 023C       sprintf(buf,"%d  ",arr[98]);
	CALL SUBOPT_0x37
	__POINTW2MN _arr,98
	CALL SUBOPT_0x3A
; 0000 023D       lcd_puts(buf);
; 0000 023E          if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x3C
; 0000 023F          {
; 0000 0240 
; 0000 0241          arr[98]=1;
	__POINTW2MN _arr,98
	LDI  R30,LOW(1)
	CALL __EEPROMWRB
; 0000 0242          lcd_putsf("1");
	__POINTW1FN _0x0,38
	CALL SUBOPT_0x2A
; 0000 0243 
; 0000 0244          }
; 0000 0245           if(!sw_kiri)
_0x3C:
	SBIC 0x16,1
	RJMP _0x3D
; 0000 0246          {
; 0000 0247               arr[98]=2;
	__POINTW2MN _arr,98
	LDI  R30,LOW(2)
	CALL __EEPROMWRB
; 0000 0248               lcd_putsf("2");;
	__POINTW1FN _0x0,40
	CALL SUBOPT_0x2A
; 0000 0249          }
; 0000 024A          if(!sw_start)
_0x3D:
	SBIC 0x16,2
	RJMP _0x3E
; 0000 024B          {
; 0000 024C               arr[98]=3;
	__POINTW2MN _arr,98
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
; 0000 024D               lcd_putsf("3");;
	CALL SUBOPT_0x43
; 0000 024E          }
; 0000 024F          if(!sw_kanan)
_0x3E:
	SBIC 0x16,3
	RJMP _0x3F
; 0000 0250          {
; 0000 0251               arr[98]=4;
	__POINTW2MN _arr,98
	LDI  R30,LOW(4)
	CALL __EEPROMWRB
; 0000 0252               lcd_putsf("4");;
	__POINTW1FN _0x0,44
	CALL SUBOPT_0x2A
; 0000 0253          }
; 0000 0254 
; 0000 0255   }
_0x3F:
	RET
;
;   void menu_mode()
; 0000 0258   {
_menu_mode:
; 0000 0259       delay_ms(150);
	CALL SUBOPT_0x34
; 0000 025A       lcd_clear();
	CALL _lcd_clear
; 0000 025B       lcd_gotoxy(2,0);
	LDI  R30,LOW(2)
	CALL SUBOPT_0x36
; 0000 025C       lcd_putsf("MODE =");
	__POINTW1FN _0x0,46
	CALL SUBOPT_0x2A
; 0000 025D       lcd_gotoxy(2,1);
	CALL SUBOPT_0x41
; 0000 025E       lcd_putsf("A");
	__POINTW1FN _0x0,53
	CALL SUBOPT_0x2A
; 0000 025F       lcd_gotoxy(5,1);
	CALL SUBOPT_0x29
; 0000 0260       lcd_putsf("B");
	__POINTW1FN _0x0,55
	CALL SUBOPT_0x2A
; 0000 0261       lcd_gotoxy(8,1);
	CALL SUBOPT_0x42
; 0000 0262       lcd_putsf("C");
	__POINTW1FN _0x0,57
	CALL SUBOPT_0x2A
; 0000 0263       lcd_gotoxy(11,1);
	LDI  R30,LOW(11)
	CALL SUBOPT_0x44
; 0000 0264       lcd_putsf("D");
	__POINTW1FN _0x0,59
	CALL SUBOPT_0x2A
; 0000 0265 
; 0000 0266 
; 0000 0267       lcd_gotoxy(8,0);
	CALL SUBOPT_0x1
; 0000 0268       sprintf(buf,"%d  ",arr[99]);
	CALL SUBOPT_0x37
	__POINTW2MN _arr,99
	CALL SUBOPT_0x3A
; 0000 0269       lcd_puts(buf);
; 0000 026A          if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x40
; 0000 026B          {
; 0000 026C 
; 0000 026D          arr[99]=1;
	__POINTW2MN _arr,99
	LDI  R30,LOW(1)
	CALL __EEPROMWRB
; 0000 026E          lcd_putsf("A");
	__POINTW1FN _0x0,53
	CALL SUBOPT_0x2A
; 0000 026F 
; 0000 0270          }
; 0000 0271           if(!sw_kiri)
_0x40:
	SBIC 0x16,1
	RJMP _0x41
; 0000 0272          {
; 0000 0273               arr[99]=2;
	__POINTW2MN _arr,99
	LDI  R30,LOW(2)
	CALL __EEPROMWRB
; 0000 0274               lcd_putsf("B");;
	__POINTW1FN _0x0,55
	CALL SUBOPT_0x2A
; 0000 0275          }
; 0000 0276          if(!sw_start)
_0x41:
	SBIC 0x16,2
	RJMP _0x42
; 0000 0277          {
; 0000 0278               arr[99]=3;
	__POINTW2MN _arr,99
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
; 0000 0279               lcd_putsf("C");;
	__POINTW1FN _0x0,57
	CALL SUBOPT_0x2A
; 0000 027A          }
; 0000 027B          if(!sw_kanan)
_0x42:
	SBIC 0x16,3
	RJMP _0x43
; 0000 027C          {
; 0000 027D               arr[99]=4;
	__POINTW2MN _arr,99
	LDI  R30,LOW(4)
	CALL __EEPROMWRB
; 0000 027E               lcd_putsf("D");;
	__POINTW1FN _0x0,59
	CALL SUBOPT_0x2A
; 0000 027F          }
; 0000 0280 
; 0000 0281   }
_0x43:
	RET
;   void set_MAX()
; 0000 0283   {
_set_MAX:
; 0000 0284       lcd_gotoxy(8,1);
	CALL SUBOPT_0x10
; 0000 0285       lcd_putsf(">");
; 0000 0286       dtadc=MAXPWM;
	CALL SUBOPT_0x45
; 0000 0287 
; 0000 0288       lcd_gotoxy(9,1);
; 0000 0289       sprintf(buf,"%d  ",dtadc);
; 0000 028A       lcd_puts(buf);
; 0000 028B          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x44
; 0000 028C          {
; 0000 028D               delay_ms(deledele);
	CALL SUBOPT_0x46
; 0000 028E               MAXPWM--;
	CALL SUBOPT_0x6
; 0000 028F          }
; 0000 0290          if(!sw_kanan)
_0x44:
	SBIC 0x16,3
	RJMP _0x45
; 0000 0291          {
; 0000 0292               delay_ms(deledele);
	CALL SUBOPT_0x46
; 0000 0293               MAXPWM++;
	CALL SUBOPT_0x7
; 0000 0294          }
; 0000 0295   }
_0x45:
	RET
;   void set_MIN()
; 0000 0297   {
_set_MIN:
; 0000 0298       lcd_gotoxy(12,1);
	CALL SUBOPT_0x14
; 0000 0299       lcd_putsf(">");
; 0000 029A       dtadc=MINPWM;
	CALL SUBOPT_0x47
; 0000 029B 
; 0000 029C       lcd_gotoxy(13,1);
; 0000 029D       sprintf(buf,"%d  ",dtadc);
; 0000 029E       lcd_puts(buf);
; 0000 029F          if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x46
; 0000 02A0          {
; 0000 02A1               delay_ms(deledele);
	CALL SUBOPT_0x48
; 0000 02A2               MINPWM--;
	CALL SUBOPT_0x6
; 0000 02A3          }
; 0000 02A4          if(!sw_kanan)
_0x46:
	SBIC 0x16,3
	RJMP _0x47
; 0000 02A5          {
; 0000 02A6               delay_ms(deledele);
	CALL SUBOPT_0x48
; 0000 02A7               MINPWM++;
	CALL SUBOPT_0x7
; 0000 02A8          }
; 0000 02A9   }
_0x47:
	RET
;  void set_WRN()
; 0000 02AB   {
_set_WRN:
; 0000 02AC     lcd_gotoxy(4,1);
	CALL SUBOPT_0xC
; 0000 02AD     lcd_putsf(">");
; 0000 02AE     if (!sw_kiri)
	SBIC 0x16,1
	RJMP _0x48
; 0000 02AF     {
; 0000 02B0         delay_ms(150);
	CALL SUBOPT_0x34
; 0000 02B1         if (warna==1)
	CALL SUBOPT_0x49
	BRNE _0x49
; 0000 02B2         {
; 0000 02B3             warna = 0;
	LDI  R26,LOW(_warna)
	LDI  R27,HIGH(_warna)
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	CALL __EEPROMWRW
; 0000 02B4             lcd_gotoxy(6,1);
	LDI  R30,LOW(6)
	CALL SUBOPT_0x44
; 0000 02B5             lcd_putsf("H");
	__POINTW1FN _0x0,61
	RJMP _0x25A
; 0000 02B6         }
; 0000 02B7         else
_0x49:
; 0000 02B8         {
; 0000 02B9             warna = 1;
	LDI  R26,LOW(_warna)
	LDI  R27,HIGH(_warna)
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	CALL __EEPROMWRW
; 0000 02BA             lcd_gotoxy(6,1);
	LDI  R30,LOW(6)
	CALL SUBOPT_0x44
; 0000 02BB             lcd_putsf("P");
	__POINTW1FN _0x0,63
_0x25A:
	ST   -Y,R31
	ST   -Y,R30
	CALL _lcd_putsf
; 0000 02BC         }
; 0000 02BD     }
; 0000 02BE   }
_0x48:
	RET
;
;  //---------------Sistem Tampilan Menu ADC------------------//
;  //-----------------------------------------------------//
;  //-----------------------------------------------------//
;
;   void disp_set_ADC1()
; 0000 02C5    {
_disp_set_ADC1:
; 0000 02C6        if (warna == 1)
	CALL SUBOPT_0x49
	BRNE _0x4B
; 0000 02C7        {
; 0000 02C8             lcd_gotoxy(6,0);
	LDI  R30,LOW(6)
	CALL SUBOPT_0x36
; 0000 02C9             lcd_putsf("P");
	__POINTW1FN _0x0,63
	RJMP _0x25B
; 0000 02CA        }
; 0000 02CB        else
_0x4B:
; 0000 02CC        {
; 0000 02CD             lcd_gotoxy(6,0);
	LDI  R30,LOW(6)
	CALL SUBOPT_0x36
; 0000 02CE             lcd_putsf("H");
	__POINTW1FN _0x0,61
_0x25B:
	ST   -Y,R31
	ST   -Y,R30
	CALL _lcd_putsf
; 0000 02CF        }
; 0000 02D0      lihat_adc();
	RCALL _lihat_adc
; 0000 02D1      lcd_gotoxy(9,0);
	LDI  R30,LOW(9)
	CALL SUBOPT_0x36
; 0000 02D2      lcd_putsf("Def:");
	__POINTW1FN _0x0,65
	CALL SUBOPT_0x2A
; 0000 02D3 
; 0000 02D4      dtadc=batas_sensor;
	CALL SUBOPT_0x3
; 0000 02D5      lcd_gotoxy(13,0);
	RJMP _0x20C0007
; 0000 02D6      sprintf(buf,"%d  ",dtadc);
; 0000 02D7      lcd_puts(buf);
; 0000 02D8 
; 0000 02D9    }
;   void disp_set_ADC2()
; 0000 02DB    {
_disp_set_ADC2:
; 0000 02DC                   dtadc=read_adc(7);
	CALL SUBOPT_0x4A
	CALL SUBOPT_0x4B
; 0000 02DD                   lcd_gotoxy(1,0);
; 0000 02DE                   sprintf(buf,"%d  ",dtadc);
; 0000 02DF                   lcd_puts(buf);
; 0000 02E0 
; 0000 02E1                   dtadc=read_adc(6);
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x4D
; 0000 02E2                   lcd_gotoxy(5,0);
; 0000 02E3                   sprintf(buf,"%d  ",dtadc);
; 0000 02E4                   lcd_puts(buf);
; 0000 02E5 
; 0000 02E6                   dtadc=read_adc(5);
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x4F
; 0000 02E7                   lcd_gotoxy(9,0);
; 0000 02E8                   sprintf(buf,"%d  ",dtadc);
; 0000 02E9                   lcd_puts(buf);
; 0000 02EA 
; 0000 02EB                   dtadc=read_adc(4);
	CALL SUBOPT_0x50
	CALL SUBOPT_0x51
; 0000 02EC                   lcd_gotoxy(13,0);
; 0000 02ED                   sprintf(buf,"%d  ",dtadc);
; 0000 02EE                   lcd_puts(buf);
; 0000 02EF 
; 0000 02F0                   dtadc=ADC7;
	CALL SUBOPT_0x1E
; 0000 02F1                   lcd_gotoxy(1,1);
	CALL SUBOPT_0xA
; 0000 02F2                   sprintf(buf,"%d  ",dtadc);
; 0000 02F3                   lcd_puts(buf);
; 0000 02F4 
; 0000 02F5                   dtadc=ADC6;
	CALL SUBOPT_0x1C
; 0000 02F6                   lcd_gotoxy(5,1);
	CALL SUBOPT_0xE
; 0000 02F7                   sprintf(buf,"%d  ",dtadc);
; 0000 02F8                   lcd_puts(buf);
; 0000 02F9 
; 0000 02FA                   dtadc=ADC5;
	CALL SUBOPT_0x1A
; 0000 02FB                   lcd_gotoxy(9,1);
	CALL SUBOPT_0x12
; 0000 02FC                   sprintf(buf,"%d  ",dtadc);
; 0000 02FD                   lcd_puts(buf);
; 0000 02FE 
; 0000 02FF                   dtadc=ADC4;
	CALL SUBOPT_0x18
; 0000 0300                   lcd_gotoxy(13,1);
	LDI  R30,LOW(13)
	RJMP _0x20C0006
; 0000 0301                   sprintf(buf,"%d  ",dtadc);
; 0000 0302                   lcd_puts(buf);
; 0000 0303    }
;void disp_set_ADC3()
; 0000 0305    {
_disp_set_ADC3:
; 0000 0306                   dtadc=read_adc(3);
	CALL SUBOPT_0x52
	CALL SUBOPT_0x4B
; 0000 0307                   lcd_gotoxy(1,0);
; 0000 0308                   sprintf(buf,"%d  ",dtadc);
; 0000 0309                   lcd_puts(buf);
; 0000 030A 
; 0000 030B                   dtadc=read_adc(2);
	CALL SUBOPT_0x53
	CALL SUBOPT_0x4D
; 0000 030C                   lcd_gotoxy(5,0);
; 0000 030D                   sprintf(buf,"%d  ",dtadc);
; 0000 030E                   lcd_puts(buf);
; 0000 030F 
; 0000 0310                   dtadc=read_adc(1);
	CALL SUBOPT_0x54
	CALL SUBOPT_0x4F
; 0000 0311                   lcd_gotoxy(9,0);
; 0000 0312                   sprintf(buf,"%d  ",dtadc);
; 0000 0313                   lcd_puts(buf);
; 0000 0314 
; 0000 0315                   dtadc=read_adc(0);
	CALL SUBOPT_0x55
	CALL SUBOPT_0x51
; 0000 0316                   lcd_gotoxy(13,0);
; 0000 0317                   sprintf(buf,"%d  ",dtadc);
; 0000 0318                   lcd_puts(buf);
; 0000 0319 
; 0000 031A                   dtadc=ADC3;
	CALL SUBOPT_0x15
; 0000 031B                   lcd_gotoxy(1,1);
	CALL SUBOPT_0xA
; 0000 031C                   sprintf(buf,"%d  ",dtadc);
; 0000 031D                   lcd_puts(buf);
; 0000 031E 
; 0000 031F                   dtadc=ADC2;
	CALL SUBOPT_0x11
; 0000 0320                   lcd_gotoxy(5,1);
	CALL SUBOPT_0xE
; 0000 0321                   sprintf(buf,"%d  ",dtadc);
; 0000 0322                   lcd_puts(buf);
; 0000 0323 
; 0000 0324                   dtadc=ADC1;
	CALL SUBOPT_0xD
; 0000 0325                   lcd_gotoxy(9,1);
	CALL SUBOPT_0x12
; 0000 0326                   sprintf(buf,"%d  ",dtadc);
; 0000 0327                   lcd_puts(buf);
; 0000 0328 
; 0000 0329                   dtadc=ADC0;
	CALL SUBOPT_0x9
; 0000 032A                   lcd_gotoxy(13,1);
	LDI  R30,LOW(13)
	RJMP _0x20C0006
; 0000 032B                   sprintf(buf,"%d  ",dtadc);
; 0000 032C                   lcd_puts(buf);
; 0000 032D    }
;
;
;   void set_ADC()
; 0000 0331    {
_set_ADC:
; 0000 0332      def:
_0x4D:
; 0000 0333             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0334             disp_set_ADC1();
	RCALL _disp_set_ADC1
; 0000 0335             lcd_gotoxy(8,0);
	CALL SUBOPT_0x1
; 0000 0336             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0337             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x4E
; 0000 0338             {
; 0000 0339               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 033A 
; 0000 033B               while(sw_ok)
_0x4F:
	SBIS 0x16,0
	RJMP _0x51
; 0000 033C               {
; 0000 033D                 set_def();
	RCALL _set_def
; 0000 033E               }
	RJMP _0x4F
_0x51:
; 0000 033F               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0340               ADC0=ADC1=ADC2=ADC3=ADC4=ADC5=ADC6=ADC7=batas_sensor;
	LDI  R26,LOW(_batas_sensor)
	LDI  R27,HIGH(_batas_sensor)
	CALL __EEPROMRDW
	LDI  R26,LOW(_ADC7)
	LDI  R27,HIGH(_ADC7)
	CALL __EEPROMWRW
	LDI  R26,LOW(_ADC6)
	LDI  R27,HIGH(_ADC6)
	CALL __EEPROMWRW
	LDI  R26,LOW(_ADC5)
	LDI  R27,HIGH(_ADC5)
	CALL __EEPROMWRW
	LDI  R26,LOW(_ADC4)
	LDI  R27,HIGH(_ADC4)
	CALL __EEPROMWRW
	LDI  R26,LOW(_ADC3)
	LDI  R27,HIGH(_ADC3)
	CALL __EEPROMWRW
	LDI  R26,LOW(_ADC2)
	LDI  R27,HIGH(_ADC2)
	CALL __EEPROMWRW
	LDI  R26,LOW(_ADC1)
	LDI  R27,HIGH(_ADC1)
	CALL __EEPROMWRW
	LDI  R26,LOW(_ADC0)
	LDI  R27,HIGH(_ADC0)
	CALL __EEPROMWRW
; 0000 0341               lcd_clear();
	CALL _lcd_clear
; 0000 0342 
; 0000 0343             }
; 0000 0344             if(!sw_kanan)
_0x4E:
	SBIC 0x16,3
	RJMP _0x52
; 0000 0345             {
; 0000 0346              lcd_clear();
	CALL _lcd_clear
; 0000 0347              goto ADC0;
	RJMP _0x53
; 0000 0348             }
; 0000 0349             if(!sw_kiri)
_0x52:
	SBIC 0x16,1
	RJMP _0x54
; 0000 034A             {
; 0000 034B              lcd_clear();
	CALL _lcd_clear
; 0000 034C              goto ADC7;
	RJMP _0x55
; 0000 034D             }
; 0000 034E             if(!sw_cancel) goto exit; else goto def;
_0x54:
	SBIS 0x16,4
	RJMP _0x57
	RJMP _0x4D
; 0000 034F      ADC0:
_0x53:
; 0000 0350             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0351             disp_set_ADC2();
	RCALL _disp_set_ADC2
; 0000 0352             lcd_gotoxy(0,0);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x36
; 0000 0353             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0354             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x59
; 0000 0355             {
; 0000 0356               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0357 
; 0000 0358               while(sw_ok)
_0x5A:
	SBIS 0x16,0
	RJMP _0x5C
; 0000 0359               {
; 0000 035A                 set_ADC0();
	RCALL _set_ADC0
; 0000 035B               }
	RJMP _0x5A
_0x5C:
; 0000 035C               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 035D               lcd_clear();
	CALL _lcd_clear
; 0000 035E 
; 0000 035F             }
; 0000 0360             if(!sw_kanan)
_0x59:
	SBIC 0x16,3
	RJMP _0x5D
; 0000 0361             {
; 0000 0362              lcd_clear();
	CALL _lcd_clear
; 0000 0363              goto ADC1;
	RJMP _0x5E
; 0000 0364             }
; 0000 0365             if(!sw_kiri)
_0x5D:
	SBIC 0x16,1
	RJMP _0x5F
; 0000 0366             {
; 0000 0367              lcd_clear();
	CALL _lcd_clear
; 0000 0368              goto def;
	RJMP _0x4D
; 0000 0369             }
; 0000 036A             if(!sw_cancel) goto exit; else goto ADC0;
_0x5F:
	SBIS 0x16,4
	RJMP _0x57
	RJMP _0x53
; 0000 036B      ADC1:
_0x5E:
; 0000 036C             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 036D             disp_set_ADC2();
	RCALL _disp_set_ADC2
; 0000 036E             lcd_gotoxy(4,0);
	LDI  R30,LOW(4)
	CALL SUBOPT_0x36
; 0000 036F             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0370             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x62
; 0000 0371             {
; 0000 0372               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0373 
; 0000 0374               while(sw_ok)
_0x63:
	SBIS 0x16,0
	RJMP _0x65
; 0000 0375               {
; 0000 0376                 set_ADC1();
	RCALL _set_ADC1
; 0000 0377               }
	RJMP _0x63
_0x65:
; 0000 0378               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0379               lcd_clear();
	CALL _lcd_clear
; 0000 037A 
; 0000 037B             }
; 0000 037C             if(!sw_kanan)
_0x62:
	SBIC 0x16,3
	RJMP _0x66
; 0000 037D             {
; 0000 037E              lcd_clear();
	CALL _lcd_clear
; 0000 037F              goto ADC2;
	RJMP _0x67
; 0000 0380             }
; 0000 0381             if(!sw_kiri)
_0x66:
	SBIC 0x16,1
	RJMP _0x68
; 0000 0382             {
; 0000 0383              lcd_clear();
	CALL _lcd_clear
; 0000 0384              goto ADC0;
	RJMP _0x53
; 0000 0385             }
; 0000 0386             if(!sw_cancel) goto exit; else goto ADC1;
_0x68:
	SBIS 0x16,4
	RJMP _0x57
	RJMP _0x5E
; 0000 0387      ADC2:
_0x67:
; 0000 0388             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0389             disp_set_ADC2();
	RCALL _disp_set_ADC2
; 0000 038A             lcd_gotoxy(8,0);
	CALL SUBOPT_0x1
; 0000 038B             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 038C             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x6B
; 0000 038D             {
; 0000 038E               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 038F 
; 0000 0390               while(sw_ok)
_0x6C:
	SBIS 0x16,0
	RJMP _0x6E
; 0000 0391               {
; 0000 0392                 set_ADC2();
	RCALL _set_ADC2
; 0000 0393               }
	RJMP _0x6C
_0x6E:
; 0000 0394               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0395               lcd_clear();
	CALL _lcd_clear
; 0000 0396 
; 0000 0397             }
; 0000 0398             if(!sw_kanan)
_0x6B:
	SBIC 0x16,3
	RJMP _0x6F
; 0000 0399             {
; 0000 039A              lcd_clear();
	CALL _lcd_clear
; 0000 039B              goto ADC3;
	RJMP _0x70
; 0000 039C             }
; 0000 039D             if(!sw_kiri)
_0x6F:
	SBIC 0x16,1
	RJMP _0x71
; 0000 039E             {
; 0000 039F              lcd_clear();
	CALL _lcd_clear
; 0000 03A0              goto ADC1;
	RJMP _0x5E
; 0000 03A1             }
; 0000 03A2             if(!sw_cancel) goto exit; else goto ADC2;
_0x71:
	SBIS 0x16,4
	RJMP _0x57
	RJMP _0x67
; 0000 03A3      ADC3:
_0x70:
; 0000 03A4             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 03A5             disp_set_ADC2();
	RCALL _disp_set_ADC2
; 0000 03A6             lcd_gotoxy(12,0);
	LDI  R30,LOW(12)
	CALL SUBOPT_0x36
; 0000 03A7             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 03A8             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x74
; 0000 03A9             {
; 0000 03AA               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 03AB 
; 0000 03AC               while(sw_ok)
_0x75:
	SBIS 0x16,0
	RJMP _0x77
; 0000 03AD               {
; 0000 03AE                 set_ADC3();
	RCALL _set_ADC3
; 0000 03AF               }
	RJMP _0x75
_0x77:
; 0000 03B0               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 03B1               lcd_clear();
	CALL _lcd_clear
; 0000 03B2 
; 0000 03B3             }
; 0000 03B4             if(!sw_kanan)
_0x74:
	SBIC 0x16,3
	RJMP _0x78
; 0000 03B5             {
; 0000 03B6              lcd_clear();
	CALL _lcd_clear
; 0000 03B7              goto ADC4;
	RJMP _0x79
; 0000 03B8             }
; 0000 03B9             if(!sw_kiri)
_0x78:
	SBIC 0x16,1
	RJMP _0x7A
; 0000 03BA             {
; 0000 03BB              lcd_clear();
	CALL _lcd_clear
; 0000 03BC              goto ADC2;
	RJMP _0x67
; 0000 03BD             }
; 0000 03BE             if(!sw_cancel) goto exit; else goto ADC3;
_0x7A:
	SBIS 0x16,4
	RJMP _0x57
	RJMP _0x70
; 0000 03BF 
; 0000 03C0      ADC4:
_0x79:
; 0000 03C1             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 03C2             disp_set_ADC3();
	RCALL _disp_set_ADC3
; 0000 03C3             lcd_gotoxy(0,0);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x36
; 0000 03C4             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 03C5             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x7D
; 0000 03C6             {
; 0000 03C7               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 03C8 
; 0000 03C9               while(sw_ok)
_0x7E:
	SBIS 0x16,0
	RJMP _0x80
; 0000 03CA               {
; 0000 03CB                 set_ADC4();
	RCALL _set_ADC4
; 0000 03CC               }
	RJMP _0x7E
_0x80:
; 0000 03CD               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 03CE               lcd_clear();
	CALL _lcd_clear
; 0000 03CF 
; 0000 03D0             }
; 0000 03D1             if(!sw_kanan)
_0x7D:
	SBIC 0x16,3
	RJMP _0x81
; 0000 03D2             {
; 0000 03D3              lcd_clear();
	CALL _lcd_clear
; 0000 03D4              goto ADC5;
	RJMP _0x82
; 0000 03D5             }
; 0000 03D6             if(!sw_kiri)
_0x81:
	SBIC 0x16,1
	RJMP _0x83
; 0000 03D7             {
; 0000 03D8              lcd_clear();
	CALL _lcd_clear
; 0000 03D9              goto ADC3;
	RJMP _0x70
; 0000 03DA             }
; 0000 03DB             if(!sw_cancel) goto exit; else goto ADC4;
_0x83:
	SBIS 0x16,4
	RJMP _0x57
	RJMP _0x79
; 0000 03DC 
; 0000 03DD      ADC5:
_0x82:
; 0000 03DE             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 03DF             disp_set_ADC3();
	RCALL _disp_set_ADC3
; 0000 03E0             lcd_gotoxy(4,0);
	LDI  R30,LOW(4)
	CALL SUBOPT_0x36
; 0000 03E1             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 03E2             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x86
; 0000 03E3             {
; 0000 03E4               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 03E5 
; 0000 03E6               while(sw_ok)
_0x87:
	SBIS 0x16,0
	RJMP _0x89
; 0000 03E7               {
; 0000 03E8                 set_ADC5();
	RCALL _set_ADC5
; 0000 03E9               }
	RJMP _0x87
_0x89:
; 0000 03EA               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 03EB               lcd_clear();
	CALL _lcd_clear
; 0000 03EC 
; 0000 03ED             }
; 0000 03EE             if(!sw_kanan)
_0x86:
	SBIC 0x16,3
	RJMP _0x8A
; 0000 03EF             {
; 0000 03F0              lcd_clear();
	CALL _lcd_clear
; 0000 03F1              goto ADC6;
	RJMP _0x8B
; 0000 03F2             }
; 0000 03F3             if(!sw_kiri)
_0x8A:
	SBIC 0x16,1
	RJMP _0x8C
; 0000 03F4             {
; 0000 03F5              lcd_clear();
	CALL _lcd_clear
; 0000 03F6              goto ADC4;
	RJMP _0x79
; 0000 03F7             }
; 0000 03F8             if(!sw_cancel) goto exit; else goto ADC5;
_0x8C:
	SBIS 0x16,4
	RJMP _0x57
	RJMP _0x82
; 0000 03F9 
; 0000 03FA      ADC6:
_0x8B:
; 0000 03FB             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 03FC             disp_set_ADC3();
	RCALL _disp_set_ADC3
; 0000 03FD             lcd_gotoxy(8,0);
	CALL SUBOPT_0x1
; 0000 03FE             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 03FF             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x8F
; 0000 0400             {
; 0000 0401               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0402 
; 0000 0403               while(sw_ok)
_0x90:
	SBIS 0x16,0
	RJMP _0x92
; 0000 0404               {
; 0000 0405                 set_ADC6();
	RCALL _set_ADC6
; 0000 0406               }
	RJMP _0x90
_0x92:
; 0000 0407               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0408               lcd_clear();
	CALL _lcd_clear
; 0000 0409 
; 0000 040A             }
; 0000 040B             if(!sw_kanan)
_0x8F:
	SBIC 0x16,3
	RJMP _0x93
; 0000 040C             {
; 0000 040D              lcd_clear();
	CALL _lcd_clear
; 0000 040E              goto ADC7;
	RJMP _0x55
; 0000 040F             }
; 0000 0410             if(!sw_kiri)
_0x93:
	SBIC 0x16,1
	RJMP _0x94
; 0000 0411             {
; 0000 0412              lcd_clear();
	CALL _lcd_clear
; 0000 0413              goto ADC5;
	RJMP _0x82
; 0000 0414             }
; 0000 0415             if(!sw_cancel) goto exit; else goto ADC6;
_0x94:
	SBIS 0x16,4
	RJMP _0x57
	RJMP _0x8B
; 0000 0416 
; 0000 0417      ADC7:
_0x55:
; 0000 0418             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0419             disp_set_ADC3();
	RCALL _disp_set_ADC3
; 0000 041A             lcd_gotoxy(12,0);
	LDI  R30,LOW(12)
	CALL SUBOPT_0x36
; 0000 041B             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 041C             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x97
; 0000 041D             {
; 0000 041E               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 041F 
; 0000 0420               while(sw_ok)
_0x98:
	SBIS 0x16,0
	RJMP _0x9A
; 0000 0421               {
; 0000 0422                 set_ADC7();
	RCALL _set_ADC7
; 0000 0423               }
	RJMP _0x98
_0x9A:
; 0000 0424               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0425               lcd_clear();
	CALL _lcd_clear
; 0000 0426 
; 0000 0427             }
; 0000 0428             if(!sw_kanan)
_0x97:
	SBIC 0x16,3
	RJMP _0x9B
; 0000 0429             {
; 0000 042A              lcd_clear();
	CALL _lcd_clear
; 0000 042B              goto def;
	RJMP _0x4D
; 0000 042C             }
; 0000 042D             if(!sw_kiri)
_0x9B:
	SBIC 0x16,1
	RJMP _0x9C
; 0000 042E             {
; 0000 042F              lcd_clear();
	CALL _lcd_clear
; 0000 0430              goto ADC6;
	RJMP _0x8B
; 0000 0431             }
; 0000 0432             if(!sw_cancel) goto exit; else goto ADC7;
_0x9C:
	SBIS 0x16,4
	RJMP _0x57
	RJMP _0x55
; 0000 0433        exit:
_0x57:
; 0000 0434    }
	RET
;
;   void disp_menu_pid()
; 0000 0437    {
_disp_menu_pid:
; 0000 0438        lcd_gotoxy(0,0);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x36
; 0000 0439        lcd_putsf(" Kp: Ki: Kd:");
	__POINTW1FN _0x0,70
	CALL SUBOPT_0x2A
; 0000 043A        dtadc=Kp;
	CALL SUBOPT_0x20
	STS  _dtadc,R30
; 0000 043B        lcd_gotoxy(1,1);
	CALL SUBOPT_0xA
; 0000 043C        sprintf(buf,"%d  ",dtadc);
; 0000 043D        lcd_puts(buf);
; 0000 043E        dtadc=Ki;
	CALL SUBOPT_0x24
	STS  _dtadc,R30
; 0000 043F        lcd_gotoxy(5,1);
	CALL SUBOPT_0xE
; 0000 0440        sprintf(buf,"%d  ",dtadc);
; 0000 0441        lcd_puts(buf);
; 0000 0442        dtadc=Kd;
	CALL SUBOPT_0x26
	STS  _dtadc,R30
; 0000 0443        lcd_gotoxy(9,1);
	LDI  R30,LOW(9)
_0x20C0006:
	ST   -Y,R30
	LDI  R30,LOW(1)
_0x20C0007:
	ST   -Y,R30
	CALL _lcd_gotoxy
; 0000 0444        sprintf(buf,"%d  ",dtadc);
	CALL SUBOPT_0x37
	CALL SUBOPT_0x57
; 0000 0445        lcd_puts(buf);
; 0000 0446    }
	RET
;   void menu_pid()
; 0000 0448    {
_menu_pid:
; 0000 0449            menukp:
_0x9F:
; 0000 044A             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 044B             disp_menu_pid();
	RCALL _disp_menu_pid
; 0000 044C             lcd_gotoxy(0,0);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x36
; 0000 044D             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 044E             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xA0
; 0000 044F             {
; 0000 0450               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0451 
; 0000 0452               while(sw_ok)
_0xA1:
	SBIS 0x16,0
	RJMP _0xA3
; 0000 0453               {
; 0000 0454                 set_Kp();
	RCALL _set_Kp
; 0000 0455               }
	RJMP _0xA1
_0xA3:
; 0000 0456               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0457               lcd_clear();
	CALL _lcd_clear
; 0000 0458 
; 0000 0459             }
; 0000 045A             if(!sw_kanan)
_0xA0:
	SBIC 0x16,3
	RJMP _0xA4
; 0000 045B             {
; 0000 045C              lcd_clear();
	CALL _lcd_clear
; 0000 045D              goto menuki;
	RJMP _0xA5
; 0000 045E             }
; 0000 045F             if(!sw_kiri)
_0xA4:
	SBIC 0x16,1
	RJMP _0xA6
; 0000 0460             {
; 0000 0461              lcd_clear();
	CALL _lcd_clear
; 0000 0462              goto menukd;
	RJMP _0xA7
; 0000 0463             }
; 0000 0464             if(!sw_cancel) goto exit; else goto menukp;
_0xA6:
	SBIS 0x16,4
	RJMP _0xA9
	RJMP _0x9F
; 0000 0465 
; 0000 0466          menuki:
_0xA5:
; 0000 0467             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0468             disp_menu_pid();
	RCALL _disp_menu_pid
; 0000 0469             lcd_gotoxy(4,0);
	LDI  R30,LOW(4)
	CALL SUBOPT_0x36
; 0000 046A             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 046B             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xAB
; 0000 046C             {
; 0000 046D               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 046E 
; 0000 046F               while(sw_ok)
_0xAC:
	SBIS 0x16,0
	RJMP _0xAE
; 0000 0470               {
; 0000 0471                 set_Ki();
	RCALL _set_Ki
; 0000 0472               }
	RJMP _0xAC
_0xAE:
; 0000 0473               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0474               lcd_clear();
	CALL _lcd_clear
; 0000 0475 
; 0000 0476             }
; 0000 0477             if(!sw_kanan)
_0xAB:
	SBIC 0x16,3
	RJMP _0xAF
; 0000 0478             {
; 0000 0479              lcd_clear();
	CALL _lcd_clear
; 0000 047A              goto menukd;
	RJMP _0xA7
; 0000 047B             }
; 0000 047C             if(!sw_kiri)
_0xAF:
	SBIC 0x16,1
	RJMP _0xB0
; 0000 047D             {
; 0000 047E              lcd_clear();
	CALL _lcd_clear
; 0000 047F              goto menukp;
	RJMP _0x9F
; 0000 0480             }
; 0000 0481             if(!sw_cancel) goto exit; else goto menuki;
_0xB0:
	SBIS 0x16,4
	RJMP _0xA9
	RJMP _0xA5
; 0000 0482 
; 0000 0483          menukd:
_0xA7:
; 0000 0484             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0485             disp_menu_pid();
	RCALL _disp_menu_pid
; 0000 0486             lcd_gotoxy(8,0);
	CALL SUBOPT_0x1
; 0000 0487             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0488             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xB3
; 0000 0489             {
; 0000 048A               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 048B 
; 0000 048C               while(sw_ok)
_0xB4:
	SBIS 0x16,0
	RJMP _0xB6
; 0000 048D               {
; 0000 048E                 set_Kd();
	RCALL _set_Kd
; 0000 048F               }
	RJMP _0xB4
_0xB6:
; 0000 0490               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0491               lcd_clear();
	CALL _lcd_clear
; 0000 0492 
; 0000 0493             }
; 0000 0494             if(!sw_kanan)
_0xB3:
	SBIC 0x16,3
	RJMP _0xB7
; 0000 0495             {
; 0000 0496              lcd_clear();
	CALL _lcd_clear
; 0000 0497              goto menukp;
	RJMP _0x9F
; 0000 0498             }
; 0000 0499             if(!sw_kiri)
_0xB7:
	SBIC 0x16,1
	RJMP _0xB8
; 0000 049A             {
; 0000 049B              lcd_clear();
	CALL _lcd_clear
; 0000 049C              goto menuki;
	RJMP _0xA5
; 0000 049D             }
; 0000 049E             if(!sw_cancel) goto exit; else goto menukd;
_0xB8:
	SBIS 0x16,4
	RJMP _0xA9
	RJMP _0xA7
; 0000 049F    exit:
_0xA9:
; 0000 04A0    }
	RET
;   void disp_menu_debug1()
; 0000 04A2    {
_disp_menu_debug1:
; 0000 04A3        lcd_gotoxy(0,0);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x36
; 0000 04A4        lcd_putsf(" ADC WRN MAX MIN");
	__POINTW1FN _0x0,83
	CALL SUBOPT_0x2A
; 0000 04A5        lcd_gotoxy(1,1);
	LDI  R30,LOW(1)
	CALL SUBOPT_0x44
; 0000 04A6        lcd_putsf("|||");
	__POINTW1FN _0x0,100
	CALL SUBOPT_0x2A
; 0000 04A7 
; 0000 04A8        dtadc=MAXPWM;
	CALL SUBOPT_0x45
; 0000 04A9        lcd_gotoxy(9,1);
; 0000 04AA        sprintf(buf,"%d  ",dtadc);
; 0000 04AB        lcd_puts(buf);
; 0000 04AC 
; 0000 04AD        dtadc=MINPWM;
	CALL SUBOPT_0x47
; 0000 04AE        lcd_gotoxy(13,1);
; 0000 04AF        sprintf(buf,"%d  ",dtadc);
; 0000 04B0        lcd_puts(buf);
; 0000 04B1 
; 0000 04B2         if (warna == 1)
	CALL SUBOPT_0x49
	BRNE _0xBB
; 0000 04B3        {
; 0000 04B4             lcd_gotoxy(6,1);
	LDI  R30,LOW(6)
	CALL SUBOPT_0x44
; 0000 04B5             lcd_putsf("P");
	__POINTW1FN _0x0,63
	RJMP _0x25C
; 0000 04B6        }
; 0000 04B7        else
_0xBB:
; 0000 04B8        {
; 0000 04B9             lcd_gotoxy(6,1);
	LDI  R30,LOW(6)
	CALL SUBOPT_0x44
; 0000 04BA             lcd_putsf("H");
	__POINTW1FN _0x0,61
_0x25C:
	ST   -Y,R31
	ST   -Y,R30
	CALL _lcd_putsf
; 0000 04BB        }
; 0000 04BC 
; 0000 04BD    }
	RET
;
;
;   void menu_debug()
; 0000 04C1    {
_menu_debug:
; 0000 04C2      menuadc:
_0xBD:
; 0000 04C3             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 04C4             disp_menu_debug1();
	RCALL _disp_menu_debug1
; 0000 04C5             lcd_gotoxy(0,0);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x36
; 0000 04C6             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 04C7             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xBE
; 0000 04C8             {
; 0000 04C9               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 04CA 
; 0000 04CB               lcd_clear();
	CALL _lcd_clear
; 0000 04CC               while(sw_cancel)
_0xBF:
	SBIS 0x16,4
	RJMP _0xC1
; 0000 04CD               {
; 0000 04CE                 set_ADC();
	RCALL _set_ADC
; 0000 04CF               }
	RJMP _0xBF
_0xC1:
; 0000 04D0               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 04D1               lcd_clear();
	CALL _lcd_clear
; 0000 04D2 
; 0000 04D3             }
; 0000 04D4             if(!sw_kanan)
_0xBE:
	SBIC 0x16,3
	RJMP _0xC2
; 0000 04D5             {
; 0000 04D6             lcd_clear();
	CALL _lcd_clear
; 0000 04D7              goto menuwarna;
	RJMP _0xC3
; 0000 04D8             }
; 0000 04D9             if(!sw_kiri)
_0xC2:
	SBIC 0x16,1
	RJMP _0xC4
; 0000 04DA             {
; 0000 04DB              lcd_clear();
	CALL _lcd_clear
; 0000 04DC              goto menumin;
	RJMP _0xC5
; 0000 04DD             }
; 0000 04DE             if(!sw_cancel) goto exit; else goto menuadc;
_0xC4:
	SBIS 0x16,4
	RJMP _0xC7
	RJMP _0xBD
; 0000 04DF 
; 0000 04E0        menumax:
_0xC9:
; 0000 04E1             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 04E2             disp_menu_debug1();
	RCALL _disp_menu_debug1
; 0000 04E3             lcd_gotoxy(8,0);
	CALL SUBOPT_0x1
; 0000 04E4             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 04E5             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xCA
; 0000 04E6             {
; 0000 04E7               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 04E8 
; 0000 04E9               while(sw_ok)
_0xCB:
	SBIS 0x16,0
	RJMP _0xCD
; 0000 04EA               {
; 0000 04EB                 set_MAX();
	RCALL _set_MAX
; 0000 04EC               }
	RJMP _0xCB
_0xCD:
; 0000 04ED               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 04EE               lcd_clear();
	CALL _lcd_clear
; 0000 04EF 
; 0000 04F0             }
; 0000 04F1             if(!sw_kanan)
_0xCA:
	SBIC 0x16,3
	RJMP _0xCE
; 0000 04F2             {
; 0000 04F3              lcd_clear();
	CALL _lcd_clear
; 0000 04F4              goto menumin;
	RJMP _0xC5
; 0000 04F5             }
; 0000 04F6             if(!sw_kiri)
_0xCE:
	SBIC 0x16,1
	RJMP _0xCF
; 0000 04F7             {
; 0000 04F8              lcd_clear();
	CALL _lcd_clear
; 0000 04F9              goto menuwarna;
	RJMP _0xC3
; 0000 04FA             }
; 0000 04FB             if(!sw_cancel) goto exit; else goto menumax;
_0xCF:
	SBIS 0x16,4
	RJMP _0xC7
	RJMP _0xC9
; 0000 04FC        menumin:
_0xC5:
; 0000 04FD             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 04FE             disp_menu_debug1();
	RCALL _disp_menu_debug1
; 0000 04FF             lcd_gotoxy(12,0);
	LDI  R30,LOW(12)
	CALL SUBOPT_0x36
; 0000 0500             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0501             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xD2
; 0000 0502             {
; 0000 0503               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0504 
; 0000 0505               while(sw_ok)
_0xD3:
	SBIS 0x16,0
	RJMP _0xD5
; 0000 0506               {
; 0000 0507                 set_MIN();
	RCALL _set_MIN
; 0000 0508               }
	RJMP _0xD3
_0xD5:
; 0000 0509               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 050A               lcd_clear();
	CALL _lcd_clear
; 0000 050B 
; 0000 050C             }
; 0000 050D             if(!sw_kanan)
_0xD2:
	SBIC 0x16,3
	RJMP _0xD6
; 0000 050E             {
; 0000 050F              lcd_clear();
	CALL _lcd_clear
; 0000 0510              goto menuadc;
	RJMP _0xBD
; 0000 0511             }
; 0000 0512             if(!sw_kiri)
_0xD6:
	SBIC 0x16,1
	RJMP _0xD7
; 0000 0513             {
; 0000 0514              lcd_clear();
	CALL _lcd_clear
; 0000 0515              goto menumax;
	RJMP _0xC9
; 0000 0516             }
; 0000 0517             if(!sw_cancel) goto exit; else goto menumin;
_0xD7:
	SBIS 0x16,4
	RJMP _0xC7
	RJMP _0xC5
; 0000 0518       menuwarna:
_0xC3:
; 0000 0519             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 051A             disp_menu_debug1();
	RCALL _disp_menu_debug1
; 0000 051B             lcd_gotoxy(4,0);
	LDI  R30,LOW(4)
	CALL SUBOPT_0x36
; 0000 051C             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 051D             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xDA
; 0000 051E             {
; 0000 051F               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0520 
; 0000 0521               while(sw_ok)
_0xDB:
	SBIS 0x16,0
	RJMP _0xDD
; 0000 0522               {
; 0000 0523                 set_WRN();
	RCALL _set_WRN
; 0000 0524               }
	RJMP _0xDB
_0xDD:
; 0000 0525               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0526               lcd_clear();
	CALL _lcd_clear
; 0000 0527 
; 0000 0528             }
; 0000 0529             if(!sw_kanan)
_0xDA:
	SBIC 0x16,3
	RJMP _0xDE
; 0000 052A             {
; 0000 052B              lcd_clear();
	CALL _lcd_clear
; 0000 052C              goto menumax;
	RJMP _0xC9
; 0000 052D             }
; 0000 052E             if(!sw_kiri)
_0xDE:
	SBIC 0x16,1
	RJMP _0xDF
; 0000 052F             {
; 0000 0530              lcd_clear();
	CALL _lcd_clear
; 0000 0531              goto menuadc;
	RJMP _0xBD
; 0000 0532             }
; 0000 0533             if(!sw_cancel) goto exit; else goto menuwarna;
_0xDF:
	SBIS 0x16,4
	RJMP _0xC7
	RJMP _0xC3
; 0000 0534 
; 0000 0535    exit:
_0xC7:
; 0000 0536    }
	RET
;   void disp_menu()
; 0000 0538    {
_disp_menu:
; 0000 0539      lcd_gotoxy(1,0);
	LDI  R30,LOW(1)
	CALL SUBOPT_0x36
; 0000 053A      lcd_putsf("D E M E N T O R");
	CALL SUBOPT_0x58
; 0000 053B      lcd_gotoxy(0,1);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x44
; 0000 053C      lcd_putsf(" DEB PID X PLAN");
	__POINTW1FN _0x0,120
	RJMP _0x20C0005
; 0000 053D    }
;
;
;
;   void disp_plan()
; 0000 0542    {
_disp_plan:
; 0000 0543      lcd_gotoxy(1,0);
	LDI  R30,LOW(1)
	CALL SUBOPT_0x36
; 0000 0544      lcd_putsf("D E M E N T O R");
	CALL SUBOPT_0x58
; 0000 0545      lcd_gotoxy(0,1);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x44
; 0000 0546      lcd_putsf(" Part Mode Area ");
	__POINTW1FN _0x0,136
	RJMP _0x20C0005
; 0000 0547    }
;
;void planning()
; 0000 054A    {
_planning:
; 0000 054B      part:
_0xE2:
; 0000 054C             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 054D             disp_plan();
	RCALL _disp_plan
; 0000 054E             lcd_gotoxy(0,1);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x44
; 0000 054F             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0550             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xE3
; 0000 0551             {
; 0000 0552               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0553 
; 0000 0554               lcd_clear();
	CALL _lcd_clear
; 0000 0555               while(sw_cancel)
_0xE4:
	SBIS 0x16,4
	RJMP _0xE6
; 0000 0556               {
; 0000 0557                 Zsken();
	RCALL _Zsken
; 0000 0558               }
	RJMP _0xE4
_0xE6:
; 0000 0559               lcd_clear();
	CALL SUBOPT_0x59
; 0000 055A               delay_ms(150);
; 0000 055B 
; 0000 055C             }
; 0000 055D 
; 0000 055E             if(!sw_kanan)
_0xE3:
	SBIC 0x16,3
	RJMP _0xE7
; 0000 055F             {
; 0000 0560              lcd_clear();
	CALL _lcd_clear
; 0000 0561              goto menumode;
	RJMP _0xE8
; 0000 0562             }
; 0000 0563 
; 0000 0564 
; 0000 0565             if(!sw_kiri)
_0xE7:
	SBIC 0x16,1
	RJMP _0xE9
; 0000 0566             {
; 0000 0567              lcd_clear();
	CALL _lcd_clear
; 0000 0568              goto menuarea;
	RJMP _0xEA
; 0000 0569             }
; 0000 056A             if(!sw_cancel) goto exit; else goto part; ;
_0xE9:
	SBIS 0x16,4
	RJMP _0xEC
	RJMP _0xE2
; 0000 056B 
; 0000 056C      menumode:
_0xE8:
; 0000 056D             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 056E             disp_plan();
	RCALL _disp_plan
; 0000 056F             lcd_gotoxy(5,1);
	CALL SUBOPT_0x29
; 0000 0570             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0571             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xEE
; 0000 0572             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xEF
; 0000 0573             {
; 0000 0574               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0575 
; 0000 0576               lcd_clear();
	CALL _lcd_clear
; 0000 0577               while(sw_cancel)
_0xF0:
	SBIS 0x16,4
	RJMP _0xF2
; 0000 0578               {
; 0000 0579                 menu_mode();
	RCALL _menu_mode
; 0000 057A               }
	RJMP _0xF0
_0xF2:
; 0000 057B               lcd_clear();
	CALL SUBOPT_0x59
; 0000 057C               delay_ms(150);
; 0000 057D 
; 0000 057E             }
; 0000 057F             if(!sw_kanan)
_0xEF:
_0xEE:
	SBIC 0x16,3
	RJMP _0xF3
; 0000 0580             {
; 0000 0581              lcd_clear();
	CALL _lcd_clear
; 0000 0582              goto menuarea;
	RJMP _0xEA
; 0000 0583             }
; 0000 0584             if(!sw_kiri)
_0xF3:
	SBIC 0x16,1
	RJMP _0xF4
; 0000 0585             {
; 0000 0586              lcd_clear();
	CALL _lcd_clear
; 0000 0587              goto part;
	RJMP _0xE2
; 0000 0588             }
; 0000 0589 
; 0000 058A             if(!sw_cancel) goto exit; else goto menumode;
_0xF4:
	SBIS 0x16,4
	RJMP _0xEC
	RJMP _0xE8
; 0000 058B 
; 0000 058C      menuarea:
_0xEA:
; 0000 058D             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 058E             disp_plan();
	RCALL _disp_plan
; 0000 058F             lcd_gotoxy(10,1);
	LDI  R30,LOW(10)
	CALL SUBOPT_0x44
; 0000 0590             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0591             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0xF7
; 0000 0592             {
; 0000 0593               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 0594 
; 0000 0595               lcd_clear();
	CALL _lcd_clear
; 0000 0596               while(sw_cancel)
_0xF8:
	SBIS 0x16,4
	RJMP _0xFA
; 0000 0597               {
; 0000 0598                 menu_area();
	RCALL _menu_area
; 0000 0599               }
	RJMP _0xF8
_0xFA:
; 0000 059A               lcd_clear();
	CALL SUBOPT_0x59
; 0000 059B               delay_ms(150);
; 0000 059C 
; 0000 059D             }
; 0000 059E             if(!sw_kanan)
_0xF7:
	SBIC 0x16,3
	RJMP _0xFB
; 0000 059F             {
; 0000 05A0              lcd_clear();
	CALL _lcd_clear
; 0000 05A1              goto part;
	RJMP _0xE2
; 0000 05A2             }
; 0000 05A3 
; 0000 05A4             if(!sw_kiri)
_0xFB:
	SBIC 0x16,1
	RJMP _0xFC
; 0000 05A5             {
; 0000 05A6              lcd_clear();
	CALL _lcd_clear
; 0000 05A7              goto menumode;
	RJMP _0xE8
; 0000 05A8             }
; 0000 05A9 
; 0000 05AA             if(!sw_cancel) goto exit; else goto menuarea;
_0xFC:
	SBIS 0x16,4
	RJMP _0xEC
	RJMP _0xEA
; 0000 05AB      exit:
_0xEC:
; 0000 05AC      }
	RET
;   void menu()
; 0000 05AE    {
_menu:
; 0000 05AF      menudeb:
_0xFF:
; 0000 05B0             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 05B1             disp_menu();
	RCALL _disp_menu
; 0000 05B2             lcd_gotoxy(0,1);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x44
; 0000 05B3             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 05B4             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x100
; 0000 05B5             {
; 0000 05B6               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 05B7 
; 0000 05B8               lcd_clear();
	CALL _lcd_clear
; 0000 05B9               while(sw_cancel)
_0x101:
	SBIS 0x16,4
	RJMP _0x103
; 0000 05BA               {
; 0000 05BB                 menu_debug();
	RCALL _menu_debug
; 0000 05BC               }
	RJMP _0x101
_0x103:
; 0000 05BD               lcd_clear();
	CALL SUBOPT_0x59
; 0000 05BE               delay_ms(150);
; 0000 05BF 
; 0000 05C0             }
; 0000 05C1             if(!sw_kanan)
_0x100:
	SBIC 0x16,3
	RJMP _0x104
; 0000 05C2             {
; 0000 05C3              lcd_clear();
	CALL _lcd_clear
; 0000 05C4              goto menupid;
	RJMP _0x105
; 0000 05C5             }
; 0000 05C6             if(!sw_kiri)
_0x104:
	SBIC 0x16,1
	RJMP _0x106
; 0000 05C7             {
; 0000 05C8              lcd_clear();
	CALL _lcd_clear
; 0000 05C9              goto menuplan;
	RJMP _0x107
; 0000 05CA             }
; 0000 05CB             if(!sw_cancel) goto exit; else goto menudeb;
_0x106:
	SBIS 0x16,4
	RJMP _0x109
	RJMP _0xFF
; 0000 05CC 
; 0000 05CD      menupid:
_0x105:
; 0000 05CE             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 05CF             disp_menu();
	RCALL _disp_menu
; 0000 05D0             lcd_gotoxy(4,1);
	LDI  R30,LOW(4)
	CALL SUBOPT_0x44
; 0000 05D1             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 05D2             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x10B
; 0000 05D3             {
; 0000 05D4               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 05D5 
; 0000 05D6               lcd_clear();
	CALL _lcd_clear
; 0000 05D7               while(sw_cancel)
_0x10C:
	SBIS 0x16,4
	RJMP _0x10E
; 0000 05D8               {
; 0000 05D9                 menu_pid();
	RCALL _menu_pid
; 0000 05DA               }
	RJMP _0x10C
_0x10E:
; 0000 05DB               lcd_clear();
	CALL SUBOPT_0x59
; 0000 05DC               delay_ms(150);
; 0000 05DD 
; 0000 05DE             }
; 0000 05DF             if(!sw_kanan)
_0x10B:
	SBIC 0x16,3
	RJMP _0x10F
; 0000 05E0             {
; 0000 05E1              lcd_clear();
	CALL _lcd_clear
; 0000 05E2              goto menux;
	RJMP _0x110
; 0000 05E3             }
; 0000 05E4             if(!sw_kiri)
_0x10F:
	SBIC 0x16,1
	RJMP _0x111
; 0000 05E5             {
; 0000 05E6              lcd_clear();
	CALL _lcd_clear
; 0000 05E7              goto menudeb;
	RJMP _0xFF
; 0000 05E8             }
; 0000 05E9             if(!sw_cancel) goto exit; else goto menupid;
_0x111:
	SBIS 0x16,4
	RJMP _0x109
	RJMP _0x105
; 0000 05EA 
; 0000 05EB      menux:
_0x110:
; 0000 05EC             delay_ms(150);
	CALL SUBOPT_0x34
; 0000 05ED             disp_menu();
	RCALL _disp_menu
; 0000 05EE             lcd_gotoxy(8,1);
	CALL SUBOPT_0x42
; 0000 05EF             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 05F0             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x114
; 0000 05F1             {
; 0000 05F2               delay_ms(150);
	CALL SUBOPT_0x34
; 0000 05F3               lcd_clear();
	CALL _lcd_clear
; 0000 05F4               lcd_gotoxy(0,0);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x36
; 0000 05F5               lcd_putsf("Status:");
	__POINTW1FN _0x0,153
	CALL SUBOPT_0x2A
; 0000 05F6 
; 0000 05F7               while(sw_ok)
_0x115:
	SBIS 0x16,0
	RJMP _0x117
; 0000 05F8               {
; 0000 05F9                 lihat_adc();
	RCALL _lihat_adc
; 0000 05FA               }
	RJMP _0x115
_0x117:
; 0000 05FB               lcd_clear();
	CALL SUBOPT_0x59
; 0000 05FC               delay_ms(150);
; 0000 05FD 
; 0000 05FE             }
; 0000 05FF             if(!sw_kanan)
_0x114:
	SBIC 0x16,3
	RJMP _0x118
; 0000 0600             {
; 0000 0601              lcd_clear();
	CALL _lcd_clear
; 0000 0602              goto menuplan;
	RJMP _0x107
; 0000 0603             }
; 0000 0604             if(!sw_kiri)
_0x118:
	SBIC 0x16,1
	RJMP _0x119
; 0000 0605             {
; 0000 0606              lcd_clear();
	CALL _lcd_clear
; 0000 0607              goto menupid;
	RJMP _0x105
; 0000 0608             }
; 0000 0609             if(!sw_cancel) goto exit; else goto menux;
_0x119:
	SBIS 0x16,4
	RJMP _0x109
	RJMP _0x110
; 0000 060A 
; 0000 060B      menuplan:
_0x107:
; 0000 060C             delay_ms(200);
	CALL SUBOPT_0x5A
; 0000 060D             disp_menu();
	RCALL _disp_menu
; 0000 060E             lcd_gotoxy(10,1);
	LDI  R30,LOW(10)
	CALL SUBOPT_0x44
; 0000 060F             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0610             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x11C
; 0000 0611             {
; 0000 0612               delay_ms(200);
	CALL SUBOPT_0x5A
; 0000 0613 
; 0000 0614               lcd_clear();
	CALL _lcd_clear
; 0000 0615               while(sw_cancel)
_0x11D:
	SBIS 0x16,4
	RJMP _0x11F
; 0000 0616               {
; 0000 0617                 planning();
	RCALL _planning
; 0000 0618               }
	RJMP _0x11D
_0x11F:
; 0000 0619               lcd_clear();
	CALL SUBOPT_0x5B
; 0000 061A               delay_ms(200);
; 0000 061B 
; 0000 061C             }
; 0000 061D             if(!sw_kanan)
_0x11C:
	SBIC 0x16,3
	RJMP _0x120
; 0000 061E             {
; 0000 061F              lcd_clear();
	CALL _lcd_clear
; 0000 0620              goto menudeb;
	RJMP _0xFF
; 0000 0621             }
; 0000 0622             if(!sw_kiri)
_0x120:
	SBIC 0x16,1
	RJMP _0x121
; 0000 0623             {
; 0000 0624              lcd_clear();
	CALL _lcd_clear
; 0000 0625              goto menux;
	RJMP _0x110
; 0000 0626             }
; 0000 0627             if(!sw_cancel) goto exit; else goto menuplan;
_0x121:
	SBIS 0x16,4
	RJMP _0x109
	RJMP _0x107
; 0000 0628       exit:
_0x109:
; 0000 0629    }
	RET
;   void interface()
; 0000 062B    {
_interface:
; 0000 062C      lcd_gotoxy(1,0);
	LDI  R30,LOW(1)
	CALL SUBOPT_0x36
; 0000 062D      lcd_putsf("-");
	__POINTW1FN _0x0,161
	CALL SUBOPT_0x2A
; 0000 062E      lcd_gotoxy(0,1);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x44
; 0000 062F      lcd_putsf(" CONFIG | START");
	__POINTW1FN _0x0,163
_0x20C0005:
	ST   -Y,R31
	ST   -Y,R30
	CALL _lcd_putsf
; 0000 0630    }
	RET
;   void menuinterface()
; 0000 0632    {
_menuinterface:
; 0000 0633      menusetting:
_0x124:
; 0000 0634             delay_ms(200);
	CALL SUBOPT_0x5A
; 0000 0635             interface();
	RCALL _interface
; 0000 0636             lcd_gotoxy(0,1);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x44
; 0000 0637             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0638             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x125
; 0000 0639             {
; 0000 063A               delay_ms(200);
	CALL SUBOPT_0x5A
; 0000 063B 
; 0000 063C               lcd_clear();
	CALL _lcd_clear
; 0000 063D               while(sw_cancel)
_0x126:
	SBIS 0x16,4
	RJMP _0x128
; 0000 063E               {
; 0000 063F                 menu();
	RCALL _menu
; 0000 0640               }
	RJMP _0x126
_0x128:
; 0000 0641               lcd_clear();
	CALL SUBOPT_0x5B
; 0000 0642               delay_ms(200);
; 0000 0643 
; 0000 0644             }
; 0000 0645             if(!sw_kanan)
_0x125:
	SBIC 0x16,3
	RJMP _0x129
; 0000 0646             {
; 0000 0647              lcd_clear();
	CALL _lcd_clear
; 0000 0648              goto menustart;
	RJMP _0x12A
; 0000 0649             }
; 0000 064A             if(!sw_cancel) goto exit; else goto menusetting;
_0x129:
	SBIS 0x16,4
	RJMP _0x12C
	RJMP _0x124
; 0000 064B 
; 0000 064C      menustart:
_0x12A:
; 0000 064D             delay_ms(125);
	LDI  R30,LOW(125)
	LDI  R31,HIGH(125)
	CALL SUBOPT_0x5C
; 0000 064E             interface();
	RCALL _interface
; 0000 064F             lcd_gotoxy(9,1);
	LDI  R30,LOW(9)
	CALL SUBOPT_0x44
; 0000 0650             lcd_putchar(0);
	CALL SUBOPT_0x56
; 0000 0651             if(!sw_ok)
	SBIC 0x16,0
	RJMP _0x12E
; 0000 0652             {
; 0000 0653               start=1;
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	STS  _start,R30
	STS  _start+1,R31
; 0000 0654               goto exit;
	RJMP _0x12C
; 0000 0655             }
; 0000 0656             if(!sw_kiri)
_0x12E:
	SBIC 0x16,1
	RJMP _0x12F
; 0000 0657             {
; 0000 0658              lcd_clear();
	CALL _lcd_clear
; 0000 0659              goto menusetting;
	RJMP _0x124
; 0000 065A             }
; 0000 065B 
; 0000 065C             if(!sw_cancel) goto exit; else goto menustart;
_0x12F:
	SBIS 0x16,4
	RJMP _0x12C
	RJMP _0x12A
; 0000 065D      exit:
_0x12C:
; 0000 065E      }
	RET
;
;//---------------------END OF MENU----------------------------------------------------//
;//------------------------------------------------------------------------------------//
;//-------------------------ambil nilai ADC--------------------------------------------//
;
;unsigned int lihat_adc()
; 0000 0665 {
_lihat_adc:
; 0000 0666      x=0b00000000;
	CLR  R8
	CLR  R9
; 0000 0667      lcd_gotoxy(2,1);
	CALL SUBOPT_0x41
; 0000 0668      //if((read_adc(0))>(ADC0)){x+=0b10000000;lcd_putchar('1');} else lcd_putchar('0');
; 0000 0669      if((read_adc(1))>(ADC1)){x+=0b01000000;lcd_putchar('1');} else lcd_putchar('0');
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x132
	MOVW R30,R8
	SUBI R30,LOW(-64)
	SBCI R31,HIGH(-64)
	MOVW R8,R30
	LDI  R30,LOW(49)
	RJMP _0x25D
_0x132:
	LDI  R30,LOW(48)
_0x25D:
	ST   -Y,R30
	CALL _lcd_putchar
; 0000 066A      if((read_adc(2))>(ADC2)){x+=0b00100000;lcd_putchar('1');} else lcd_putchar('0');
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x134
	MOVW R30,R8
	ADIW R30,32
	MOVW R8,R30
	LDI  R30,LOW(49)
	RJMP _0x25E
_0x134:
	LDI  R30,LOW(48)
_0x25E:
	ST   -Y,R30
	CALL _lcd_putchar
; 0000 066B      if((read_adc(3))>(ADC3)){x+=0b00010000;lcd_putchar('1');} else lcd_putchar('0');
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x136
	MOVW R30,R8
	ADIW R30,16
	MOVW R8,R30
	LDI  R30,LOW(49)
	RJMP _0x25F
_0x136:
	LDI  R30,LOW(48)
_0x25F:
	ST   -Y,R30
	CALL _lcd_putchar
; 0000 066C      if((read_adc(4))>(ADC4)){x+=0b00001000;lcd_putchar('1');} else lcd_putchar('0');
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x138
	MOVW R30,R8
	ADIW R30,8
	MOVW R8,R30
	LDI  R30,LOW(49)
	RJMP _0x260
_0x138:
	LDI  R30,LOW(48)
_0x260:
	ST   -Y,R30
	CALL _lcd_putchar
; 0000 066D      if((read_adc(5))>(ADC5)){x+=0b00000100;lcd_putchar('1');} else lcd_putchar('0');
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x13A
	MOVW R30,R8
	ADIW R30,4
	MOVW R8,R30
	LDI  R30,LOW(49)
	RJMP _0x261
_0x13A:
	LDI  R30,LOW(48)
_0x261:
	ST   -Y,R30
	CALL _lcd_putchar
; 0000 066E      if((read_adc(6))>(ADC6)){x+=0b00000010;lcd_putchar('1');} else lcd_putchar('0');
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRSH _0x13C
	MOVW R30,R8
	ADIW R30,2
	MOVW R8,R30
	LDI  R30,LOW(49)
	RJMP _0x262
_0x13C:
	LDI  R30,LOW(48)
_0x262:
	ST   -Y,R30
	CALL _lcd_putchar
; 0000 066F     // if((read_adc(7))>(ADC7)){x+=0b00000001;lcd_putchar('1');} else lcd_putchar('0');
; 0000 0670     lcd_gotoxy(0,0) ; if ((read_adc(0))>(ADC0)){lcd_putsf("L:1");} else    lcd_putsf("L:0");
	LDI  R30,LOW(0)
	CALL SUBOPT_0x36
	CALL SUBOPT_0x55
	CALL SUBOPT_0x63
	CP   R30,R26
	CPC  R31,R27
	BRSH _0x13E
	__POINTW1FN _0x0,179
	RJMP _0x263
_0x13E:
	__POINTW1FN _0x0,183
_0x263:
	ST   -Y,R31
	ST   -Y,R30
	CALL _lcd_putsf
; 0000 0671     lcd_gotoxy(3,0) ; if ((read_adc(7))>(ADC7)){lcd_putsf("R:1");} else    lcd_putsf("R:0");
	LDI  R30,LOW(3)
	CALL SUBOPT_0x36
	CALL SUBOPT_0x4A
	CALL SUBOPT_0x64
	CP   R30,R26
	CPC  R31,R27
	BRSH _0x140
	__POINTW1FN _0x0,187
	RJMP _0x264
_0x140:
	__POINTW1FN _0x0,191
_0x264:
	ST   -Y,R31
	ST   -Y,R30
	CALL _lcd_putsf
; 0000 0672      return x;
	MOVW R30,R8
	RET
; 0000 0673 }
;
;//------------------------------------------------------------------------------------//
;
;//-------------------------PROTEKSI MENTOK--------------------------------------------//
;
;
;//--------------------------BACA GARIS n PENENTUAN------------------------------------//
;int MV, P, I, D, PV, mem, error, last_error, rate;
;int var_Kp, var_Ki, var_Kd;
;unsigned char SP = 0;
;
;void trace_garis()
; 0000 0680 {
_trace_garis:
; 0000 0681 
; 0000 0682     int x=lihat_adc();
; 0000 0683     switch(x)
	ST   -Y,R17
	ST   -Y,R16
;	x -> R16,R17
	RCALL _lihat_adc
	MOVW R16,R30
	MOVW R30,R16
; 0000 0684     {
; 0000 0685 
; 0000 0686      //-----------nilai PV(error)-------//
; 0000 0687 
; 0000 0688 
; 0000 0689      //-------------kanan----------------//
; 0000 068A 
; 0000 068B      case 0b01000000 :
	CPI  R30,LOW(0x40)
	LDI  R26,HIGH(0x40)
	CPC  R31,R26
	BRNE _0x145
; 0000 068C                    PV=6;
	LDI  R30,LOW(6)
	LDI  R31,HIGH(6)
	CALL SUBOPT_0x65
; 0000 068D                    mem=6;
	LDI  R30,LOW(6)
	LDI  R31,HIGH(6)
	CALL SUBOPT_0x66
; 0000 068E                    //maju();
; 0000 068F                    break;
	RJMP _0x144
; 0000 0690 
; 0000 0691      case 0b01100000 :
_0x145:
	CPI  R30,LOW(0x60)
	LDI  R26,HIGH(0x60)
	CPC  R31,R26
	BRNE _0x146
; 0000 0692                    PV=5;
	LDI  R30,LOW(5)
	LDI  R31,HIGH(5)
	CALL SUBOPT_0x65
; 0000 0693                    mem=5;
	LDI  R30,LOW(5)
	LDI  R31,HIGH(5)
	CALL SUBOPT_0x66
; 0000 0694                    //maju();
; 0000 0695                    break;
	RJMP _0x144
; 0000 0696 
; 0000 0697 
; 0000 0698      case 0b00100000 :
_0x146:
	CPI  R30,LOW(0x20)
	LDI  R26,HIGH(0x20)
	CPC  R31,R26
	BRNE _0x147
; 0000 0699                    PV=4;
	LDI  R30,LOW(4)
	LDI  R31,HIGH(4)
	CALL SUBOPT_0x65
; 0000 069A                    mem=4;
	LDI  R30,LOW(4)
	LDI  R31,HIGH(4)
	CALL SUBOPT_0x66
; 0000 069B                    //maju();
; 0000 069C                    break;
	RJMP _0x144
; 0000 069D 
; 0000 069E      case 0b00110000 :
_0x147:
	CPI  R30,LOW(0x30)
	LDI  R26,HIGH(0x30)
	CPC  R31,R26
	BRNE _0x148
; 0000 069F                    PV=3;
	LDI  R30,LOW(3)
	LDI  R31,HIGH(3)
	CALL SUBOPT_0x65
; 0000 06A0                    mem=3;
	LDI  R30,LOW(3)
	LDI  R31,HIGH(3)
	CALL SUBOPT_0x66
; 0000 06A1                    //maju();
; 0000 06A2                    break;
	RJMP _0x144
; 0000 06A3 
; 0000 06A4      //-------------TENGAH-----------------//
; 0000 06A5      case 0b00010000 :
_0x148:
	CPI  R30,LOW(0x10)
	LDI  R26,HIGH(0x10)
	CPC  R31,R26
	BRNE _0x149
; 0000 06A6                    PV= 2;
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	CALL SUBOPT_0x65
; 0000 06A7                    mem=2;
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	CALL SUBOPT_0x66
; 0000 06A8                    //maju();
; 0000 06A9                    break;
	RJMP _0x144
; 0000 06AA 
; 0000 06AB      case 0b00011000 :
_0x149:
	CPI  R30,LOW(0x18)
	LDI  R26,HIGH(0x18)
	CPC  R31,R26
	BRNE _0x14A
; 0000 06AC                    PV= 0;
	CALL SUBOPT_0x67
; 0000 06AD                    mem= 0;
	STS  _mem,R30
	STS  _mem+1,R30
; 0000 06AE                   // maju();
; 0000 06AF                    break;
	RJMP _0x144
; 0000 06B0 
; 0000 06B1      case 0b00001000 :
_0x14A:
	CPI  R30,LOW(0x8)
	LDI  R26,HIGH(0x8)
	CPC  R31,R26
	BRNE _0x14B
; 0000 06B2                    PV=-2;
	LDI  R30,LOW(65534)
	LDI  R31,HIGH(65534)
	CALL SUBOPT_0x65
; 0000 06B3                    mem=-2;
	LDI  R30,LOW(65534)
	LDI  R31,HIGH(65534)
	CALL SUBOPT_0x66
; 0000 06B4                    //maju();
; 0000 06B5                    break;
	RJMP _0x144
; 0000 06B6        //--------------KIRI------------------//
; 0000 06B7      case 0b00001100 :
_0x14B:
	CPI  R30,LOW(0xC)
	LDI  R26,HIGH(0xC)
	CPC  R31,R26
	BRNE _0x14C
; 0000 06B8                    PV=-3;
	LDI  R30,LOW(65533)
	LDI  R31,HIGH(65533)
	CALL SUBOPT_0x65
; 0000 06B9                    mem=-3;
	LDI  R30,LOW(65533)
	LDI  R31,HIGH(65533)
	CALL SUBOPT_0x66
; 0000 06BA                   // maju();
; 0000 06BB                    break;
	RJMP _0x144
; 0000 06BC 
; 0000 06BD      case 0b00000100 :
_0x14C:
	CPI  R30,LOW(0x4)
	LDI  R26,HIGH(0x4)
	CPC  R31,R26
	BRNE _0x14D
; 0000 06BE                    PV=-4;
	LDI  R30,LOW(65532)
	LDI  R31,HIGH(65532)
	CALL SUBOPT_0x65
; 0000 06BF                    mem=-4;
	LDI  R30,LOW(65532)
	LDI  R31,HIGH(65532)
	CALL SUBOPT_0x66
; 0000 06C0                    //maju();
; 0000 06C1                    break;
	RJMP _0x144
; 0000 06C2 
; 0000 06C3      case 0b00000110 :
_0x14D:
	CPI  R30,LOW(0x6)
	LDI  R26,HIGH(0x6)
	CPC  R31,R26
	BRNE _0x14E
; 0000 06C4                    PV=-5;
	LDI  R30,LOW(65531)
	LDI  R31,HIGH(65531)
	CALL SUBOPT_0x65
; 0000 06C5                    mem=-5;
	LDI  R30,LOW(65531)
	LDI  R31,HIGH(65531)
	CALL SUBOPT_0x66
; 0000 06C6                   // maju();
; 0000 06C7                    break;
	RJMP _0x144
; 0000 06C8 
; 0000 06C9      case 0b00000010 :
_0x14E:
	CPI  R30,LOW(0x2)
	LDI  R26,HIGH(0x2)
	CPC  R31,R26
	BRNE _0x14F
; 0000 06CA                    PV=-6;
	LDI  R30,LOW(65530)
	LDI  R31,HIGH(65530)
	CALL SUBOPT_0x65
; 0000 06CB                    mem=-6;
	LDI  R30,LOW(65530)
	LDI  R31,HIGH(65530)
	CALL SUBOPT_0x66
; 0000 06CC                   // maju();
; 0000 06CD                    break;
	RJMP _0x144
; 0000 06CE 
; 0000 06CF 
; 0000 06D0 
; 0000 06D1     // UNTUK KANAN
; 0000 06D2 
; 0000 06D3     //LOST OUT OF CONTROL
; 0000 06D4 
; 0000 06D5     case 0b00000000 :
_0x14F:
	SBIW R30,0
	BRNE _0x144
; 0000 06D6 
; 0000 06D7                     if(mem == 0)
	LDS  R30,_mem
	LDS  R31,_mem+1
	SBIW R30,0
	BRNE _0x151
; 0000 06D8                     {motor(maju,maju,MAXPWM,MAXPWM);delay_ms(20);
	CALL SUBOPT_0x68
	LDI  R26,LOW(_MAXPWM)
	LDI  R27,HIGH(_MAXPWM)
	CALL __EEPROMRDB
	ST   -Y,R30
	LDI  R26,LOW(_MAXPWM)
	LDI  R27,HIGH(_MAXPWM)
	CALL __EEPROMRDB
	CALL SUBOPT_0x69
; 0000 06D9                     break;
	RJMP _0x144
; 0000 06DA                     }
; 0000 06DB 
; 0000 06DC                     else if(mem > 0)
_0x151:
	LDS  R26,_mem
	LDS  R27,_mem+1
	CALL __CPW02
	BRGE _0x153
; 0000 06DD                     {
; 0000 06DE                     motor(maju,mundur,60,30);delay_ms(20);
	CALL SUBOPT_0x6A
	CALL SUBOPT_0x69
; 0000 06DF                     break;
	RJMP _0x144
; 0000 06E0 
; 0000 06E1                     }
; 0000 06E2                     else if(mem < 0)
_0x153:
	LDS  R26,_mem+1
	TST  R26
	BRPL _0x155
; 0000 06E3                     {
; 0000 06E4 
; 0000 06E5                     motor(mundur,maju,30,60);delay_ms(20);
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R30,LOW(30)
	ST   -Y,R30
	LDI  R30,LOW(60)
	CALL SUBOPT_0x69
; 0000 06E6                     break;
; 0000 06E7                     }
; 0000 06E8 
; 0000 06E9 
; 0000 06EA 
; 0000 06EB 
; 0000 06EC     }
_0x155:
_0x144:
; 0000 06ED 
; 0000 06EE 
; 0000 06EF 
; 0000 06F0 
; 0000 06F1 
; 0000 06F2     //---------------------------------------//
; 0000 06F3     //---------------P+I+D-------------------//
; 0000 06F4     //---------------------------------------//
; 0000 06F5 
; 0000 06F6     error = SP - PV;
	LDS  R30,_SP
	LDI  R31,0
	LDS  R26,_PV
	LDS  R27,_PV+1
	SUB  R30,R26
	SBC  R31,R27
	STS  _error,R30
	STS  _error+1,R31
; 0000 06F7     P = (var_Kp* error)/10 ;
	CALL SUBOPT_0x6B
	LDS  R26,_var_Kp
	LDS  R27,_var_Kp+1
	CALL SUBOPT_0x6C
	STS  _P,R30
	STS  _P+1,R31
; 0000 06F8 
; 0000 06F9     I = I + error;
	CALL SUBOPT_0x6B
	LDS  R26,_I
	LDS  R27,_I+1
	ADD  R30,R26
	ADC  R31,R27
	STS  _I,R30
	STS  _I+1,R31
; 0000 06FA     I = (I * var_Ki)/10;
	LDS  R30,_var_Ki
	LDS  R31,_var_Ki+1
	LDS  R26,_I
	LDS  R27,_I+1
	CALL SUBOPT_0x6C
	STS  _I,R30
	STS  _I+1,R31
; 0000 06FB 
; 0000 06FC     rate = error - last_error;
	LDS  R26,_last_error
	LDS  R27,_last_error+1
	CALL SUBOPT_0x6B
	SUB  R30,R26
	SBC  R31,R27
	STS  _rate,R30
	STS  _rate+1,R31
; 0000 06FD     D    = (rate * var_Kd)/10;
	LDS  R30,_var_Kd
	LDS  R31,_var_Kd+1
	LDS  R26,_rate
	LDS  R27,_rate+1
	CALL SUBOPT_0x6C
	STS  _D,R30
	STS  _D+1,R31
; 0000 06FE 
; 0000 06FF     last_error = error;
	CALL SUBOPT_0x6B
	STS  _last_error,R30
	STS  _last_error+1,R31
; 0000 0700 
; 0000 0701     MV = P+I+D;
	LDS  R30,_I
	LDS  R31,_I+1
	LDS  R26,_P
	LDS  R27,_P+1
	ADD  R30,R26
	ADC  R31,R27
	LDS  R26,_D
	LDS  R27,_D+1
	ADD  R30,R26
	ADC  R31,R27
	STS  _MV,R30
	STS  _MV+1,R31
; 0000 0702 
; 0000 0703 
; 0000 0704    if (kondisiZ == 0)
	LDS  R30,_kondisiZ
	CPI  R30,0
	BRNE _0x156
; 0000 0705         {DCPWM = 90;}
	LDI  R30,LOW(90)
	LDI  R31,HIGH(90)
	RJMP _0x265
; 0000 0706         else
_0x156:
; 0000 0707         {DCPWM = MAXPWM;}
	LDI  R26,LOW(_MAXPWM)
	LDI  R27,HIGH(_MAXPWM)
	CALL __EEPROMRDW
_0x265:
	STS  _DCPWM,R30
	STS  _DCPWM+1,R31
; 0000 0708 
; 0000 0709 
; 0000 070A     if (MV == 0 )
	CALL SUBOPT_0x6D
	SBIW R30,0
	BRNE _0x158
; 0000 070B     {
; 0000 070C 
; 0000 070D          motor(maju,maju,DCPWM,DCPWM);
	CALL SUBOPT_0x68
	LDS  R30,_DCPWM
	ST   -Y,R30
	RJMP _0x266
; 0000 070E 
; 0000 070F     }
; 0000 0710      else if (MV > 0) //belok kanan
_0x158:
	CALL SUBOPT_0x6E
	CALL __CPW02
	BRLT PC+3
	JMP _0x15A
; 0000 0711     {
; 0000 0712 
; 0000 0713          VARKIRI= DCPWM + ( ( intervalPWM - 50 ) * MV);
	CALL SUBOPT_0x6F
	ADD  R30,R26
	ADC  R31,R27
	MOVW R6,R30
; 0000 0714          VARKANAN= DCPWM + ( ( intervalPWM * MV ) - 30 );
	CALL SUBOPT_0x70
	SBIW R30,30
	LDS  R26,_DCPWM
	LDS  R27,_DCPWM+1
	ADD  R30,R26
	ADC  R31,R27
	MOVW R4,R30
; 0000 0715 
; 0000 0716 
; 0000 0717          if (VARKIRI > DCPWM) {PWMKIRI = DCPWM; }
	CALL SUBOPT_0x71
	CP   R30,R6
	CPC  R31,R7
	BRGE _0x15B
	CALL SUBOPT_0x71
	OUT  0x2A+1,R31
	OUT  0x2A,R30
; 0000 0718         else if (VARKIRI < MINPWM) {VARKIRI = MINPWM; }
	RJMP _0x15C
_0x15B:
	CALL SUBOPT_0x72
	CP   R6,R30
	CPC  R7,R31
	BRGE _0x15D
	CALL SUBOPT_0x72
	MOVW R6,R30
; 0000 0719         else if (VARKIRI < DCPWM) {PWMKIRI = VARKIRI;}
	RJMP _0x15E
_0x15D:
	CALL SUBOPT_0x71
	CP   R6,R30
	CPC  R7,R31
	BRGE _0x15F
	__OUTWR 6,7,42
; 0000 071A 
; 0000 071B 
; 0000 071C         if (VARKANAN  > DCPWM) {PWMKANAN  = DCPWM; }
_0x15F:
_0x15E:
_0x15C:
	CALL SUBOPT_0x71
	CP   R30,R4
	CPC  R31,R5
	BRGE _0x160
	CALL SUBOPT_0x71
	OUT  0x28+1,R31
	OUT  0x28,R30
; 0000 071D          else if (VARKANAN < MINPWM) {PWMKANAN = MINPWM; }
	RJMP _0x161
_0x160:
	CALL SUBOPT_0x72
	CP   R4,R30
	CPC  R5,R31
	BRGE _0x162
	CALL SUBOPT_0x72
	OUT  0x28+1,R31
	OUT  0x28,R30
; 0000 071E          else if (VARKANAN  < DCPWM) {PWMKANAN  = VARKANAN; }
	RJMP _0x163
_0x162:
	CALL SUBOPT_0x71
	CP   R4,R30
	CPC  R5,R31
	BRGE _0x164
	__OUTWR 4,5,40
; 0000 071F 
; 0000 0720        motor(maju,maju,PWMKANAN,PWMKIRI);
_0x164:
_0x163:
_0x161:
	RJMP _0x267
; 0000 0721     }
; 0000 0722 
; 0000 0723     else if (MV < 0)    //belok kiri
_0x15A:
	LDS  R26,_MV+1
	TST  R26
	BRMI PC+3
	JMP _0x166
; 0000 0724     {
; 0000 0725 
; 0000 0726 
; 0000 0727          VARKANAN= DCPWM - ((intervalPWM - 50) * MV);
	CALL SUBOPT_0x6F
	SUB  R26,R30
	SBC  R27,R31
	MOVW R4,R26
; 0000 0728          VARKIRI=(DCPWM - (intervalPWM * MV) - 30) ;
	CALL SUBOPT_0x70
	LDS  R26,_DCPWM
	LDS  R27,_DCPWM+1
	SUB  R26,R30
	SBC  R27,R31
	SBIW R26,30
	MOVW R6,R26
; 0000 0729 
; 0000 072A 
; 0000 072B           if (VARKIRI > DCPWM) {PWMKIRI = DCPWM;}
	CALL SUBOPT_0x71
	CP   R30,R6
	CPC  R31,R7
	BRGE _0x167
	CALL SUBOPT_0x71
	OUT  0x2A+1,R31
	OUT  0x2A,R30
; 0000 072C         else if (VARKIRI < MINPWM) {PWMKIRI = MINPWM;}
	RJMP _0x168
_0x167:
	CALL SUBOPT_0x72
	CP   R6,R30
	CPC  R7,R31
	BRGE _0x169
	CALL SUBOPT_0x72
	OUT  0x2A+1,R31
	OUT  0x2A,R30
; 0000 072D         else if (VARKIRI < DCPWM ) {PWMKIRI = VARKIRI; }
	RJMP _0x16A
_0x169:
	CALL SUBOPT_0x71
	CP   R6,R30
	CPC  R7,R31
	BRGE _0x16B
	__OUTWR 6,7,42
; 0000 072E 
; 0000 072F 
; 0000 0730         if (VARKANAN  > DCPWM) {PWMKANAN  = DCPWM; }
_0x16B:
_0x16A:
_0x168:
	CALL SUBOPT_0x71
	CP   R30,R4
	CPC  R31,R5
	BRGE _0x16C
	CALL SUBOPT_0x71
	OUT  0x28+1,R31
	OUT  0x28,R30
; 0000 0731         else if (VARKANAN < MINPWM) {PWMKANAN = MINPWM; }
	RJMP _0x16D
_0x16C:
	CALL SUBOPT_0x72
	CP   R4,R30
	CPC  R5,R31
	BRGE _0x16E
	CALL SUBOPT_0x72
	OUT  0x28+1,R31
	OUT  0x28,R30
; 0000 0732         else if (VARKANAN  < DCPWM) {PWMKANAN  = VARKANAN; }
	RJMP _0x16F
_0x16E:
	CALL SUBOPT_0x71
	CP   R4,R30
	CPC  R5,R31
	BRGE _0x170
	__OUTWR 4,5,40
; 0000 0733 
; 0000 0734         motor(maju,maju,PWMKANAN,PWMKIRI);
_0x170:
_0x16F:
_0x16D:
_0x267:
	LDI  R30,LOW(0)
	ST   -Y,R30
	ST   -Y,R30
	IN   R30,0x28
	IN   R31,0x28+1
	ST   -Y,R30
	IN   R30,0x2A
	IN   R31,0x2A+1
_0x266:
	ST   -Y,R30
	CALL _motor
; 0000 0735 
; 0000 0736     }
; 0000 0737 
; 0000 0738 
; 0000 0739 
; 0000 073A   //-----------------------------------------------//
; 0000 073B   //-------STATUS PWM WAKRU RACE-------------------//
; 0000 073C   //-----------------------------------------------//
; 0000 073D 
; 0000 073E      dtadc=PWMKANAN;
_0x166:
	IN   R30,0x28
	STS  _dtadc,R30
; 0000 073F      lcd_gotoxy(11,0);
	LDI  R30,LOW(11)
	CALL SUBOPT_0x36
; 0000 0740      sprintf(buf,"L:%d  ",dtadc);
	CALL SUBOPT_0x3D
	__POINTW1FN _0x0,195
	CALL SUBOPT_0x73
; 0000 0741      lcd_puts(buf);
; 0000 0742 
; 0000 0743 
; 0000 0744      dtadc=PWMKIRI;
	IN   R30,0x2A
	STS  _dtadc,R30
; 0000 0745      lcd_gotoxy(11,1);
	LDI  R30,LOW(11)
	CALL SUBOPT_0x44
; 0000 0746      sprintf(buf,"R:%d  ",dtadc);
	CALL SUBOPT_0x3D
	__POINTW1FN _0x0,202
	CALL SUBOPT_0x73
; 0000 0747      lcd_puts(buf);
; 0000 0748 
; 0000 0749      dtadc=kondisiZ;
	LDS  R30,_kondisiZ
	STS  _dtadc,R30
; 0000 074A      lcd_gotoxy(0,1);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x44
; 0000 074B      sprintf(buf,"z%d  ",dtadc);
	CALL SUBOPT_0x3D
	__POINTW1FN _0x0,209
	CALL SUBOPT_0x73
; 0000 074C      lcd_puts(buf);
; 0000 074D 
; 0000 074E 
; 0000 074F   //------------------------------------------------//
; 0000 0750   //------------------------------------------------//
; 0000 0751 
; 0000 0752 }
	LD   R16,Y+
	LD   R17,Y+
	RET
;
;//-----------------------------------------------------------------------------------------------------------//
;
;
;
;
;void main(void)
; 0000 075A {
_main:
; 0000 075B unsigned char cnt,aksi,kondisi;
; 0000 075C PORTD=0x00;
;	cnt -> R17
;	aksi -> R16
;	kondisi -> R19
	LDI  R30,LOW(0)
	OUT  0x12,R30
; 0000 075D DDRD=0x78;
	LDI  R30,LOW(120)
	OUT  0x11,R30
; 0000 075E TCCR1A=0xA1;
	LDI  R30,LOW(161)
	OUT  0x2F,R30
; 0000 075F TCCR1B=0x0D;
	LDI  R30,LOW(13)
	OUT  0x2E,R30
; 0000 0760 TCNT1H=0x00;
	LDI  R30,LOW(0)
	OUT  0x2D,R30
; 0000 0761 TCNT1L=0x00;
	OUT  0x2C,R30
; 0000 0762 OCR1A=0;
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	OUT  0x2A+1,R31
	OUT  0x2A,R30
; 0000 0763 OCR1B=0;
	OUT  0x28+1,R31
	OUT  0x28,R30
; 0000 0764 ACSR=0x80;
	LDI  R30,LOW(128)
	OUT  0x8,R30
; 0000 0765 SFIOR=0x00;
	LDI  R30,LOW(0)
	OUT  0x30,R30
; 0000 0766 
; 0000 0767 ADMUX=ADC_VREF_TYPE & 0xff;
	LDI  R30,LOW(96)
	OUT  0x7,R30
; 0000 0768 ADCSRA=0x84;
	LDI  R30,LOW(132)
	OUT  0x6,R30
; 0000 0769 SFIOR&=0xEF;
	IN   R30,0x30
	ANDI R30,0xEF
	OUT  0x30,R30
; 0000 076A SFIOR|=0x10;
	IN   R30,0x30
	ORI  R30,0x10
	OUT  0x30,R30
; 0000 076B 
; 0000 076C 
; 0000 076D // LCD module initialization
; 0000 076E lcd_init(16);
	LDI  R30,LOW(16)
	ST   -Y,R30
	CALL _lcd_init
; 0000 076F /* define user character 0 */
; 0000 0770 define_char(char0,0);
	LDI  R30,LOW(_char0*2)
	LDI  R31,HIGH(_char0*2)
	ST   -Y,R31
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	CALL _define_char
; 0000 0771 while (kondisidahdiset !=1   )
_0x171:
	LDS  R26,_kondisidahdiset
	LDS  R27,_kondisidahdiset+1
	SBIW R26,1
	BREQ _0x173
; 0000 0772  {
; 0000 0773     lcd_gotoxy(1,0);
	LDI  R30,LOW(1)
	CALL SUBOPT_0x36
; 0000 0774      lcd_putsf("D E M E N T O R");
	CALL SUBOPT_0x58
; 0000 0775      lcd_gotoxy(0,1);
	LDI  R30,LOW(0)
	CALL SUBOPT_0x44
; 0000 0776      lcd_putsf(" AWAL AKHIR SKEN");
	__POINTW1FN _0x0,215
	CALL SUBOPT_0x2A
; 0000 0777       if(!sw_kiri)
	SBIC 0x16,1
	RJMP _0x174
; 0000 0778             {
; 0000 0779               delay_ms(200);
	CALL SUBOPT_0x5A
; 0000 077A                kondisiZ= 2;
	LDI  R30,LOW(2)
	STS  _kondisiZ,R30
; 0000 077B               kondisidahdiset = 1;
	CALL SUBOPT_0x74
; 0000 077C               delay_ms(200);
	CALL SUBOPT_0x5A
; 0000 077D               lcd_clear();
	CALL _lcd_clear
; 0000 077E             }
; 0000 077F       if(!sw_start)
_0x174:
	SBIC 0x16,2
	RJMP _0x175
; 0000 0780             {
; 0000 0781               delay_ms(200);
	CALL SUBOPT_0x5A
; 0000 0782               kondisidahdiset = 1;
	CALL SUBOPT_0x74
; 0000 0783               kondisiZ= 2;
	LDI  R30,LOW(2)
	STS  _kondisiZ,R30
; 0000 0784               lcd_clear();
	CALL SUBOPT_0x5B
; 0000 0785               delay_ms(200);
; 0000 0786             }
; 0000 0787       if(!sw_kanan)
_0x175:
	SBIC 0x16,3
	RJMP _0x176
; 0000 0788             {
; 0000 0789               delay_ms(200);
	CALL SUBOPT_0x5A
; 0000 078A               kondisidahdiset = 1;
	CALL SUBOPT_0x74
; 0000 078B               kondisiZ= 2;
	LDI  R30,LOW(2)
	STS  _kondisiZ,R30
; 0000 078C               lcd_clear();
	CALL SUBOPT_0x5B
; 0000 078D               delay_ms(200);
; 0000 078E 
; 0000 078F             }
; 0000 0790 
; 0000 0791 
; 0000 0792 
; 0000 0793 
; 0000 0794  }
_0x176:
	RJMP _0x171
_0x173:
; 0000 0795     delay_ms(100);
	CALL SUBOPT_0x3E
; 0000 0796 
; 0000 0797 while (start!=1)
_0x177:
	LDS  R26,_start
	LDS  R27,_start+1
	SBIW R26,1
	BREQ _0x179
; 0000 0798  {
; 0000 0799      menuinterface();
	RCALL _menuinterface
; 0000 079A 
; 0000 079B 
; 0000 079C  }
	RJMP _0x177
_0x179:
; 0000 079D 
; 0000 079E 
; 0000 079F var_Kp  = Kp;
	CALL SUBOPT_0x20
	LDI  R31,0
	STS  _var_Kp,R30
	STS  _var_Kp+1,R31
; 0000 07A0 var_Ki  = Ki;
	CALL SUBOPT_0x24
	LDI  R31,0
	STS  _var_Ki,R30
	STS  _var_Ki+1,R31
; 0000 07A1 var_Kd  = Kd;
	CALL SUBOPT_0x26
	LDI  R31,0
	STS  _var_Kd,R30
	STS  _var_Kd+1,R31
; 0000 07A2 intervalPWM = (MAXPWM - MINPWM) / 8; ;
	LDI  R26,LOW(_MAXPWM)
	LDI  R27,HIGH(_MAXPWM)
	CALL __EEPROMRDW
	MOVW R0,R30
	CALL SUBOPT_0x72
	MOVW R26,R0
	SUB  R26,R30
	SBC  R27,R31
	LDI  R30,LOW(8)
	LDI  R31,HIGH(8)
	CALL __DIVW21
	STS  _intervalPWM,R30
	STS  _intervalPWM+1,R31
; 0000 07A3 batas_sensor=batas_sensor;
	LDI  R26,LOW(_batas_sensor)
	LDI  R27,HIGH(_batas_sensor)
	CALL __EEPROMRDW
	LDI  R26,LOW(_batas_sensor)
	LDI  R27,HIGH(_batas_sensor)
	CALL __EEPROMWRW
; 0000 07A4 PV = 0;
	CALL SUBOPT_0x67
; 0000 07A5 MV= 0;
	STS  _MV,R30
	STS  _MV+1,R30
; 0000 07A6 error = 0;
	LDI  R30,LOW(0)
	STS  _error,R30
	STS  _error+1,R30
; 0000 07A7 last_error = 0;
	STS  _last_error,R30
	STS  _last_error+1,R30
; 0000 07A8 
; 0000 07A9 lcd_clear();
	CALL _lcd_clear
; 0000 07AA lcd_gotoxy(5,0);
	LDI  R30,LOW(5)
	CALL SUBOPT_0x36
; 0000 07AB lcd_putsf("Allahuakbar...");
	__POINTW1FN _0x0,232
	CALL SUBOPT_0x2A
; 0000 07AC lcd_gotoxy(4,1);
	LDI  R30,LOW(4)
	CALL SUBOPT_0x44
; 0000 07AD lcd_putsf("Bismillah");
	__POINTW1FN _0x0,247
	CALL SUBOPT_0x2A
; 0000 07AE delay_ms(500);
	LDI  R30,LOW(500)
	LDI  R31,HIGH(500)
	CALL SUBOPT_0x5C
; 0000 07AF lcd_clear();
	CALL _lcd_clear
; 0000 07B0 
; 0000 07B1   motor(maju,maju,0,0);
	CALL SUBOPT_0x68
	CALL SUBOPT_0x68
	CALL _motor
; 0000 07B2 
; 0000 07B3 if (arr[99]==1 && arr[98] == 1)
	CALL SUBOPT_0x75
	BRNE _0x17B
	CALL SUBOPT_0x76
	CPI  R30,LOW(0x1)
	BREQ _0x17C
_0x17B:
	RJMP _0x17A
_0x17C:
; 0000 07B4  {
; 0000 07B5 //======== TULIS MANUAL=============================
; 0000 07B6 arr[100] = 15;//jumlah skenario               //SKENARIO kanan
	CALL SUBOPT_0x77
; 0000 07B7         arr[0] = 2; //1
; 0000 07B8  arr[50]= 3;
; 0000 07B9         arr[1] = 2;//2
	LDI  R30,LOW(2)
	CALL SUBOPT_0x78
; 0000 07BA  arr[51]= 2;
; 0000 07BB         arr[2] = 1;//3
	CALL SUBOPT_0x79
; 0000 07BC  arr[52]= 1;
; 0000 07BD        arr[3] = 2; // 4
	CALL SUBOPT_0x7A
; 0000 07BE  arr[53]= 2;
; 0000 07BF         arr[4] =3;//5
	LDI  R30,LOW(3)
	CALL SUBOPT_0x7B
; 0000 07C0  arr[54]= 2;
	LDI  R30,LOW(2)
	CALL SUBOPT_0x7C
; 0000 07C1        arr[5] = 1;//6 CEK POINT 2
	LDI  R30,LOW(1)
	CALL SUBOPT_0x7D
; 0000 07C2  arr[55]= 1;
	CALL SUBOPT_0x7E
; 0000 07C3         arr[6] = 3;//7
; 0000 07C4  arr[56]= 2;
	LDI  R30,LOW(2)
	CALL SUBOPT_0x7F
; 0000 07C5        arr[7] = 3;// 8
; 0000 07C6  arr[57]= 1;
	LDI  R30,LOW(1)
	CALL __EEPROMWRB
; 0000 07C7         arr[8] = 1; //9  //disini belum fiiiix
	__POINTW2MN _arr,8
	CALL SUBOPT_0x80
; 0000 07C8  arr[58]= 1;
	LDI  R30,LOW(1)
	CALL __EEPROMWRB
; 0000 07C9         arr[9] = 4; //10 ck 2
	__POINTW2MN _arr,9
	LDI  R30,LOW(4)
	CALL __EEPROMWRB
; 0000 07CA  arr[59]= 3;
	__POINTW2MN _arr,59
	CALL SUBOPT_0x81
; 0000 07CB         arr[10] = 4; //11 ck 3
	CALL __EEPROMWRB
; 0000 07CC  arr[60]= 3;
	__POINTW2MN _arr,60
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
; 0000 07CD         arr[11] = 4; //11 ck 3
	__POINTW2MN _arr,11
	LDI  R30,LOW(4)
	CALL __EEPROMWRB
; 0000 07CE  arr[61]= 3;
	__POINTW2MN _arr,61
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
; 0000 07CF         arr[12] = 4; //11 ck 3
	__POINTW2MN _arr,12
	LDI  R30,LOW(4)
	CALL __EEPROMWRB
; 0000 07D0  arr[62]= 3;
	__POINTW2MN _arr,62
	RJMP _0x268
; 0000 07D1  }
; 0000 07D2 
; 0000 07D3 
; 0000 07D4 else if(arr[99]==1 && arr[98]==2)
_0x17A:
	CALL SUBOPT_0x75
	BRNE _0x17F
	CALL SUBOPT_0x76
	CPI  R30,LOW(0x2)
	BREQ _0x180
_0x17F:
	RJMP _0x17E
_0x180:
; 0000 07D5  {
; 0000 07D6  arr[100] = 10;//jumlah skenario
	CALL SUBOPT_0x82
; 0000 07D7         arr[0] = 1; //6 CEK POINT 2
	CALL SUBOPT_0x83
; 0000 07D8  arr[50]= 1;
; 0000 07D9         arr[1] = 3; //
	CALL SUBOPT_0x78
; 0000 07DA  arr[51]= 2;
; 0000 07DB        arr[2] = 3;//8
	CALL SUBOPT_0x84
; 0000 07DC  arr[52]= 1;
; 0000 07DD         arr[3] = 1;//9
	CALL SUBOPT_0x85
; 0000 07DE  arr[53]= 1;
; 0000 07DF         arr[4] = 4;//9
	LDI  R30,LOW(4)
	CALL SUBOPT_0x7B
; 0000 07E0  arr[54]= 3;
	LDI  R30,LOW(3)
	CALL SUBOPT_0x7C
; 0000 07E1         arr[5] = 4;//9
	LDI  R30,LOW(4)
	CALL SUBOPT_0x7D
; 0000 07E2  arr[55]= 1;
	RJMP _0x269
; 0000 07E3 
; 0000 07E4   }
; 0000 07E5 
; 0000 07E6 
; 0000 07E7 
; 0000 07E8 
; 0000 07E9 
; 0000 07EA  else if(arr[99]==1 && arr[98]==3)
_0x17E:
	CALL SUBOPT_0x75
	BRNE _0x183
	CALL SUBOPT_0x76
	CPI  R30,LOW(0x3)
	BREQ _0x184
_0x183:
	RJMP _0x182
_0x184:
; 0000 07EB {
; 0000 07EC arr[100] = 10;//jumlah skenario
	CALL SUBOPT_0x82
; 0000 07ED         arr[0] = 3; //6 CEK POINT 2
	LDI  R30,LOW(3)
	CALL SUBOPT_0x86
; 0000 07EE  arr[50]= 3;
	LDI  R30,LOW(3)
	CALL SUBOPT_0x87
; 0000 07EF         arr[1] = 3; //
; 0000 07F0  arr[51]= 3;
	LDI  R30,LOW(3)
	CALL SUBOPT_0x88
; 0000 07F1        arr[2] = 4;//8
	LDI  R30,LOW(4)
	CALL SUBOPT_0x89
; 0000 07F2  arr[52]= 2;
; 0000 07F3         arr[3] = 4;//9
	LDI  R30,LOW(4)
	CALL SUBOPT_0x8A
; 0000 07F4  arr[53]= 2;
	LDI  R30,LOW(2)
	RJMP _0x269
; 0000 07F5   }
; 0000 07F6 
; 0000 07F7 
; 0000 07F8  else if(arr[99]==1 && arr[98]==4)
_0x182:
	CALL SUBOPT_0x75
	BRNE _0x187
	CALL SUBOPT_0x76
	CPI  R30,LOW(0x4)
	BREQ _0x188
_0x187:
	RJMP _0x186
_0x188:
; 0000 07F9 {
; 0000 07FA arr[100] = 10;//jumlah skenario
	CALL SUBOPT_0x82
; 0000 07FB         arr[0] = 1; //6 CEK POINT 2
	CALL SUBOPT_0x83
; 0000 07FC  arr[50]= 1;
; 0000 07FD         arr[1] = 3; //
	CALL SUBOPT_0x8B
; 0000 07FE  arr[51]= 1;
; 0000 07FF        arr[2] = 3;//8
	CALL SUBOPT_0x84
; 0000 0800  arr[52]= 1;
; 0000 0801         arr[3] = 3;//9
	LDI  R30,LOW(3)
	CALL SUBOPT_0x8A
; 0000 0802  arr[53]= 1;
	LDI  R30,LOW(1)
	CALL __EEPROMWRB
; 0000 0803         arr[4] = 4;//9
	__POINTW2MN _arr,4
	LDI  R30,LOW(4)
	CALL SUBOPT_0x7B
; 0000 0804  arr[54]= 3;
	LDI  R30,LOW(3)
	CALL SUBOPT_0x7C
; 0000 0805         arr[5] = 4;//9
	LDI  R30,LOW(4)
	CALL SUBOPT_0x7D
; 0000 0806  arr[55]= 1;
	RJMP _0x269
; 0000 0807   }
; 0000 0808 
; 0000 0809 
; 0000 080A 
; 0000 080B 
; 0000 080C else if(arr[99]==2 && arr[98]==1)
_0x186:
	CALL SUBOPT_0x8C
	BRNE _0x18B
	CALL SUBOPT_0x76
	CPI  R30,LOW(0x1)
	BREQ _0x18C
_0x18B:
	RJMP _0x18A
_0x18C:
; 0000 080D  {
; 0000 080E  arr[100] = 15;//jumlah skenario               //SKENARIO KIRI
	CALL SUBOPT_0x77
; 0000 080F         arr[0] = 2; //1
; 0000 0810  arr[50]= 3;
; 0000 0811         arr[1] = 1;//2
	LDI  R30,LOW(1)
	CALL SUBOPT_0x8B
; 0000 0812  arr[51]= 1;
; 0000 0813         arr[2] = 2;//3 // CEK POINT
	LDI  R30,LOW(2)
	CALL SUBOPT_0x89
; 0000 0814  arr[52]= 2;
; 0000 0815        arr[3] = 1; // 4
	CALL SUBOPT_0x85
; 0000 0816  arr[53]= 1;
; 0000 0817         arr[4] = 3;//5
	LDI  R30,LOW(3)
	CALL SUBOPT_0x7B
; 0000 0818  arr[54]= 1;
	LDI  R30,LOW(1)
	CALL SUBOPT_0x7C
; 0000 0819        arr[5] = 2;//6 CEK POINT 1
	LDI  R30,LOW(2)
	CALL SUBOPT_0x8D
; 0000 081A  arr[55]= 2;
	LDI  R30,LOW(2)
	CALL SUBOPT_0x7E
; 0000 081B         arr[6] = 3;//7
; 0000 081C  arr[56]= 1;
	LDI  R30,LOW(1)
	CALL SUBOPT_0x7F
; 0000 081D        arr[7] = 3;// 8
; 0000 081E  arr[57]= 2;
	LDI  R30,LOW(2)
	CALL __EEPROMWRB
; 0000 081F        arr[8] = 2;// 9
	__POINTW2MN _arr,8
	CALL SUBOPT_0x80
; 0000 0820 arr[58]= 2;
	LDI  R30,LOW(2)
	CALL __EEPROMWRB
; 0000 0821         arr[9] = 4;// 9  //ck 2
	__POINTW2MN _arr,9
	LDI  R30,LOW(4)
	CALL SUBOPT_0x80
; 0000 0822 arr[58]= 3;
	CALL SUBOPT_0x81
; 0000 0823         arr[10] = 4;// 9  //ck 3
	CALL SUBOPT_0x80
; 0000 0824 arr[58]= 3;
	RJMP _0x268
; 0000 0825  }
; 0000 0826 
; 0000 0827  else if(arr[99]==2 && arr[98]==2)
_0x18A:
	CALL SUBOPT_0x8C
	BRNE _0x18F
	CALL SUBOPT_0x76
	CPI  R30,LOW(0x2)
	BREQ _0x190
_0x18F:
	RJMP _0x18E
_0x190:
; 0000 0828  {
; 0000 0829  arr[100] = 10;//jumlah skenario
	CALL SUBOPT_0x82
; 0000 082A         arr[0] = 2; // 1 CEK POINT 2 dan 3
	LDI  R30,LOW(2)
	CALL SUBOPT_0x86
; 0000 082B  arr[50]= 2;
	LDI  R30,LOW(2)
	CALL SUBOPT_0x87
; 0000 082C         arr[1] = 3; //2
; 0000 082D  arr[51]=1;
	LDI  R30,LOW(1)
	CALL SUBOPT_0x88
; 0000 082E        arr[2] = 3; //3
	LDI  R30,LOW(3)
	CALL SUBOPT_0x89
; 0000 082F  arr[52]= 2;
; 0000 0830         arr[3] = 2;//4
	CALL SUBOPT_0x7A
; 0000 0831  arr[53]= 2;
; 0000 0832         arr[4] = 4;//4
	LDI  R30,LOW(4)
	CALL SUBOPT_0x7B
; 0000 0833  arr[54]= 3;
	LDI  R30,LOW(3)
	CALL SUBOPT_0x7C
; 0000 0834         arr[5] = 4;//4
	LDI  R30,LOW(4)
	CALL SUBOPT_0x8D
; 0000 0835  arr[55]= 3;
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
; 0000 0836         arr[6] = 4;//4
	__POINTW2MN _arr,6
	LDI  R30,LOW(4)
	CALL __EEPROMWRB
; 0000 0837  arr[56]= 3;
	__POINTW2MN _arr,56
	RJMP _0x268
; 0000 0838 
; 0000 0839   }
; 0000 083A 
; 0000 083B   else if(arr[99]==2 && arr[98]==3)
_0x18E:
	CALL SUBOPT_0x8C
	BRNE _0x193
	CALL SUBOPT_0x76
	CPI  R30,LOW(0x3)
	BREQ _0x194
_0x193:
	RJMP _0x192
_0x194:
; 0000 083C  {
; 0000 083D  arr[100] = 5;//jumlah skenario
	__POINTW2MN _arr,100
	LDI  R30,LOW(5)
	CALL __EEPROMWRB
; 0000 083E         arr[0] = 4; // 1
	LDI  R26,LOW(_arr)
	LDI  R27,HIGH(_arr)
	LDI  R30,LOW(4)
	CALL SUBOPT_0x86
; 0000 083F  arr[50]= 3;
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
; 0000 0840         arr[1] = 4; //2
	__POINTW2MN _arr,1
	LDI  R30,LOW(4)
	CALL __EEPROMWRB
; 0000 0841  arr[51]= 3;
	__POINTW2MN _arr,51
	LDI  R30,LOW(3)
	CALL SUBOPT_0x88
; 0000 0842        arr[2] = 1; //3
	CALL SUBOPT_0x79
; 0000 0843  arr[52]= 1;
; 0000 0844         arr[3] = 0;//4
	LDI  R30,LOW(0)
	CALL SUBOPT_0x8A
; 0000 0845  arr[53]= 1;
	LDI  R30,LOW(1)
	RJMP _0x269
; 0000 0846 
; 0000 0847   }
; 0000 0848 
; 0000 0849  else if(arr[99]==2 && arr[98]==4)
_0x192:
	CALL SUBOPT_0x8C
	BRNE _0x197
	CALL SUBOPT_0x76
	CPI  R30,LOW(0x4)
	BREQ _0x198
_0x197:
	RJMP _0x196
_0x198:
; 0000 084A   {
; 0000 084B  arr[100] = 10;//jumlah skenario
	CALL SUBOPT_0x82
; 0000 084C         arr[0] = 2; // 1 CEK POINT 2 dan 3
	LDI  R30,LOW(2)
	CALL SUBOPT_0x86
; 0000 084D  arr[50]= 2;
	LDI  R30,LOW(2)
	CALL __EEPROMWRB
; 0000 084E         arr[1] = 3; //2
	__POINTW2MN _arr,1
	LDI  R30,LOW(3)
	CALL SUBOPT_0x78
; 0000 084F  arr[51]= 2;
; 0000 0850        arr[2] = 3; //3
	LDI  R30,LOW(3)
	CALL SUBOPT_0x89
; 0000 0851  arr[52]= 2;
; 0000 0852         arr[3] = 3;//4
	LDI  R30,LOW(3)
	CALL SUBOPT_0x8A
; 0000 0853  arr[53]= 2;
	LDI  R30,LOW(2)
	CALL __EEPROMWRB
; 0000 0854         arr[4] = 2;//4
	__POINTW2MN _arr,4
	CALL SUBOPT_0x7B
; 0000 0855  arr[54]= 2;
	LDI  R30,LOW(2)
	CALL SUBOPT_0x7C
; 0000 0856         arr[5] = 4;//4
	LDI  R30,LOW(4)
	CALL SUBOPT_0x8D
; 0000 0857  arr[55]= 3;
_0x268:
	LDI  R30,LOW(3)
_0x269:
	CALL __EEPROMWRB
; 0000 0858 
; 0000 0859   }
; 0000 085A 
; 0000 085B 
; 0000 085C 
; 0000 085D 
; 0000 085E while(kondisiZ < 2)
_0x196:
_0x199:
	LDS  R26,_kondisiZ
	CPI  R26,LOW(0x2)
	BRSH _0x19B
; 0000 085F  {
; 0000 0860          if(countZ >= 4)
	LDS  R26,_countZ
	CPI  R26,LOW(0x4)
	BRLO _0x19C
; 0000 0861         {kondisiZ =1;}
	LDI  R30,LOW(1)
	STS  _kondisiZ,R30
; 0000 0862         if (kondisiZ == 1)
_0x19C:
	LDS  R26,_kondisiZ
	CPI  R26,LOW(0x1)
	BRNE _0x19D
; 0000 0863 
; 0000 0864  if((read_adc(3))>(ADC3)&& (read_adc(4))>(ADC4) )
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x19F
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x1A0
_0x19F:
	RJMP _0x19E
_0x1A0:
; 0000 0865       {
; 0000 0866        motor(maju,maju,0,0);delay_ms(100);
	CALL SUBOPT_0x68
	CALL SUBOPT_0x68
	CALL _motor
	CALL SUBOPT_0x3E
; 0000 0867        kondisiZ ++;
	LDS  R30,_kondisiZ
	SUBI R30,-LOW(1)
	STS  _kondisiZ,R30
; 0000 0868       }
; 0000 0869        trace_garis();
_0x19E:
_0x19D:
	RCALL _trace_garis
; 0000 086A  }
	RJMP _0x199
_0x19B:
; 0000 086B 
; 0000 086C   cnt=0;
	LDI  R17,LOW(0)
; 0000 086D  while(cnt<arr[100])
_0x1A1:
	__POINTW2MN _arr,100
	CALL __EEPROMRDB
	CP   R17,R30
	BRLO PC+3
	JMP _0x1A3
; 0000 086E   {
; 0000 086F       kondisi=0;
	LDI  R19,LOW(0)
; 0000 0870 
; 0000 0871 
; 0000 0872       if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4))
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1A5
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1A5
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1A5
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x1A6
_0x1A5:
	RJMP _0x1A4
_0x1A6:
; 0000 0873       {
; 0000 0874       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 0875       }
; 0000 0876       else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
_0x1A4:
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1A9
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1A9
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x1AA
_0x1A9:
	RJMP _0x1A8
_0x1AA:
; 0000 0877       {
; 0000 0878       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 0879       }
; 0000 087A        else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
_0x1A8:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1AD
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1AD
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x1AE
_0x1AD:
	RJMP _0x1AC
_0x1AE:
; 0000 087B       {
; 0000 087C       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 087D       }
; 0000 087E        else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(4)>(ADC4))
_0x1AC:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1B1
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1B1
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x1B2
_0x1B1:
	RJMP _0x1B0
_0x1B2:
; 0000 087F       {
; 0000 0880       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 0881       }
; 0000 0882        else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3))
_0x1B0:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1B5
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1B5
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRLO _0x1B6
_0x1B5:
	RJMP _0x1B4
_0x1B6:
; 0000 0883       {
; 0000 0884       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 0885       }
; 0000 0886        else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3))
_0x1B4:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1B9
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRLO _0x1BA
_0x1B9:
	RJMP _0x1B8
_0x1BA:
; 0000 0887       {
; 0000 0888       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 0889       }
; 0000 088A        else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5) && read_adc(6)>(ADC6))//kaanan
_0x1B8:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1BD
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x1BD
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x1BD
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1BE
_0x1BD:
	RJMP _0x1BC
_0x1BE:
; 0000 088B       {
; 0000 088C       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 088D       }
; 0000 088E        else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
_0x1BC:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1C1
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x1C1
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRLO _0x1C2
_0x1C1:
	RJMP _0x1C0
_0x1C2:
; 0000 088F       {
; 0000 0890       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 0891       }
; 0000 0892        else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(6)>(ADC6))
_0x1C0:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1C5
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x1C5
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1C6
_0x1C5:
	RJMP _0x1C4
_0x1C6:
; 0000 0893       {
; 0000 0894       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 0895       }
; 0000 0896        else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
_0x1C4:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1C9
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x1C9
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1CA
_0x1C9:
	RJMP _0x1C8
_0x1CA:
; 0000 0897       {
; 0000 0898       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 0899       }
; 0000 089A        else if(read_adc(4)>(ADC4) && read_adc(6)>(ADC6))
_0x1C8:
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x1CD
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1CE
_0x1CD:
	RJMP _0x1CC
_0x1CE:
; 0000 089B       {
; 0000 089C       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 089D       }
; 0000 089E        else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
_0x1CC:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1D1
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1D1
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1D1
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x1D1
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x1D1
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1D2
_0x1D1:
	RJMP _0x1D0
_0x1D2:
; 0000 089F       {
; 0000 08A0       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 08A1       }
; 0000 08A2       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
_0x1D0:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1D5
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1D5
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x1D5
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1D6
_0x1D5:
	RJMP _0x1D4
_0x1D6:
; 0000 08A3       {
; 0000 08A4       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 08A5       }
; 0000 08A6       else if(read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
_0x1D4:
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1D9
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1D9
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x1D9
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRLO _0x1DA
_0x1D9:
	RJMP _0x1D8
_0x1DA:
; 0000 08A7       {
; 0000 08A8       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 08A9       }
; 0000 08AA       else if(read_adc(1)>(ADC1)&& read_adc(6)>(ADC6)) //tengah
_0x1D8:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1DD
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1DE
_0x1DD:
	RJMP _0x1DC
_0x1DE:
; 0000 08AB       {
; 0000 08AC       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 08AD       }
; 0000 08AE        else if(read_adc(2)>(ADC2) && read_adc(4)>(ADC4))
_0x1DC:
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1E1
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x1E2
_0x1E1:
	RJMP _0x1E0
_0x1E2:
; 0000 08AF       {
; 0000 08B0       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 08B1       }
; 0000 08B2         else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5))
_0x1E0:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1E5
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRLO _0x1E6
_0x1E5:
	RJMP _0x1E4
_0x1E6:
; 0000 08B3       {
; 0000 08B4       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 08B5       }
; 0000 08B6       else if(read_adc(3)>(ADC3) && read_adc(6)>(ADC6))
_0x1E4:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1E9
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1EA
_0x1E9:
	RJMP _0x1E8
_0x1EA:
; 0000 08B7       {
; 0000 08B8       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 08B9       }
; 0000 08BA       else if(read_adc(1)>(ADC1) && read_adc(4)>(ADC4))
_0x1E8:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1ED
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x1EE
_0x1ED:
	RJMP _0x1EC
_0x1EE:
; 0000 08BB       {
; 0000 08BC       kondisi =3;//01100110
	RJMP _0x26A
; 0000 08BD       }
; 0000 08BE       else if(read_adc(2)>(ADC2) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
_0x1EC:
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1F1
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x1F1
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1F2
_0x1F1:
	RJMP _0x1F0
_0x1F2:
; 0000 08BF       {
; 0000 08C0       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 08C1       }
; 0000 08C2       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5))
_0x1F0:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1F5
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1F5
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRLO _0x1F6
_0x1F5:
	RJMP _0x1F4
_0x1F6:
; 0000 08C3       {
; 0000 08C4       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 08C5       }
; 0000 08C6       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
_0x1F4:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1F9
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1F9
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x1F9
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x1F9
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1FA
_0x1F9:
	RJMP _0x1F8
_0x1FA:
; 0000 08C7       {
; 0000 08C8       kondisi = 3;//01100110
	RJMP _0x26A
; 0000 08C9       }
; 0000 08CA       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
_0x1F8:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x1FD
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x1FD
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x1FD
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x1FD
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x1FE
_0x1FD:
	RJMP _0x1FC
_0x1FE:
; 0000 08CB       {
; 0000 08CC       kondisi = 3;//01100110
_0x26A:
	LDI  R19,LOW(3)
; 0000 08CD       }
; 0000 08CE 
; 0000 08CF 
; 0000 08D0       if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4))
_0x1FC:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x200
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x200
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x200
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x201
_0x200:
	RJMP _0x1FF
_0x201:
; 0000 08D1       {
; 0000 08D2       kondisi = 2;//01100110
	LDI  R19,LOW(2)
; 0000 08D3       }
; 0000 08D4       else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
	RJMP _0x202
_0x1FF:
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x204
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x204
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x205
_0x204:
	RJMP _0x203
_0x205:
; 0000 08D5       {
; 0000 08D6       kondisi = 2;//01100110
	LDI  R19,LOW(2)
; 0000 08D7       }
; 0000 08D8        else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
	RJMP _0x206
_0x203:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x208
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x208
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x209
_0x208:
	RJMP _0x207
_0x209:
; 0000 08D9       {
; 0000 08DA       kondisi = 2;//01100110
	LDI  R19,LOW(2)
; 0000 08DB       }
; 0000 08DC        else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(4)>(ADC4))
	RJMP _0x20A
_0x207:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x20C
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x20C
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x20D
_0x20C:
	RJMP _0x20B
_0x20D:
; 0000 08DD       {
; 0000 08DE       kondisi = 2;//01100110
	LDI  R19,LOW(2)
; 0000 08DF       }
; 0000 08E0        else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3))
	RJMP _0x20E
_0x20B:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x210
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x210
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRLO _0x211
_0x210:
	RJMP _0x20F
_0x211:
; 0000 08E1       {
; 0000 08E2       kondisi = 2;//01100110
	LDI  R19,LOW(2)
; 0000 08E3       }
; 0000 08E4       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5))
	RJMP _0x212
_0x20F:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x214
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x214
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRLO _0x215
_0x214:
	RJMP _0x213
_0x215:
; 0000 08E5       {
; 0000 08E6       kondisi = 2;//01100110
	LDI  R19,LOW(2)
; 0000 08E7       }
; 0000 08E8        else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3))
	RJMP _0x216
_0x213:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x218
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRLO _0x219
_0x218:
	RJMP _0x217
_0x219:
; 0000 08E9       {
; 0000 08EA       kondisi = 2;//01100110
	LDI  R19,LOW(2)
; 0000 08EB       }
; 0000 08EC       else if(read_adc(2)>(ADC2) && read_adc(4)>(ADC4))
	RJMP _0x21A
_0x217:
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x21C
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x21D
_0x21C:
	RJMP _0x21B
_0x21D:
; 0000 08ED       {
; 0000 08EE       kondisi = 2;//01100110
	LDI  R19,LOW(2)
; 0000 08EF       }
; 0000 08F0       else if(read_adc(1)>(ADC1) && read_adc(4)>(ADC4))
	RJMP _0x21E
_0x21B:
	CALL SUBOPT_0x54
	CALL SUBOPT_0x5D
	BRSH _0x220
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRLO _0x221
_0x220:
	RJMP _0x21F
_0x221:
; 0000 08F1       {
; 0000 08F2       kondisi = 2;//01100110
	LDI  R19,LOW(2)
; 0000 08F3       }
; 0000 08F4 
; 0000 08F5 
; 0000 08F6        else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5) && read_adc(6)>(ADC6))//kaanan
	RJMP _0x222
_0x21F:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x224
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x224
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x224
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x225
_0x224:
	RJMP _0x223
_0x225:
; 0000 08F7       {
; 0000 08F8       kondisi = 1;//01100110
	RJMP _0x26B
; 0000 08F9       }
; 0000 08FA        else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
_0x223:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x228
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x228
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRLO _0x229
_0x228:
	RJMP _0x227
_0x229:
; 0000 08FB       {
; 0000 08FC       kondisi = 1;//01100110
	RJMP _0x26B
; 0000 08FD       }
; 0000 08FE        else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(6)>(ADC6))
_0x227:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x22C
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x22C
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x22D
_0x22C:
	RJMP _0x22B
_0x22D:
; 0000 08FF       {
; 0000 0900       kondisi = 1;//01100110
	RJMP _0x26B
; 0000 0901       }
; 0000 0902        else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
_0x22B:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x230
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x230
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x231
_0x230:
	RJMP _0x22F
_0x231:
; 0000 0903       {
; 0000 0904       kondisi = 1;//01100110
	RJMP _0x26B
; 0000 0905       }
; 0000 0906       else if(read_adc(2)>(ADC2) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
_0x22F:
	CALL SUBOPT_0x53
	CALL SUBOPT_0x5E
	BRSH _0x234
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRSH _0x234
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x235
_0x234:
	RJMP _0x233
_0x235:
; 0000 0907       {
; 0000 0908       kondisi = 1;//01100110
	RJMP _0x26B
; 0000 0909       }
; 0000 090A        else if(read_adc(4)>(ADC4) && read_adc(6)>(ADC6))
_0x233:
	CALL SUBOPT_0x50
	CALL SUBOPT_0x60
	BRSH _0x238
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x239
_0x238:
	RJMP _0x237
_0x239:
; 0000 090B       {
; 0000 090C       kondisi = 1;//01100110
	RJMP _0x26B
; 0000 090D       }
; 0000 090E        else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5))
_0x237:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x23C
	CALL SUBOPT_0x4E
	CALL SUBOPT_0x61
	BRLO _0x23D
_0x23C:
	RJMP _0x23B
_0x23D:
; 0000 090F       {
; 0000 0910       kondisi = 1;//01100110
	RJMP _0x26B
; 0000 0911       }
; 0000 0912       else if(read_adc(3)>(ADC3) && read_adc(6)>(ADC6))
_0x23B:
	CALL SUBOPT_0x52
	CALL SUBOPT_0x5F
	BRSH _0x240
	CALL SUBOPT_0x4C
	CALL SUBOPT_0x62
	BRLO _0x241
_0x240:
	RJMP _0x23F
_0x241:
; 0000 0913       {
; 0000 0914       kondisi = 1;//01100110
_0x26B:
	LDI  R19,LOW(1)
; 0000 0915       }
; 0000 0916 
; 0000 0917 
; 0000 0918 
; 0000 0919 
; 0000 091A 
; 0000 091B 
; 0000 091C 
; 0000 091D 
; 0000 091E       /*
; 0000 091F         else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(4)>(ADC4) && read_adc(5)>(ADC5))
; 0000 0920       {
; 0000 0921       kondisi = 3;//01100110
; 0000 0922       }
; 0000 0923       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
; 0000 0924       {
; 0000 0925       kondisi = 3;//01111110
; 0000 0926       }
; 0000 0927       else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4) && read_adc(5)>(ADC5))
; 0000 0928       {
; 0000 0929       kondisi = 3;//00111100
; 0000 092A       }
; 0000 092B       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
; 0000 092C       {
; 0000 092D       kondisi = 3; //01111100
; 0000 092E       }
; 0000 092F       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5) && read_adc(6)>(ADC6))
; 0000 0930       {
; 0000 0931       kondisi = 3;//01100110
; 0000 0932       }
; 0000 0933       else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
; 0000 0934       {
; 0000 0935         kondisi =3;//00111110
; 0000 0936       }
; 0000 0937       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
; 0000 0938       {
; 0000 0939         kondisi =3;//01111100
; 0000 093A       }
; 0000 093B       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
; 0000 093C       {
; 0000 093D         kondisi =3;//00111110
; 0000 093E       }
; 0000 093F       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
; 0000 0940       {
; 0000 0941         kondisi =3;//00111110
; 0000 0942       }
; 0000 0943        else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
; 0000 0944       {
; 0000 0945         kondisi =3;//01111000
; 0000 0946       }
; 0000 0947        else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
; 0000 0948       {
; 0000 0949         kondisi =3;//01111000
; 0000 094A       }
; 0000 094B       else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3))
; 0000 094C       {
; 0000 094D       kondisi = 3;//01010000
; 0000 094E       }
; 0000 094F       else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
; 0000 0950       {
; 0000 0951         kondisi =3;//00110110
; 0000 0952       }
; 0000 0953        else if(read_adc(4)>(ADC4) && read_adc(6)>(ADC6))
; 0000 0954       {
; 0000 0955       kondisi = 3;//00001010
; 0000 0956       }
; 0000 0957       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(6)>(ADC6))
; 0000 0958       {
; 0000 0959       kondisi = 3;//00011010
; 0000 095A       }
; 0000 095B       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
; 0000 095C       {
; 0000 095D       kondisi = 3; //00011110
; 0000 095E       }
; 0000 095F       else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3) && read_adc(4) >(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
; 0000 0960       {
; 0000 0961         kondisi =3;
; 0000 0962       }
; 0000 0963       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
; 0000 0964       {
; 0000 0965         kondisi =3;
; 0000 0966       }
; 0000 0967      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
; 0000 0968       {
; 0000 0969       kondisi = 3;
; 0000 096A       }
; 0000 096B       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&&read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
; 0000 096C       {
; 0000 096D       kondisi = 3;
; 0000 096E       }
; 0000 096F       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) &&read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)> (ADC5)&& read_adc(6)>(ADC6))
; 0000 0970       {
; 0000 0971       kondisi = 3;
; 0000 0972       }
; 0000 0973       else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&&read_adc(5)>(ADC5) && read_adc(6)>(ADC6))
; 0000 0974       {
; 0000 0975       kondisi = 3;
; 0000 0976       }
; 0000 0977       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&&read_adc(4)>(ADC4) && read_adc(5)>(ADC5))
; 0000 0978       {
; 0000 0979       kondisi = 3;
; 0000 097A       }
; 0000 097B 
; 0000 097C       if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
; 0000 097D       {
; 0000 097E         kondisi =1;//00110110
; 0000 097F       }
; 0000 0980        else if(read_adc(4)>(ADC4) && read_adc(6)>(ADC6))
; 0000 0981       {
; 0000 0982       kondisi = 1;//00001010
; 0000 0983       }
; 0000 0984       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(6)>(ADC6))
; 0000 0985       {
; 0000 0986       kondisi = 1;//00011010
; 0000 0987       }
; 0000 0988       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
; 0000 0989       {
; 0000 098A       kondisi = 1; //00011110
; 0000 098B       }
; 0000 098C        else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
; 0000 098D       {
; 0000 098E         kondisi =2;//01111000
; 0000 098F       }
; 0000 0990        else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
; 0000 0991       {
; 0000 0992         kondisi =2;//01111000
; 0000 0993       }
; 0000 0994       else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3))
; 0000 0995       {
; 0000 0996       kondisi = 2;//01010000
; 0000 0997       }
; 0000 0998       else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
; 0000 0999       {
; 0000 099A       kondisi = 2;//01011000
; 0000 099B       }
; 0000 099C 
; 0000 099D      */
; 0000 099E 
; 0000 099F       if (kondisi == arr[cnt])
_0x23F:
_0x222:
_0x21E:
_0x21A:
_0x216:
_0x212:
_0x20E:
_0x20A:
_0x206:
_0x202:
	MOV  R26,R17
	CALL SUBOPT_0x39
	CALL __EEPROMRDB
	CP   R30,R19
	BREQ PC+3
	JMP _0x242
; 0000 09A0       {
; 0000 09A1 
; 0000 09A2                 if (arr[50+cnt] == 1)
	MOV  R30,R17
	CALL SUBOPT_0x2C
	CPI  R30,LOW(0x1)
	BRNE _0x243
; 0000 09A3                 {
; 0000 09A4                       while((read_adc(7))<(ADC7))
_0x244:
	CALL SUBOPT_0x4A
	CALL SUBOPT_0x64
	CP   R26,R30
	CPC  R27,R31
	BRSH _0x246
; 0000 09A5                       {motor(maju,maju,20,20);}
	CALL SUBOPT_0x68
	CALL SUBOPT_0x8E
	RJMP _0x244
_0x246:
; 0000 09A6                       while(read_adc(5)<(ADC5)&& read_adc(6)<(ADC6))
_0x247:
	CALL SUBOPT_0x4E
	MOV  R0,R30
	LDI  R26,LOW(_ADC5)
	LDI  R27,HIGH(_ADC5)
	CALL SUBOPT_0x8F
	BRSH _0x24A
	CALL SUBOPT_0x4C
	MOV  R0,R30
	LDI  R26,LOW(_ADC6)
	LDI  R27,HIGH(_ADC6)
	CALL SUBOPT_0x8F
	BRLO _0x24B
_0x24A:
	RJMP _0x249
_0x24B:
; 0000 09A7                        {motor(mundur,maju,60,30);}
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R30,LOW(60)
	ST   -Y,R30
	LDI  R30,LOW(30)
	ST   -Y,R30
	CALL _motor
	RJMP _0x247
_0x249:
; 0000 09A8                        motor(maju,maju,0,0);delay_ms(30);
	CALL SUBOPT_0x68
	CALL SUBOPT_0x68
	CALL _motor
	LDI  R30,LOW(30)
	LDI  R31,HIGH(30)
	RJMP _0x26C
; 0000 09A9                 }
; 0000 09AA 
; 0000 09AB                 else if(arr[50+cnt] == 2)
_0x243:
	MOV  R30,R17
	CALL SUBOPT_0x2C
	CPI  R30,LOW(0x2)
	BRNE _0x24D
; 0000 09AC                 {     while((read_adc(0))<(ADC0))
_0x24E:
	CALL SUBOPT_0x55
	CALL SUBOPT_0x63
	CP   R26,R30
	CPC  R27,R31
	BRSH _0x250
; 0000 09AD                       {motor(maju,maju,20,20);}
	CALL SUBOPT_0x68
	CALL SUBOPT_0x8E
	RJMP _0x24E
_0x250:
; 0000 09AE                       while(read_adc(1)<(ADC1)&&read_adc(2)<(ADC2))
_0x251:
	CALL SUBOPT_0x54
	MOV  R0,R30
	LDI  R26,LOW(_ADC1)
	LDI  R27,HIGH(_ADC1)
	CALL SUBOPT_0x8F
	BRSH _0x254
	CALL SUBOPT_0x53
	MOV  R0,R30
	LDI  R26,LOW(_ADC2)
	LDI  R27,HIGH(_ADC2)
	CALL SUBOPT_0x8F
	BRLO _0x255
_0x254:
	RJMP _0x253
_0x255:
; 0000 09AF                       {motor(maju,mundur,60,30);}
	CALL SUBOPT_0x6A
	ST   -Y,R30
	CALL _motor
	RJMP _0x251
_0x253:
; 0000 09B0                       motor(maju,maju,0,0);delay_ms(30);
	CALL SUBOPT_0x68
	CALL SUBOPT_0x68
	CALL _motor
	LDI  R30,LOW(30)
	LDI  R31,HIGH(30)
	RJMP _0x26C
; 0000 09B1                 }
; 0000 09B2                 else if(arr[50+cnt] == 3)
_0x24D:
	MOV  R30,R17
	CALL SUBOPT_0x2C
	CPI  R30,LOW(0x3)
	BRNE _0x257
; 0000 09B3                 {    motor(maju,maju,10,10);delay_ms(100) ;
	CALL SUBOPT_0x68
	LDI  R30,LOW(10)
	ST   -Y,R30
	ST   -Y,R30
	CALL _motor
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
_0x26C:
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
; 0000 09B4                 }
; 0000 09B5            cnt++;
_0x257:
	SUBI R17,-1
; 0000 09B6 
; 0000 09B7       }
; 0000 09B8       else
	RJMP _0x258
_0x242:
; 0000 09B9       {
; 0000 09BA            trace_garis();
	RCALL _trace_garis
; 0000 09BB       }
_0x258:
; 0000 09BC         lcd_gotoxy(9,0);
	LDI  R30,LOW(9)
	CALL SUBOPT_0x36
; 0000 09BD       sprintf(buf,"%d  ",cnt);
	CALL SUBOPT_0x37
	MOV  R30,R17
	CALL SUBOPT_0x2D
; 0000 09BE       lcd_puts(buf);
; 0000 09BF 
; 0000 09C0       }
	RJMP _0x1A1
_0x1A3:
; 0000 09C1 
; 0000 09C2 
; 0000 09C3 
; 0000 09C4 
; 0000 09C5 }
_0x259:
	RJMP _0x259
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif

	.CSEG
_put_buff_G100:
	ST   -Y,R17
	ST   -Y,R16
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	ADIW R26,2
	CALL __GETW1P
	SBIW R30,0
	BREQ _0x2000010
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	ADIW R26,4
	CALL __GETW1P
	MOVW R16,R30
	SBIW R30,0
	BREQ _0x2000012
	__CPWRN 16,17,2
	BRLO _0x2000013
	MOVW R30,R16
	SBIW R30,1
	MOVW R16,R30
	__PUTW1SNS 2,4
_0x2000012:
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	ADIW R26,2
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	SBIW R30,1
	LDD  R26,Y+4
	STD  Z+0,R26
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	CALL __GETW1P
	TST  R31
	BRMI _0x2000014
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
_0x2000014:
_0x2000013:
	RJMP _0x2000015
_0x2000010:
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	LDI  R30,LOW(65535)
	LDI  R31,HIGH(65535)
	ST   X+,R30
	ST   X,R31
_0x2000015:
	LDD  R17,Y+1
	LDD  R16,Y+0
	ADIW R28,5
	RET
__print_G100:
	SBIW R28,6
	CALL __SAVELOCR6
	LDI  R17,0
	LDD  R26,Y+12
	LDD  R27,Y+12+1
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	ST   X+,R30
	ST   X,R31
_0x2000016:
	LDD  R30,Y+18
	LDD  R31,Y+18+1
	ADIW R30,1
	STD  Y+18,R30
	STD  Y+18+1,R31
	SBIW R30,1
	LPM  R30,Z
	MOV  R18,R30
	CPI  R30,0
	BRNE PC+3
	JMP _0x2000018
	MOV  R30,R17
	CPI  R30,0
	BRNE _0x200001C
	CPI  R18,37
	BRNE _0x200001D
	LDI  R17,LOW(1)
	RJMP _0x200001E
_0x200001D:
	CALL SUBOPT_0x90
_0x200001E:
	RJMP _0x200001B
_0x200001C:
	CPI  R30,LOW(0x1)
	BRNE _0x200001F
	CPI  R18,37
	BRNE _0x2000020
	CALL SUBOPT_0x90
	RJMP _0x20000C9
_0x2000020:
	LDI  R17,LOW(2)
	LDI  R20,LOW(0)
	LDI  R16,LOW(0)
	CPI  R18,45
	BRNE _0x2000021
	LDI  R16,LOW(1)
	RJMP _0x200001B
_0x2000021:
	CPI  R18,43
	BRNE _0x2000022
	LDI  R20,LOW(43)
	RJMP _0x200001B
_0x2000022:
	CPI  R18,32
	BRNE _0x2000023
	LDI  R20,LOW(32)
	RJMP _0x200001B
_0x2000023:
	RJMP _0x2000024
_0x200001F:
	CPI  R30,LOW(0x2)
	BRNE _0x2000025
_0x2000024:
	LDI  R21,LOW(0)
	LDI  R17,LOW(3)
	CPI  R18,48
	BRNE _0x2000026
	ORI  R16,LOW(128)
	RJMP _0x200001B
_0x2000026:
	RJMP _0x2000027
_0x2000025:
	CPI  R30,LOW(0x3)
	BREQ PC+3
	JMP _0x200001B
_0x2000027:
	CPI  R18,48
	BRLO _0x200002A
	CPI  R18,58
	BRLO _0x200002B
_0x200002A:
	RJMP _0x2000029
_0x200002B:
	LDI  R26,LOW(10)
	MUL  R21,R26
	MOV  R21,R0
	MOV  R30,R18
	SUBI R30,LOW(48)
	ADD  R21,R30
	RJMP _0x200001B
_0x2000029:
	MOV  R30,R18
	CPI  R30,LOW(0x63)
	BRNE _0x200002F
	CALL SUBOPT_0x91
	LDD  R30,Y+16
	LDD  R31,Y+16+1
	LDD  R26,Z+4
	ST   -Y,R26
	CALL SUBOPT_0x92
	RJMP _0x2000030
_0x200002F:
	CPI  R30,LOW(0x73)
	BRNE _0x2000032
	CALL SUBOPT_0x91
	CALL SUBOPT_0x93
	CALL _strlen
	MOV  R17,R30
	RJMP _0x2000033
_0x2000032:
	CPI  R30,LOW(0x70)
	BRNE _0x2000035
	CALL SUBOPT_0x91
	CALL SUBOPT_0x93
	CALL _strlenf
	MOV  R17,R30
	ORI  R16,LOW(8)
_0x2000033:
	ORI  R16,LOW(2)
	ANDI R16,LOW(127)
	LDI  R19,LOW(0)
	RJMP _0x2000036
_0x2000035:
	CPI  R30,LOW(0x64)
	BREQ _0x2000039
	CPI  R30,LOW(0x69)
	BRNE _0x200003A
_0x2000039:
	ORI  R16,LOW(4)
	RJMP _0x200003B
_0x200003A:
	CPI  R30,LOW(0x75)
	BRNE _0x200003C
_0x200003B:
	LDI  R30,LOW(_tbl10_G100*2)
	LDI  R31,HIGH(_tbl10_G100*2)
	STD  Y+6,R30
	STD  Y+6+1,R31
	LDI  R17,LOW(5)
	RJMP _0x200003D
_0x200003C:
	CPI  R30,LOW(0x58)
	BRNE _0x200003F
	ORI  R16,LOW(8)
	RJMP _0x2000040
_0x200003F:
	CPI  R30,LOW(0x78)
	BREQ PC+3
	JMP _0x2000071
_0x2000040:
	LDI  R30,LOW(_tbl16_G100*2)
	LDI  R31,HIGH(_tbl16_G100*2)
	STD  Y+6,R30
	STD  Y+6+1,R31
	LDI  R17,LOW(4)
_0x200003D:
	SBRS R16,2
	RJMP _0x2000042
	CALL SUBOPT_0x91
	CALL SUBOPT_0x94
	LDD  R26,Y+11
	TST  R26
	BRPL _0x2000043
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	CALL __ANEGW1
	STD  Y+10,R30
	STD  Y+10+1,R31
	LDI  R20,LOW(45)
_0x2000043:
	CPI  R20,0
	BREQ _0x2000044
	SUBI R17,-LOW(1)
	RJMP _0x2000045
_0x2000044:
	ANDI R16,LOW(251)
_0x2000045:
	RJMP _0x2000046
_0x2000042:
	CALL SUBOPT_0x91
	CALL SUBOPT_0x94
_0x2000046:
_0x2000036:
	SBRC R16,0
	RJMP _0x2000047
_0x2000048:
	CP   R17,R21
	BRSH _0x200004A
	SBRS R16,7
	RJMP _0x200004B
	SBRS R16,2
	RJMP _0x200004C
	ANDI R16,LOW(251)
	MOV  R18,R20
	SUBI R17,LOW(1)
	RJMP _0x200004D
_0x200004C:
	LDI  R18,LOW(48)
_0x200004D:
	RJMP _0x200004E
_0x200004B:
	LDI  R18,LOW(32)
_0x200004E:
	CALL SUBOPT_0x90
	SUBI R21,LOW(1)
	RJMP _0x2000048
_0x200004A:
_0x2000047:
	MOV  R19,R17
	SBRS R16,1
	RJMP _0x200004F
_0x2000050:
	CPI  R19,0
	BREQ _0x2000052
	SBRS R16,3
	RJMP _0x2000053
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	LPM  R18,Z+
	STD  Y+6,R30
	STD  Y+6+1,R31
	RJMP _0x2000054
_0x2000053:
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	LD   R18,X+
	STD  Y+6,R26
	STD  Y+6+1,R27
_0x2000054:
	CALL SUBOPT_0x90
	CPI  R21,0
	BREQ _0x2000055
	SUBI R21,LOW(1)
_0x2000055:
	SUBI R19,LOW(1)
	RJMP _0x2000050
_0x2000052:
	RJMP _0x2000056
_0x200004F:
_0x2000058:
	LDI  R18,LOW(48)
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	CALL __GETW1PF
	STD  Y+8,R30
	STD  Y+8+1,R31
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	ADIW R30,2
	STD  Y+6,R30
	STD  Y+6+1,R31
_0x200005A:
	LDD  R30,Y+8
	LDD  R31,Y+8+1
	LDD  R26,Y+10
	LDD  R27,Y+10+1
	CP   R26,R30
	CPC  R27,R31
	BRLO _0x200005C
	SUBI R18,-LOW(1)
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	SUB  R30,R26
	SBC  R31,R27
	STD  Y+10,R30
	STD  Y+10+1,R31
	RJMP _0x200005A
_0x200005C:
	CPI  R18,58
	BRLO _0x200005D
	SBRS R16,3
	RJMP _0x200005E
	SUBI R18,-LOW(7)
	RJMP _0x200005F
_0x200005E:
	SUBI R18,-LOW(39)
_0x200005F:
_0x200005D:
	SBRC R16,4
	RJMP _0x2000061
	CPI  R18,49
	BRSH _0x2000063
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,1
	BRNE _0x2000062
_0x2000063:
	RJMP _0x20000CA
_0x2000062:
	CP   R21,R19
	BRLO _0x2000067
	SBRS R16,0
	RJMP _0x2000068
_0x2000067:
	RJMP _0x2000066
_0x2000068:
	LDI  R18,LOW(32)
	SBRS R16,7
	RJMP _0x2000069
	LDI  R18,LOW(48)
_0x20000CA:
	ORI  R16,LOW(16)
	SBRS R16,2
	RJMP _0x200006A
	ANDI R16,LOW(251)
	ST   -Y,R20
	CALL SUBOPT_0x92
	CPI  R21,0
	BREQ _0x200006B
	SUBI R21,LOW(1)
_0x200006B:
_0x200006A:
_0x2000069:
_0x2000061:
	CALL SUBOPT_0x90
	CPI  R21,0
	BREQ _0x200006C
	SUBI R21,LOW(1)
_0x200006C:
_0x2000066:
	SUBI R19,LOW(1)
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,2
	BRLO _0x2000059
	RJMP _0x2000058
_0x2000059:
_0x2000056:
	SBRS R16,0
	RJMP _0x200006D
_0x200006E:
	CPI  R21,0
	BREQ _0x2000070
	SUBI R21,LOW(1)
	LDI  R30,LOW(32)
	ST   -Y,R30
	CALL SUBOPT_0x92
	RJMP _0x200006E
_0x2000070:
_0x200006D:
_0x2000071:
_0x2000030:
_0x20000C9:
	LDI  R17,LOW(0)
_0x200001B:
	RJMP _0x2000016
_0x2000018:
	LDD  R26,Y+12
	LDD  R27,Y+12+1
	CALL __GETW1P
	CALL __LOADLOCR6
	ADIW R28,20
	RET
_sprintf:
	PUSH R15
	MOV  R15,R24
	SBIW R28,6
	CALL __SAVELOCR4
	CALL SUBOPT_0x95
	SBIW R30,0
	BRNE _0x2000072
	LDI  R30,LOW(65535)
	LDI  R31,HIGH(65535)
	RJMP _0x20C0004
_0x2000072:
	MOVW R26,R28
	ADIW R26,6
	CALL __ADDW2R15
	MOVW R16,R26
	CALL SUBOPT_0x95
	STD  Y+6,R30
	STD  Y+6+1,R31
	LDI  R30,LOW(0)
	STD  Y+8,R30
	STD  Y+8+1,R30
	MOVW R26,R28
	ADIW R26,10
	CALL __ADDW2R15
	CALL __GETW1P
	ST   -Y,R31
	ST   -Y,R30
	ST   -Y,R17
	ST   -Y,R16
	LDI  R30,LOW(_put_buff_G100)
	LDI  R31,HIGH(_put_buff_G100)
	ST   -Y,R31
	ST   -Y,R30
	MOVW R30,R28
	ADIW R30,10
	ST   -Y,R31
	ST   -Y,R30
	RCALL __print_G100
	MOVW R18,R30
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	LDI  R30,LOW(0)
	ST   X,R30
	MOVW R30,R18
_0x20C0004:
	CALL __LOADLOCR4
	ADIW R28,10
	POP  R15
	RET

	.CSEG

	.DSEG

	.CSEG
    .equ __lcd_direction=__lcd_port-1
    .equ __lcd_pin=__lcd_port-2
    .equ __lcd_rs=0
    .equ __lcd_rd=1
    .equ __lcd_enable=2
    .equ __lcd_busy_flag=7

	.DSEG

	.CSEG
__lcd_delay_G102:
    ldi   r31,15
__lcd_delay0:
    dec   r31
    brne  __lcd_delay0
	RET
__lcd_ready:
    in    r26,__lcd_direction
    andi  r26,0xf                 ;set as input
    out   __lcd_direction,r26
    sbi   __lcd_port,__lcd_rd     ;RD=1
    cbi   __lcd_port,__lcd_rs     ;RS=0
__lcd_busy:
	RCALL __lcd_delay_G102
    sbi   __lcd_port,__lcd_enable ;EN=1
	RCALL __lcd_delay_G102
    in    r26,__lcd_pin
    cbi   __lcd_port,__lcd_enable ;EN=0
	RCALL __lcd_delay_G102
    sbi   __lcd_port,__lcd_enable ;EN=1
	RCALL __lcd_delay_G102
    cbi   __lcd_port,__lcd_enable ;EN=0
    sbrc  r26,__lcd_busy_flag
    rjmp  __lcd_busy
	RET
__lcd_write_nibble_G102:
    andi  r26,0xf0
    or    r26,r27
    out   __lcd_port,r26          ;write
    sbi   __lcd_port,__lcd_enable ;EN=1
	CALL __lcd_delay_G102
    cbi   __lcd_port,__lcd_enable ;EN=0
	CALL __lcd_delay_G102
	RET
__lcd_write_data:
    cbi  __lcd_port,__lcd_rd 	  ;RD=0
    in    r26,__lcd_direction
    ori   r26,0xf0 | (1<<__lcd_rs) | (1<<__lcd_rd) | (1<<__lcd_enable) ;set as output
    out   __lcd_direction,r26
    in    r27,__lcd_port
    andi  r27,0xf
    ld    r26,y
	RCALL __lcd_write_nibble_G102
    ld    r26,y
    swap  r26
	RCALL __lcd_write_nibble_G102
    sbi   __lcd_port,__lcd_rd     ;RD=1
	JMP  _0x20C0001
_lcd_write_byte:
	CALL __lcd_ready
	LDD  R30,Y+1
	CALL SUBOPT_0x96
    sbi   __lcd_port,__lcd_rs     ;RS=1
	LD   R30,Y
	ST   -Y,R30
	RCALL __lcd_write_data
	RJMP _0x20C0003
__lcd_read_nibble_G102:
    sbi   __lcd_port,__lcd_enable ;EN=1
	CALL __lcd_delay_G102
    in    r30,__lcd_pin           ;read
    cbi   __lcd_port,__lcd_enable ;EN=0
	CALL __lcd_delay_G102
    andi  r30,0xf0
	RET
_lcd_read_byte0_G102:
	CALL __lcd_delay_G102
	RCALL __lcd_read_nibble_G102
    mov   r26,r30
	RCALL __lcd_read_nibble_G102
    cbi   __lcd_port,__lcd_rd     ;RD=0
    swap  r30
    or    r30,r26
	RET
_lcd_gotoxy:
	CALL __lcd_ready
	CALL SUBOPT_0x0
	SUBI R30,LOW(-__base_y_G102)
	SBCI R31,HIGH(-__base_y_G102)
	LD   R30,Z
	LDD  R26,Y+1
	ADD  R30,R26
	ST   -Y,R30
	CALL __lcd_write_data
	LDD  R30,Y+1
	STS  __lcd_x,R30
	LD   R30,Y
	STS  __lcd_y,R30
_0x20C0003:
	ADIW R28,2
	RET
_lcd_clear:
	CALL __lcd_ready
	LDI  R30,LOW(2)
	CALL SUBOPT_0x96
	LDI  R30,LOW(12)
	CALL SUBOPT_0x96
	LDI  R30,LOW(1)
	ST   -Y,R30
	CALL __lcd_write_data
	LDI  R30,LOW(0)
	STS  __lcd_y,R30
	STS  __lcd_x,R30
	RET
_lcd_putchar:
    push r30
    push r31
    ld   r26,y
    set
    cpi  r26,10
    breq __lcd_putchar1
    clt
	LDS  R30,__lcd_maxx
	LDS  R26,__lcd_x
	CP   R26,R30
	BRLO _0x2040004
	__lcd_putchar1:
	LDS  R30,__lcd_y
	SUBI R30,-LOW(1)
	STS  __lcd_y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDS  R30,__lcd_y
	ST   -Y,R30
	RCALL _lcd_gotoxy
	brts __lcd_putchar0
_0x2040004:
	LDS  R30,__lcd_x
	SUBI R30,-LOW(1)
	STS  __lcd_x,R30
    rcall __lcd_ready
    sbi  __lcd_port,__lcd_rs ;RS=1
    ld   r26,y
    st   -y,r26
    rcall __lcd_write_data
__lcd_putchar0:
    pop  r31
    pop  r30
	JMP  _0x20C0001
_lcd_puts:
	ST   -Y,R17
_0x2040005:
	LDD  R26,Y+1
	LDD  R27,Y+1+1
	LD   R30,X+
	STD  Y+1,R26
	STD  Y+1+1,R27
	MOV  R17,R30
	CPI  R30,0
	BREQ _0x2040007
	ST   -Y,R17
	RCALL _lcd_putchar
	RJMP _0x2040005
_0x2040007:
	RJMP _0x20C0002
_lcd_putsf:
	ST   -Y,R17
_0x2040008:
	LDD  R30,Y+1
	LDD  R31,Y+1+1
	ADIW R30,1
	STD  Y+1,R30
	STD  Y+1+1,R31
	SBIW R30,1
	LPM  R30,Z
	MOV  R17,R30
	CPI  R30,0
	BREQ _0x204000A
	ST   -Y,R17
	RCALL _lcd_putchar
	RJMP _0x2040008
_0x204000A:
_0x20C0002:
	LDD  R17,Y+0
	ADIW R28,3
	RET
__long_delay_G102:
    clr   r26
    clr   r27
__long_delay0:
    sbiw  r26,1         ;2 cycles
    brne  __long_delay0 ;2 cycles
	RET
__lcd_init_write_G102:
    cbi  __lcd_port,__lcd_rd 	  ;RD=0
    in    r26,__lcd_direction
    ori   r26,0xf7                ;set as output
    out   __lcd_direction,r26
    in    r27,__lcd_port
    andi  r27,0xf
    ld    r26,y
	CALL __lcd_write_nibble_G102
    sbi   __lcd_port,__lcd_rd     ;RD=1
	RJMP _0x20C0001
_lcd_init:
    cbi   __lcd_port,__lcd_enable ;EN=0
    cbi   __lcd_port,__lcd_rs     ;RS=0
	LD   R30,Y
	STS  __lcd_maxx,R30
	SUBI R30,-LOW(128)
	__PUTB1MN __base_y_G102,2
	LD   R30,Y
	SUBI R30,-LOW(192)
	__PUTB1MN __base_y_G102,3
	CALL SUBOPT_0x97
	CALL SUBOPT_0x97
	CALL SUBOPT_0x97
	RCALL __long_delay_G102
	LDI  R30,LOW(32)
	ST   -Y,R30
	RCALL __lcd_init_write_G102
	RCALL __long_delay_G102
	LDI  R30,LOW(40)
	CALL SUBOPT_0x98
	LDI  R30,LOW(4)
	CALL SUBOPT_0x98
	LDI  R30,LOW(133)
	CALL SUBOPT_0x98
    in    r26,__lcd_direction
    andi  r26,0xf                 ;set as input
    out   __lcd_direction,r26
    sbi   __lcd_port,__lcd_rd     ;RD=1
	CALL _lcd_read_byte0_G102
	CPI  R30,LOW(0x5)
	BREQ _0x204000B
	LDI  R30,LOW(0)
	RJMP _0x20C0001
_0x204000B:
	CALL __lcd_ready
	LDI  R30,LOW(6)
	ST   -Y,R30
	CALL __lcd_write_data
	CALL _lcd_clear
	LDI  R30,LOW(1)
_0x20C0001:
	ADIW R28,1
	RET

	.CSEG

	.CSEG
_strlen:
    ld   r26,y+
    ld   r27,y+
    clr  r30
    clr  r31
strlen0:
    ld   r22,x+
    tst  r22
    breq strlen1
    adiw r30,1
    rjmp strlen0
strlen1:
    ret
_strlenf:
    clr  r26
    clr  r27
    ld   r30,y+
    ld   r31,y+
strlenf0:
	lpm  r0,z+
    tst  r0
    breq strlenf1
    adiw r26,1
    rjmp strlenf0
strlenf1:
    movw r30,r26
    ret

	.CSEG

	.ESEG
_Kp:
	.DB  0xA
_z:
	.DB  0x9
_Ki:
	.DB  0x0
_Kd:
	.DB  0x1E
_batas_sensor:
	.DW  0x78
_ADC0:
	.DW  0x78
_ADC1:
	.DW  0x78
_ADC2:
	.DW  0x78
_ADC3:
	.DW  0x78
_ADC4:
	.DW  0x78
_ADC5:
	.DW  0x78
_ADC6:
	.DW  0x78
_ADC7:
	.DW  0x78
_MAXPWM:
	.DW  0xE6
_MINPWM:
	.DW  0x0
_warna:
	.DW  0x0
_arr:
	.BYTE 0x96

	.DSEG
_intervalPWM:
	.BYTE 0x2
_deledele:
	.BYTE 0x2
_start:
	.BYTE 0x2
_buf:
	.BYTE 0x21
_dtadc:
	.BYTE 0x1
_kondisidahdiset:
	.BYTE 0x2
_countZ:
	.BYTE 0x1
_kondisiZ:
	.BYTE 0x1
_DCPWM:
	.BYTE 0x2
_MV:
	.BYTE 0x2
_P:
	.BYTE 0x2
_I:
	.BYTE 0x2
_D:
	.BYTE 0x2
_PV:
	.BYTE 0x2
_mem:
	.BYTE 0x2
_error:
	.BYTE 0x2
_last_error:
	.BYTE 0x2
_rate:
	.BYTE 0x2
_var_Kp:
	.BYTE 0x2
_var_Ki:
	.BYTE 0x2
_var_Kd:
	.BYTE 0x2
_SP:
	.BYTE 0x1
__seed_G101:
	.BYTE 0x4
__base_y_G102:
	.BYTE 0x4
__lcd_x:
	.BYTE 0x1
__lcd_y:
	.BYTE 0x1
__lcd_maxx:
	.BYTE 0x1

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x0:
	LD   R30,Y
	LDI  R31,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:25 WORDS
SUBOPT_0x1:
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	JMP  _lcd_gotoxy

;OPTIMIZER ADDED SUBROUTINE, CALLED 15 TIMES, CODE SIZE REDUCTION:53 WORDS
SUBOPT_0x2:
	__POINTW1FN _0x0,0
	ST   -Y,R31
	ST   -Y,R30
	JMP  _lcd_putsf

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x3:
	LDI  R26,LOW(_batas_sensor)
	LDI  R27,HIGH(_batas_sensor)
	CALL __EEPROMRDB
	STS  _dtadc,R30
	LDI  R30,LOW(13)
	ST   -Y,R30
	LDI  R30,LOW(0)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 33 TIMES, CODE SIZE REDUCTION:829 WORDS
SUBOPT_0x4:
	ST   -Y,R30
	CALL _lcd_gotoxy
	LDI  R30,LOW(_buf)
	LDI  R31,HIGH(_buf)
	ST   -Y,R31
	ST   -Y,R30
	__POINTW1FN _0x0,2
	ST   -Y,R31
	ST   -Y,R30
	LDS  R30,_dtadc
	CLR  R31
	CLR  R22
	CLR  R23
	CALL __PUTPARD1
	LDI  R24,4
	CALL _sprintf
	ADIW R28,8
	LDI  R30,LOW(_buf)
	LDI  R31,HIGH(_buf)
	ST   -Y,R31
	ST   -Y,R30
	JMP  _lcd_puts

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x5:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_batas_sensor)
	LDI  R27,HIGH(_batas_sensor)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 11 TIMES, CODE SIZE REDUCTION:17 WORDS
SUBOPT_0x6:
	SBIW R30,1
	CALL __EEPROMWRW
	ADIW R30,1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 11 TIMES, CODE SIZE REDUCTION:17 WORDS
SUBOPT_0x7:
	ADIW R30,1
	CALL __EEPROMWRW
	SBIW R30,1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x8:
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	CALL _lcd_gotoxy
	RJMP SUBOPT_0x2

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x9:
	LDI  R26,LOW(_ADC0)
	LDI  R27,HIGH(_ADC0)
	CALL __EEPROMRDB
	STS  _dtadc,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0xA:
	LDI  R30,LOW(1)
	ST   -Y,R30
	RJMP SUBOPT_0x4

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0xB:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_ADC0)
	LDI  R27,HIGH(_ADC0)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0xC:
	LDI  R30,LOW(4)
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	CALL _lcd_gotoxy
	RJMP SUBOPT_0x2

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xD:
	LDI  R26,LOW(_ADC1)
	LDI  R27,HIGH(_ADC1)
	CALL __EEPROMRDB
	STS  _dtadc,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0xE:
	LDI  R30,LOW(5)
	ST   -Y,R30
	LDI  R30,LOW(1)
	RJMP SUBOPT_0x4

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0xF:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_ADC1)
	LDI  R27,HIGH(_ADC1)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0x10:
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	CALL _lcd_gotoxy
	RJMP SUBOPT_0x2

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x11:
	LDI  R26,LOW(_ADC2)
	LDI  R27,HIGH(_ADC2)
	CALL __EEPROMRDB
	STS  _dtadc,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0x12:
	LDI  R30,LOW(9)
	ST   -Y,R30
	LDI  R30,LOW(1)
	RJMP SUBOPT_0x4

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x13:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_ADC2)
	LDI  R27,HIGH(_ADC2)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x14:
	LDI  R30,LOW(12)
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	CALL _lcd_gotoxy
	RJMP SUBOPT_0x2

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x15:
	LDI  R26,LOW(_ADC3)
	LDI  R27,HIGH(_ADC3)
	CALL __EEPROMRDB
	STS  _dtadc,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x16:
	LDI  R30,LOW(13)
	ST   -Y,R30
	LDI  R30,LOW(1)
	RJMP SUBOPT_0x4

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x17:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_ADC3)
	LDI  R27,HIGH(_ADC3)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x18:
	LDI  R26,LOW(_ADC4)
	LDI  R27,HIGH(_ADC4)
	CALL __EEPROMRDB
	STS  _dtadc,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x19:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_ADC4)
	LDI  R27,HIGH(_ADC4)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1A:
	LDI  R26,LOW(_ADC5)
	LDI  R27,HIGH(_ADC5)
	CALL __EEPROMRDB
	STS  _dtadc,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x1B:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_ADC5)
	LDI  R27,HIGH(_ADC5)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1C:
	LDI  R26,LOW(_ADC6)
	LDI  R27,HIGH(_ADC6)
	CALL __EEPROMRDB
	STS  _dtadc,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x1D:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_ADC6)
	LDI  R27,HIGH(_ADC6)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1E:
	LDI  R26,LOW(_ADC7)
	LDI  R27,HIGH(_ADC7)
	CALL __EEPROMRDB
	STS  _dtadc,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x1F:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_ADC7)
	LDI  R27,HIGH(_ADC7)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x20:
	LDI  R26,LOW(_Kp)
	LDI  R27,HIGH(_Kp)
	CALL __EEPROMRDB
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x21:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	RJMP SUBOPT_0x20

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x22:
	SUBI R30,LOW(1)
	CALL __EEPROMWRB
	SUBI R30,-LOW(1)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x23:
	SUBI R30,-LOW(1)
	CALL __EEPROMWRB
	SUBI R30,LOW(1)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x24:
	LDI  R26,LOW(_Ki)
	LDI  R27,HIGH(_Ki)
	CALL __EEPROMRDB
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x25:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	RJMP SUBOPT_0x24

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x26:
	LDI  R26,LOW(_Kd)
	LDI  R27,HIGH(_Kd)
	CALL __EEPROMRDB
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x27:
	STS  _dtadc,R30
	RJMP SUBOPT_0x12

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x28:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	RJMP SUBOPT_0x26

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x29:
	LDI  R30,LOW(5)
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	JMP  _lcd_gotoxy

;OPTIMIZER ADDED SUBROUTINE, CALLED 33 TIMES, CODE SIZE REDUCTION:61 WORDS
SUBOPT_0x2A:
	ST   -Y,R31
	ST   -Y,R30
	JMP  _lcd_putsf

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:33 WORDS
SUBOPT_0x2B:
	LDI  R30,LOW(11)
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	CALL _lcd_gotoxy
	LDI  R30,LOW(_buf)
	LDI  R31,HIGH(_buf)
	ST   -Y,R31
	ST   -Y,R30
	__POINTW1FN _0x0,2
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0x2C:
	LDI  R31,0
	__ADDW1MN _arr,50
	MOVW R26,R30
	CALL __EEPROMRDB
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:114 WORDS
SUBOPT_0x2D:
	CLR  R31
	CLR  R22
	CLR  R23
	CALL __PUTPARD1
	LDI  R24,4
	CALL _sprintf
	ADIW R28,8
	LDI  R30,LOW(_buf)
	LDI  R31,HIGH(_buf)
	ST   -Y,R31
	ST   -Y,R30
	JMP  _lcd_puts

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x2E:
	LDI  R30,LOW(150)
	LDI  R31,HIGH(150)
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	RJMP SUBOPT_0x0

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x2F:
	CALL __EEPROMWRB
	RJMP SUBOPT_0x2B

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x30:
	__GETD1N 0x1
	CALL __PUTPARD1
	LDI  R24,4
	CALL _sprintf
	ADIW R28,8
	LDI  R30,LOW(_buf)
	LDI  R31,HIGH(_buf)
	ST   -Y,R31
	ST   -Y,R30
	JMP  _lcd_puts

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x31:
	__GETD1N 0x2
	CALL __PUTPARD1
	LDI  R24,4
	CALL _sprintf
	ADIW R28,8
	LDI  R30,LOW(_buf)
	LDI  R31,HIGH(_buf)
	ST   -Y,R31
	ST   -Y,R30
	JMP  _lcd_puts

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x32:
	__GETD1N 0x3
	CALL __PUTPARD1
	LDI  R24,4
	CALL _sprintf
	ADIW R28,8
	LDI  R30,LOW(_buf)
	LDI  R31,HIGH(_buf)
	ST   -Y,R31
	ST   -Y,R30
	JMP  _lcd_puts

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x33:
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	CLR  R12
	CLR  R13
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 75 TIMES, CODE SIZE REDUCTION:293 WORDS
SUBOPT_0x34:
	LDI  R30,LOW(150)
	LDI  R31,HIGH(150)
	ST   -Y,R31
	ST   -Y,R30
	JMP  _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x35:
	CALL _lcd_clear
	LDI  R30,LOW(0)
	ST   -Y,R30
	ST   -Y,R30
	CALL _lcd_gotoxy
	LDI  R30,LOW(0)
	ST   -Y,R30
	CALL _lcd_putchar
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	JMP  _lcd_gotoxy

;OPTIMIZER ADDED SUBROUTINE, CALLED 29 TIMES, CODE SIZE REDUCTION:81 WORDS
SUBOPT_0x36:
	ST   -Y,R30
	LDI  R30,LOW(0)
	ST   -Y,R30
	JMP  _lcd_gotoxy

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:51 WORDS
SUBOPT_0x37:
	LDI  R30,LOW(_buf)
	LDI  R31,HIGH(_buf)
	ST   -Y,R31
	ST   -Y,R30
	__POINTW1FN _0x0,2
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:21 WORDS
SUBOPT_0x38:
	LDI  R30,LOW(1)
	ST   -Y,R30
	ST   -Y,R30
	CALL _lcd_gotoxy
	RJMP SUBOPT_0x37

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0x39:
	LDI  R27,0
	SUBI R26,LOW(-_arr)
	SBCI R27,HIGH(-_arr)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x3A:
	CALL __EEPROMRDB
	RJMP SUBOPT_0x2D

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x3B:
	CALL __EEPROMWRB
	RJMP SUBOPT_0x38

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x3C:
	LD   R30,Y
	ST   -Y,R30
	CALL _aksi
	CLR  R12
	CLR  R13
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x3D:
	LDI  R30,LOW(_buf)
	LDI  R31,HIGH(_buf)
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x3E:
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	ST   -Y,R31
	ST   -Y,R30
	JMP  _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x3F:
	LDI  R26,LOW(_z)
	LDI  R27,HIGH(_z)
	CALL __EEPROMRDB
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x40:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	RJMP SUBOPT_0x3F

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x41:
	LDI  R30,LOW(2)
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	JMP  _lcd_gotoxy

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x42:
	LDI  R30,LOW(8)
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	JMP  _lcd_gotoxy

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x43:
	__POINTW1FN _0x0,42
	RJMP SUBOPT_0x2A

;OPTIMIZER ADDED SUBROUTINE, CALLED 21 TIMES, CODE SIZE REDUCTION:57 WORDS
SUBOPT_0x44:
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	JMP  _lcd_gotoxy

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x45:
	LDI  R26,LOW(_MAXPWM)
	LDI  R27,HIGH(_MAXPWM)
	CALL __EEPROMRDB
	RJMP SUBOPT_0x27

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x46:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_MAXPWM)
	LDI  R27,HIGH(_MAXPWM)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x47:
	LDI  R26,LOW(_MINPWM)
	LDI  R27,HIGH(_MINPWM)
	CALL __EEPROMRDB
	STS  _dtadc,R30
	RJMP SUBOPT_0x16

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x48:
	LDS  R30,_deledele
	LDS  R31,_deledele+1
	ST   -Y,R31
	ST   -Y,R30
	CALL _delay_ms
	LDI  R26,LOW(_MINPWM)
	LDI  R27,HIGH(_MINPWM)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x49:
	LDI  R26,LOW(_warna)
	LDI  R27,HIGH(_warna)
	CALL __EEPROMRDW
	CPI  R30,LOW(0x1)
	LDI  R26,HIGH(0x1)
	CPC  R31,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x4A:
	LDI  R30,LOW(7)
	ST   -Y,R30
	JMP  _read_adc

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x4B:
	STS  _dtadc,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(0)
	RJMP SUBOPT_0x4

;OPTIMIZER ADDED SUBROUTINE, CALLED 20 TIMES, CODE SIZE REDUCTION:35 WORDS
SUBOPT_0x4C:
	LDI  R30,LOW(6)
	ST   -Y,R30
	JMP  _read_adc

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x4D:
	STS  _dtadc,R30
	LDI  R30,LOW(5)
	ST   -Y,R30
	LDI  R30,LOW(0)
	RJMP SUBOPT_0x4

;OPTIMIZER ADDED SUBROUTINE, CALLED 20 TIMES, CODE SIZE REDUCTION:35 WORDS
SUBOPT_0x4E:
	LDI  R30,LOW(5)
	ST   -Y,R30
	JMP  _read_adc

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x4F:
	STS  _dtadc,R30
	LDI  R30,LOW(9)
	ST   -Y,R30
	LDI  R30,LOW(0)
	RJMP SUBOPT_0x4

;OPTIMIZER ADDED SUBROUTINE, CALLED 26 TIMES, CODE SIZE REDUCTION:47 WORDS
SUBOPT_0x50:
	LDI  R30,LOW(4)
	ST   -Y,R30
	JMP  _read_adc

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x51:
	STS  _dtadc,R30
	LDI  R30,LOW(13)
	ST   -Y,R30
	LDI  R30,LOW(0)
	RJMP SUBOPT_0x4

;OPTIMIZER ADDED SUBROUTINE, CALLED 28 TIMES, CODE SIZE REDUCTION:51 WORDS
SUBOPT_0x52:
	LDI  R30,LOW(3)
	ST   -Y,R30
	JMP  _read_adc

;OPTIMIZER ADDED SUBROUTINE, CALLED 22 TIMES, CODE SIZE REDUCTION:39 WORDS
SUBOPT_0x53:
	LDI  R30,LOW(2)
	ST   -Y,R30
	JMP  _read_adc

;OPTIMIZER ADDED SUBROUTINE, CALLED 22 TIMES, CODE SIZE REDUCTION:39 WORDS
SUBOPT_0x54:
	LDI  R30,LOW(1)
	ST   -Y,R30
	JMP  _read_adc

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x55:
	LDI  R30,LOW(0)
	ST   -Y,R30
	JMP  _read_adc

;OPTIMIZER ADDED SUBROUTINE, CALLED 25 TIMES, CODE SIZE REDUCTION:45 WORDS
SUBOPT_0x56:
	LDI  R30,LOW(0)
	ST   -Y,R30
	JMP  _lcd_putchar

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x57:
	LDS  R30,_dtadc
	RJMP SUBOPT_0x2D

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x58:
	__POINTW1FN _0x0,104
	RJMP SUBOPT_0x2A

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x59:
	CALL _lcd_clear
	RJMP SUBOPT_0x34

;OPTIMIZER ADDED SUBROUTINE, CALLED 12 TIMES, CODE SIZE REDUCTION:41 WORDS
SUBOPT_0x5A:
	LDI  R30,LOW(200)
	LDI  R31,HIGH(200)
	ST   -Y,R31
	ST   -Y,R30
	JMP  _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x5B:
	CALL _lcd_clear
	RJMP SUBOPT_0x5A

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x5C:
	ST   -Y,R31
	ST   -Y,R30
	JMP  _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 20 TIMES, CODE SIZE REDUCTION:168 WORDS
SUBOPT_0x5D:
	MOV  R0,R30
	LDI  R26,LOW(_ADC1)
	LDI  R27,HIGH(_ADC1)
	CALL __EEPROMRDW
	MOV  R26,R0
	LDI  R27,0
	CP   R30,R26
	CPC  R31,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 20 TIMES, CODE SIZE REDUCTION:168 WORDS
SUBOPT_0x5E:
	MOV  R0,R30
	LDI  R26,LOW(_ADC2)
	LDI  R27,HIGH(_ADC2)
	CALL __EEPROMRDW
	MOV  R26,R0
	LDI  R27,0
	CP   R30,R26
	CPC  R31,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 27 TIMES, CODE SIZE REDUCTION:231 WORDS
SUBOPT_0x5F:
	MOV  R0,R30
	LDI  R26,LOW(_ADC3)
	LDI  R27,HIGH(_ADC3)
	CALL __EEPROMRDW
	MOV  R26,R0
	LDI  R27,0
	CP   R30,R26
	CPC  R31,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 25 TIMES, CODE SIZE REDUCTION:213 WORDS
SUBOPT_0x60:
	MOV  R0,R30
	LDI  R26,LOW(_ADC4)
	LDI  R27,HIGH(_ADC4)
	CALL __EEPROMRDW
	MOV  R26,R0
	LDI  R27,0
	CP   R30,R26
	CPC  R31,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 18 TIMES, CODE SIZE REDUCTION:150 WORDS
SUBOPT_0x61:
	MOV  R0,R30
	LDI  R26,LOW(_ADC5)
	LDI  R27,HIGH(_ADC5)
	CALL __EEPROMRDW
	MOV  R26,R0
	LDI  R27,0
	CP   R30,R26
	CPC  R31,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 18 TIMES, CODE SIZE REDUCTION:150 WORDS
SUBOPT_0x62:
	MOV  R0,R30
	LDI  R26,LOW(_ADC6)
	LDI  R27,HIGH(_ADC6)
	CALL __EEPROMRDW
	MOV  R26,R0
	LDI  R27,0
	CP   R30,R26
	CPC  R31,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x63:
	MOV  R0,R30
	LDI  R26,LOW(_ADC0)
	LDI  R27,HIGH(_ADC0)
	CALL __EEPROMRDW
	MOV  R26,R0
	LDI  R27,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x64:
	MOV  R0,R30
	LDI  R26,LOW(_ADC7)
	LDI  R27,HIGH(_ADC7)
	CALL __EEPROMRDW
	MOV  R26,R0
	LDI  R27,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0x65:
	STS  _PV,R30
	STS  _PV+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0x66:
	STS  _mem,R30
	STS  _mem+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x67:
	LDI  R30,LOW(0)
	STS  _PV,R30
	STS  _PV+1,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 13 TIMES, CODE SIZE REDUCTION:21 WORDS
SUBOPT_0x68:
	LDI  R30,LOW(0)
	ST   -Y,R30
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x69:
	ST   -Y,R30
	CALL _motor
	LDI  R30,LOW(20)
	LDI  R31,HIGH(20)
	RJMP SUBOPT_0x5C

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x6A:
	LDI  R30,LOW(0)
	ST   -Y,R30
	LDI  R30,LOW(1)
	ST   -Y,R30
	LDI  R30,LOW(60)
	ST   -Y,R30
	LDI  R30,LOW(30)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x6B:
	LDS  R30,_error
	LDS  R31,_error+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x6C:
	CALL __MULW12
	MOVW R26,R30
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CALL __DIVW21
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x6D:
	LDS  R30,_MV
	LDS  R31,_MV+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x6E:
	LDS  R26,_MV
	LDS  R27,_MV+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:8 WORDS
SUBOPT_0x6F:
	LDS  R30,_intervalPWM
	LDS  R31,_intervalPWM+1
	SBIW R30,50
	RCALL SUBOPT_0x6E
	CALL __MULW12
	LDS  R26,_DCPWM
	LDS  R27,_DCPWM+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x70:
	RCALL SUBOPT_0x6D
	LDS  R26,_intervalPWM
	LDS  R27,_intervalPWM+1
	CALL __MULW12
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 12 TIMES, CODE SIZE REDUCTION:19 WORDS
SUBOPT_0x71:
	LDS  R30,_DCPWM
	LDS  R31,_DCPWM+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x72:
	LDI  R26,LOW(_MINPWM)
	LDI  R27,HIGH(_MINPWM)
	CALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x73:
	ST   -Y,R31
	ST   -Y,R30
	RJMP SUBOPT_0x57

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x74:
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	STS  _kondisidahdiset,R30
	STS  _kondisidahdiset+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x75:
	__POINTW2MN _arr,99
	CALL __EEPROMRDB
	CPI  R30,LOW(0x1)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x76:
	__POINTW2MN _arr,98
	CALL __EEPROMRDB
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0x77:
	__POINTW2MN _arr,100
	LDI  R30,LOW(15)
	CALL __EEPROMWRB
	LDI  R26,LOW(_arr)
	LDI  R27,HIGH(_arr)
	LDI  R30,LOW(2)
	CALL __EEPROMWRB
	__POINTW2MN _arr,50
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
	__POINTW2MN _arr,1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x78:
	CALL __EEPROMWRB
	__POINTW2MN _arr,51
	LDI  R30,LOW(2)
	CALL __EEPROMWRB
	__POINTW2MN _arr,2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x79:
	LDI  R30,LOW(1)
	CALL __EEPROMWRB
	__POINTW2MN _arr,52
	CALL __EEPROMWRB
	__POINTW2MN _arr,3
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x7A:
	LDI  R30,LOW(2)
	CALL __EEPROMWRB
	__POINTW2MN _arr,53
	CALL __EEPROMWRB
	__POINTW2MN _arr,4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x7B:
	CALL __EEPROMWRB
	__POINTW2MN _arr,54
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x7C:
	CALL __EEPROMWRB
	__POINTW2MN _arr,5
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x7D:
	CALL __EEPROMWRB
	__POINTW2MN _arr,55
	LDI  R30,LOW(1)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x7E:
	CALL __EEPROMWRB
	__POINTW2MN _arr,6
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
	__POINTW2MN _arr,56
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x7F:
	CALL __EEPROMWRB
	__POINTW2MN _arr,7
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
	__POINTW2MN _arr,57
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x80:
	CALL __EEPROMWRB
	__POINTW2MN _arr,58
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x81:
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
	__POINTW2MN _arr,10
	LDI  R30,LOW(4)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:17 WORDS
SUBOPT_0x82:
	__POINTW2MN _arr,100
	LDI  R30,LOW(10)
	CALL __EEPROMWRB
	LDI  R26,LOW(_arr)
	LDI  R27,HIGH(_arr)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x83:
	LDI  R30,LOW(1)
	CALL __EEPROMWRB
	__POINTW2MN _arr,50
	CALL __EEPROMWRB
	__POINTW2MN _arr,1
	LDI  R30,LOW(3)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x84:
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
	__POINTW2MN _arr,52
	LDI  R30,LOW(1)
	CALL __EEPROMWRB
	__POINTW2MN _arr,3
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x85:
	LDI  R30,LOW(1)
	CALL __EEPROMWRB
	__POINTW2MN _arr,53
	CALL __EEPROMWRB
	__POINTW2MN _arr,4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x86:
	CALL __EEPROMWRB
	__POINTW2MN _arr,50
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x87:
	CALL __EEPROMWRB
	__POINTW2MN _arr,1
	LDI  R30,LOW(3)
	CALL __EEPROMWRB
	__POINTW2MN _arr,51
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x88:
	CALL __EEPROMWRB
	__POINTW2MN _arr,2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:18 WORDS
SUBOPT_0x89:
	CALL __EEPROMWRB
	__POINTW2MN _arr,52
	LDI  R30,LOW(2)
	CALL __EEPROMWRB
	__POINTW2MN _arr,3
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x8A:
	CALL __EEPROMWRB
	__POINTW2MN _arr,53
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x8B:
	CALL __EEPROMWRB
	__POINTW2MN _arr,51
	LDI  R30,LOW(1)
	RJMP SUBOPT_0x88

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x8C:
	__POINTW2MN _arr,99
	CALL __EEPROMRDB
	CPI  R30,LOW(0x2)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x8D:
	CALL __EEPROMWRB
	__POINTW2MN _arr,55
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x8E:
	LDI  R30,LOW(20)
	ST   -Y,R30
	ST   -Y,R30
	JMP  _motor

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0x8F:
	CALL __EEPROMRDW
	MOV  R26,R0
	LDI  R27,0
	CP   R26,R30
	CPC  R27,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:21 WORDS
SUBOPT_0x90:
	ST   -Y,R18
	LDD  R30,Y+13
	LDD  R31,Y+13+1
	ST   -Y,R31
	ST   -Y,R30
	LDD  R30,Y+17
	LDD  R31,Y+17+1
	ICALL
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x91:
	LDD  R30,Y+16
	LDD  R31,Y+16+1
	SBIW R30,4
	STD  Y+16,R30
	STD  Y+16+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x92:
	LDD  R30,Y+13
	LDD  R31,Y+13+1
	ST   -Y,R31
	ST   -Y,R30
	LDD  R30,Y+17
	LDD  R31,Y+17+1
	ICALL
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x93:
	LDD  R26,Y+16
	LDD  R27,Y+16+1
	ADIW R26,4
	CALL __GETW1P
	STD  Y+6,R30
	STD  Y+6+1,R31
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x94:
	LDD  R26,Y+16
	LDD  R27,Y+16+1
	ADIW R26,4
	CALL __GETW1P
	STD  Y+10,R30
	STD  Y+10+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x95:
	MOVW R26,R28
	ADIW R26,12
	CALL __ADDW2R15
	CALL __GETW1P
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x96:
	ST   -Y,R30
	CALL __lcd_write_data
	JMP  __lcd_ready

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x97:
	CALL __long_delay_G102
	LDI  R30,LOW(48)
	ST   -Y,R30
	JMP  __lcd_init_write_G102

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x98:
	ST   -Y,R30
	CALL __lcd_write_data
	JMP  __long_delay_G102


	.CSEG
_delay_ms:
	ld   r30,y+
	ld   r31,y+
	adiw r30,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0xBB8
	wdr
	sbiw r30,1
	brne __delay_ms0
__delay_ms1:
	ret

__ADDW2R15:
	CLR  R0
	ADD  R26,R15
	ADC  R27,R0
	RET

__ANEGW1:
	NEG  R31
	NEG  R30
	SBCI R31,0
	RET

__MULW12U:
	MUL  R31,R26
	MOV  R31,R0
	MUL  R30,R27
	ADD  R31,R0
	MUL  R30,R26
	MOV  R30,R0
	ADD  R31,R1
	RET

__MULW12:
	RCALL __CHKSIGNW
	RCALL __MULW12U
	BRTC __MULW121
	RCALL __ANEGW1
__MULW121:
	RET

__DIVW21U:
	CLR  R0
	CLR  R1
	LDI  R25,16
__DIVW21U1:
	LSL  R26
	ROL  R27
	ROL  R0
	ROL  R1
	SUB  R0,R30
	SBC  R1,R31
	BRCC __DIVW21U2
	ADD  R0,R30
	ADC  R1,R31
	RJMP __DIVW21U3
__DIVW21U2:
	SBR  R26,1
__DIVW21U3:
	DEC  R25
	BRNE __DIVW21U1
	MOVW R30,R26
	MOVW R26,R0
	RET

__DIVW21:
	RCALL __CHKSIGNW
	RCALL __DIVW21U
	BRTC __DIVW211
	RCALL __ANEGW1
__DIVW211:
	RET

__CHKSIGNW:
	CLT
	SBRS R31,7
	RJMP __CHKSW1
	RCALL __ANEGW1
	SET
__CHKSW1:
	SBRS R27,7
	RJMP __CHKSW2
	COM  R26
	COM  R27
	ADIW R26,1
	BLD  R0,0
	INC  R0
	BST  R0,0
__CHKSW2:
	RET

__GETW1P:
	LD   R30,X+
	LD   R31,X
	SBIW R26,1
	RET

__GETW1PF:
	LPM  R0,Z+
	LPM  R31,Z
	MOV  R30,R0
	RET

__PUTPARD1:
	ST   -Y,R23
	ST   -Y,R22
	ST   -Y,R31
	ST   -Y,R30
	RET

__EEPROMRDW:
	ADIW R26,1
	RCALL __EEPROMRDB
	MOV  R31,R30
	SBIW R26,1

__EEPROMRDB:
	SBIC EECR,EEWE
	RJMP __EEPROMRDB
	PUSH R31
	IN   R31,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R30,EEDR
	OUT  SREG,R31
	POP  R31
	RET

__EEPROMWRW:
	RCALL __EEPROMWRB
	ADIW R26,1
	PUSH R30
	MOV  R30,R31
	RCALL __EEPROMWRB
	POP  R30
	SBIW R26,1
	RET

__EEPROMWRB:
	SBIS EECR,EEWE
	RJMP __EEPROMWRB1
	WDR
	RJMP __EEPROMWRB
__EEPROMWRB1:
	IN   R25,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R24,EEDR
	CP   R30,R24
	BREQ __EEPROMWRB0
	OUT  EEDR,R30
	SBI  EECR,EEMWE
	SBI  EECR,EEWE
__EEPROMWRB0:
	OUT  SREG,R25
	RET

__CPW02:
	CLR  R0
	CP   R0,R26
	CPC  R0,R27
	RET

__SAVELOCR6:
	ST   -Y,R21
__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR6:
	LDD  R21,Y+5
__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

;END OF CODE MARKER
__END_OF_CODE:
