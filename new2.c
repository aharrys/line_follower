/*****************************************************
This program was produced by the
CodeWizardAVR V2.03.9 Standard
Automatic Program Generator
� Copyright 1998-2008 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : 
Version : 
Date    : 28/11/2011
Author  : Akhmad Harry Susanto
Company : Unila Robotika Dan Otomasi
Comments: 


Chip type               : ATmega32
Program type            : Application
AVR Core Clock frequency: 12,000000 MHz
Memory model            : Small
External RAM size       : 0
Data Stack size         : 512
*****************************************************/

#include <mega32.h>
#include <stdio.h>
#include <stdlib.h>
#include <delay.h>
// Alphanumeric LCD Module functions
#asm
   .equ __lcd_port=0x15 ;PORTC
#endasm
#include <lcd.h>

//---------GAMBAR PANAH------------------------//
typedef unsigned char byte;
flash byte char0[8]={
0b1100000,
0b0011000,
0b0000110,
0b1111111,
0b1111111,
0b0000110,
0b0011000,
0b1100000};

void define_char(byte flash *pc,byte char_code)
{
byte i,a;
a=(char_code<<3) | 0x40;
for (i=0; i<8; i++) lcd_write_byte(a++,*pc++);
}
//-----------------------------------------------//
//---------------Debug switch---------------------------//
#define sw_ok     PINB.0
#define sw_cancel PINB.4
#define sw_kiri   PINB.1
#define sw_kanan  PINB.3  
#define sw_start  PINB.2 

//-----------------port motor----------------------------//
#define kiri    PORTD.3
#define kanan   PORTD.6

//------------------enable motor-------------------------//
#define PWMKIRI       OCR1A
#define PWMKANAN      OCR1B  

//-------------sensor tambahan---------------------------//

#define maju    0
#define mundur  1
//-------------------------------------------------------//


 
void menu();
void trace_garis();  
unsigned int lihat_adc();




//-----------------Variabel Global----------------------------------//

eeprom unsigned char Kp = 10;
eeprom unsigned char z = 9; 
eeprom unsigned char Ki = 0;
eeprom unsigned char Kd = 30; 
eeprom unsigned int  batas_sensor=120;
//eeprom unsigned int  X=4;
eeprom unsigned int  ADC0=120;
eeprom unsigned int  ADC1=120;
eeprom unsigned int  ADC2=120;
eeprom unsigned int  ADC3=120;
eeprom unsigned int  ADC4=120;
eeprom unsigned int  ADC5=120;
eeprom unsigned int  ADC6=120;
eeprom unsigned int  ADC7=120;
eeprom int MAXPWM  = 230;
eeprom int MINPWM = 0;
//eeprom int skenario=1;
eeprom int warna=0; //0:hitam; 1:putih;
eeprom unsigned char arr[150];


signed int VARKANAN;
signed int VARKIRI;
int x=0b00000000;
int counter=0,terus; 
int intervalPWM;
int deledele=100; 
int start=0;
unsigned char buf[33];
unsigned char dtadc;
int kondisidahdiset=0;
char countZ;
unsigned char kondisiZ = 0;
byte PWMZ = 40;
int DCPWM;

//-------------------------ADC GENERATOR-------------------------------//

#define ADC_VREF_TYPE 0x60
// Read the 8 most significant bits
// of the AD conversion result
unsigned char read_adc(unsigned char adc_input)
{
ADMUX=adc_input | (ADC_VREF_TYPE & 0xff);
// Delay needed for the stabilization of the ADC input voltage
delay_us(10);
// Start the AD conversion
ADCSRA|=0x40;
// Wait for the AD conversion to complete
while ((ADCSRA & 0x10)==0);
ADCSRA|=0x10;
return ADCH;
}

//----------------MOTOR DIRECTION-------------------------------------//
//--------ma=motor atas(kemudi) mb=motor bawah------------------------// 

    void motor(char xka,char xki,unsigned char Vkanan,unsigned char vkiri)
    {
     kanan=xka;
     kiri=xki;
     PWMKANAN=Vkanan;
     PWMKIRI=vkiri;
    
    } 
               
 

//------------------------M.E.N.U------------------------------//
//-------------------------------------------------------------//
//-----------------SETTING VARIABEL--------------------//

 
  void set_def()
  {
      lcd_gotoxy(8,0);
      lcd_putsf(">");
      dtadc=batas_sensor; 
                          
      lcd_gotoxy(13,0);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              batas_sensor--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              batas_sensor++;
         }
  }
   void set_ADC0()
  {
      lcd_gotoxy(0,1);
      lcd_putsf(">");
      dtadc=ADC0; 
                          
      lcd_gotoxy(1,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              ADC0--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              ADC0++;
         }
  }
    void set_ADC1()
  {
      lcd_gotoxy(4,1);
      lcd_putsf(">");
      dtadc=ADC1; 
                          
      lcd_gotoxy(5,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              ADC1--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              ADC1++;
         }
  }
     void set_ADC2()
  {
      lcd_gotoxy(8,1);
      lcd_putsf(">");
      dtadc=ADC2; 
                          
      lcd_gotoxy(9,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              ADC2--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              ADC2++;
         }
  }
     void set_ADC3()
  {
      lcd_gotoxy(12,1);
      lcd_putsf(">");
      dtadc=ADC3; 
                          
      lcd_gotoxy(13,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              ADC3--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              ADC3++;
         }
  }
    void set_ADC4()
  {
      lcd_gotoxy(0,1);
      lcd_putsf(">");
      dtadc=ADC4; 
                          
      lcd_gotoxy(1,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              ADC4--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              ADC4++;
         }
  } 
  void set_ADC5()
  {
      lcd_gotoxy(4,1);
      lcd_putsf(">");
      dtadc=ADC5; 
                          
      lcd_gotoxy(5,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              ADC5--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              ADC5++;
         }
  }
  void set_ADC6()
  {
      lcd_gotoxy(8,1);
      lcd_putsf(">");
      dtadc=ADC6; 
                          
      lcd_gotoxy(9,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              ADC6--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              ADC6++;
         }
         
  }
  void set_ADC7()
  {
      lcd_gotoxy(12,1);
      lcd_putsf(">");
      dtadc=ADC7; 
                          
      lcd_gotoxy(13,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              ADC7--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              ADC7++;
         }
         
  }
     
    void set_Kp()
  {
      lcd_gotoxy(0,1);
      lcd_putsf(">");
      dtadc=Kp; 
                          
      lcd_gotoxy(1,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              Kp--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              Kp++;
         }
  }
   void set_Ki()
  {
      lcd_gotoxy(4,1);
      lcd_putsf(">");
      dtadc=Ki; 
                          
      lcd_gotoxy(5,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              Ki--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              Ki++;
         }
  }
  void set_Kd()
  {
      lcd_gotoxy(8,1);
      lcd_putsf(">");
      dtadc=Kd; 
                          
      lcd_gotoxy(9,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              Kd--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              Kd++;
         }
  } 
  
  void aksi(char px)
  { 
   terus=1;
   while(terus==1)
   {
    lcd_gotoxy(5,1);
    lcd_putsf("aksi=");
    lcd_gotoxy(11,1);
       sprintf(buf,"%d  ",arr[px+50]); 
       lcd_puts(buf); 
    if(!sw_ok)
     { 
       delay_ms(150);
       arr[px+50] = 1; 
       lcd_gotoxy(11,1);
       sprintf(buf,"%d  ",1); 
       lcd_puts(buf); 
       terus=0;
     }
     if(!sw_kiri)
     { 
       delay_ms(150);
       arr[px+50] = 2; 
       lcd_gotoxy(11,1);
       sprintf(buf,"%d  ",2); 
       lcd_puts(buf); 
       terus=0;
     }
     if(!sw_start)
     { 
       delay_ms(150);
       arr[px+50] = 3;
       lcd_gotoxy(11,1);
       sprintf(buf,"%d  ",3); 
       lcd_puts(buf); 
       terus=0;
     }
     
     if(!sw_kanan)
     {   
        delay_ms(100);
         terus = 0;     
     }
     
    
   }     
  }
  void set_SKN( char px)
  {  
    terus=1;
    while(terus==1)
    {          
    dtadc=px;
    delay_ms(150);
    lcd_clear();
    lcd_gotoxy(0,0);                
    lcd_putchar(0);        
    lcd_gotoxy(1,0);
    lcd_putsf("SCN ke ");
    lcd_gotoxy(7,0);
    sprintf(buf,"%d  ",px); 
    lcd_puts(buf);  
    lcd_gotoxy(1,1);
       sprintf(buf,"%d  ",arr[px]); 
       lcd_puts(buf);  
     if(!sw_ok)
     { 
       delay_ms(150);
       arr[px] = 1; 
       lcd_gotoxy(1,1);
       sprintf(buf,"%d  ",1); 
       lcd_puts(buf); 
       aksi(px);
       terus=0;
     }
     if(!sw_kiri)
     { 
       delay_ms(150);
       arr[px] = 2; 
       lcd_gotoxy(1,1);
       sprintf(buf,"%d  ",2); 
       lcd_puts(buf); 
       aksi(px);
       terus=0;
     }
     if(!sw_start)
     { 
       delay_ms(150);
       arr[px] = 3;
       lcd_gotoxy(1,1);
       sprintf(buf,"%d  ",3); 
       lcd_puts(buf); 
       aksi(px);
       terus=0;
     }
     if(!sw_kanan)
     { 
       delay_ms(150);
       arr[px] = 4;
       lcd_gotoxy(1,1);
       sprintf(buf,"%d  ",4); 
       lcd_puts(buf); 
       aksi(px);
       terus=0;
     }
     
     if(!sw_ok)
     {   
        delay_ms(100);
         terus = 0;     
     }
     
     }
     
    }
  void Zsken()
  {
    int i;
    delay_ms(100);
    dtadc=z;
    lcd_clear();
    lcd_gotoxy(0,0);                
    lcd_putchar(0);        
    lcd_gotoxy(1,0);
    lcd_putsf("Jumlah = ");
    lcd_gotoxy(1,1);
    sprintf(buf,"%d  ",dtadc); 
    lcd_puts(buf);    
    arr[100] = z;
    arr[99] = 3;   
        if(!sw_kiri)
         {
              delay_ms(deledele);                      
              z--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              z++;
         }
          if(!sw_start)
         {
              delay_ms(150);              
              for(i=0;i<z;i++)
                {   
                    set_SKN(i);
                }                
              lcd_clear();   
              
          }
  
  }
   
   void menu_area()
  {   
      delay_ms(150);
      lcd_clear();
      lcd_gotoxy(2,0);
      lcd_putsf("AREA =");
      lcd_gotoxy(2,1);
      lcd_putsf("1");
      lcd_gotoxy(5,1);
      lcd_putsf("2");
      lcd_gotoxy(8,1);
      lcd_putsf("3");
      lcd_gotoxy(12,1);
      lcd_putsf("3");
       
                          
      lcd_gotoxy(8,0);                
      sprintf(buf,"%d  ",arr[98]);        
      lcd_puts(buf);             
         if(!sw_ok)
         {  
           
         arr[98]=1;
         lcd_putsf("1");
                                    
         }
          if(!sw_kiri)
         {   
              arr[98]=2;
              lcd_putsf("2");;                      
         }
         if(!sw_start)
         {   
              arr[98]=3;
              lcd_putsf("3");;                      
         }
         if(!sw_kanan)
         {   
              arr[98]=4;
              lcd_putsf("4");;                      
         }
        
  }  
   
   void menu_mode()
  {   
      delay_ms(150);
      lcd_clear();
      lcd_gotoxy(2,0);
      lcd_putsf("MODE =");
      lcd_gotoxy(2,1);
      lcd_putsf("A");
      lcd_gotoxy(5,1);
      lcd_putsf("B");
      lcd_gotoxy(8,1);
      lcd_putsf("C");
      lcd_gotoxy(11,1);
      lcd_putsf("D");
       
                          
      lcd_gotoxy(8,0);                
      sprintf(buf,"%d  ",arr[99]);        
      lcd_puts(buf);             
         if(!sw_ok)
         {  
           
         arr[99]=1;
         lcd_putsf("A");
                                    
         }
          if(!sw_kiri)
         {   
              arr[99]=2;
              lcd_putsf("B");;                      
         }
         if(!sw_start)
         {   
              arr[99]=3;
              lcd_putsf("C");;                      
         }
         if(!sw_kanan)
         {   
              arr[99]=4;
              lcd_putsf("D");;                      
         }
        
  }
   void set_MAX()
  {
      lcd_gotoxy(8,1);
      lcd_putsf(">");
      dtadc=MAXPWM; 
                          
      lcd_gotoxy(9,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              MAXPWM--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              MAXPWM++;
         }
  }
   void set_MIN()
  {
      lcd_gotoxy(12,1);
      lcd_putsf(">");
      dtadc=MINPWM; 
                          
      lcd_gotoxy(13,1);                
      sprintf(buf,"%d  ",dtadc);        
      lcd_puts(buf);             
         if(!sw_kiri)
         {
              delay_ms(deledele);                      
              MINPWM--;
         }
         if(!sw_kanan)
         {
              delay_ms(deledele);
              MINPWM++;
         }
  }
  void set_WRN()
  {
    lcd_gotoxy(4,1);
    lcd_putsf(">");
    if (!sw_kiri)
    {
        delay_ms(150);
        if (warna==1) 
        {
            warna = 0;
            lcd_gotoxy(6,1);
            lcd_putsf("H"); 
        }
        else
        {
            warna = 1;
            lcd_gotoxy(6,1);
            lcd_putsf("P"); 
        }
    }
  }
  
  //---------------Sistem Tampilan Menu ADC------------------//
  //-----------------------------------------------------//
  //-----------------------------------------------------//
  
   void disp_set_ADC1()
   {
       if (warna == 1)
       {
            lcd_gotoxy(6,0);
            lcd_putsf("P"); 
       }
       else
       {
            lcd_gotoxy(6,0);
            lcd_putsf("H");
       } 
     lihat_adc();
     lcd_gotoxy(9,0);
     lcd_putsf("Def:");
     
     dtadc=batas_sensor;                    
     lcd_gotoxy(13,0);                
     sprintf(buf,"%d  ",dtadc);        
     lcd_puts(buf);
     
   }
   void disp_set_ADC2()
   {                                       
                  dtadc=read_adc(7);                     
                  lcd_gotoxy(1,0);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);
                                                          
                  dtadc=read_adc(6);                     
                  lcd_gotoxy(5,0);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);        
                                                          
                  dtadc=read_adc(5);                     
                  lcd_gotoxy(9,0);                
                  sprintf(buf,"%d  ",dtadc);    
                  lcd_puts(buf);
                                                          
                  dtadc=read_adc(4);                      
                  lcd_gotoxy(13,0);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);              
                             
                  dtadc=ADC7;                     
                  lcd_gotoxy(1,1);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);
                                                          
                  dtadc=ADC6;                     
                  lcd_gotoxy(5,1);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);        
                                                          
                  dtadc=ADC5;                     
                  lcd_gotoxy(9,1);                
                  sprintf(buf,"%d  ",dtadc);    
                  lcd_puts(buf);
                                                          
                  dtadc=ADC4;                      
                  lcd_gotoxy(13,1);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);
   }
void disp_set_ADC3()
   {                                       
                  dtadc=read_adc(3);                     
                  lcd_gotoxy(1,0);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);
                                                          
                  dtadc=read_adc(2);                     
                  lcd_gotoxy(5,0);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);        
                                                          
                  dtadc=read_adc(1);                     
                  lcd_gotoxy(9,0);                
                  sprintf(buf,"%d  ",dtadc);    
                  lcd_puts(buf);
                                                          
                  dtadc=read_adc(0);                      
                  lcd_gotoxy(13,0);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);              
                             
                  dtadc=ADC3;                     
                  lcd_gotoxy(1,1);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);
                                                          
                  dtadc=ADC2;                     
                  lcd_gotoxy(5,1);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);        
                                                          
                  dtadc=ADC1;                     
                  lcd_gotoxy(9,1);                
                  sprintf(buf,"%d  ",dtadc);    
                  lcd_puts(buf);
                                                          
                  dtadc=ADC0;                      
                  lcd_gotoxy(13,1);                
                  sprintf(buf,"%d  ",dtadc);        
                  lcd_puts(buf);
   }
   
   
   void set_ADC()
   {
     def:
            delay_ms(150);
            disp_set_ADC1();
            lcd_gotoxy(8,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_def();
              } 
              delay_ms(150); 
              ADC0=ADC1=ADC2=ADC3=ADC4=ADC5=ADC6=ADC7=batas_sensor;  
              lcd_clear();  
               
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto ADC0;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto ADC7;            
            }
            if(!sw_cancel) goto exit; else goto def;
     ADC0:
            delay_ms(150);
            disp_set_ADC2();
            lcd_gotoxy(0,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_ADC0();
              } 
              delay_ms(150);   
              lcd_clear();  
               
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto ADC1;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto def;            
            }
            if(!sw_cancel) goto exit; else goto ADC0;
     ADC1:
            delay_ms(150);
            disp_set_ADC2();
            lcd_gotoxy(4,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_ADC1();
              } 
              delay_ms(150);   
              lcd_clear();  
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto ADC2;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto ADC0;            
            }
            if(!sw_cancel) goto exit; else goto ADC1;
     ADC2:
            delay_ms(150);
            disp_set_ADC2();
            lcd_gotoxy(8,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_ADC2();
              } 
              delay_ms(150);   
              lcd_clear();  
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto ADC3;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto ADC1;            
            }
            if(!sw_cancel) goto exit; else goto ADC2;
     ADC3:
            delay_ms(150);
            disp_set_ADC2();
            lcd_gotoxy(12,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_ADC3();
              } 
              delay_ms(150);   
              lcd_clear();  
                
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto ADC4;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto ADC2;            
            }
            if(!sw_cancel) goto exit; else goto ADC3; 
     
     ADC4:
            delay_ms(150);
            disp_set_ADC3();
            lcd_gotoxy(0,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_ADC4();
              } 
              delay_ms(150);   
              lcd_clear();  
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto ADC5;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto ADC3;            
            }
            if(!sw_cancel) goto exit; else goto ADC4;
            
     ADC5:
            delay_ms(150);
            disp_set_ADC3();
            lcd_gotoxy(4,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_ADC5();
              } 
              delay_ms(150);   
              lcd_clear();  
                
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto ADC6;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto ADC4;            
            }
            if(!sw_cancel) goto exit; else goto ADC5;
            
     ADC6:
            delay_ms(150);
            disp_set_ADC3();
            lcd_gotoxy(8,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_ADC6();
              } 
              delay_ms(150);   
              lcd_clear();  
                
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto ADC7;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto ADC5;            
            }
            if(!sw_cancel) goto exit; else goto ADC6;
            
     ADC7:
            delay_ms(150);
            disp_set_ADC3();
            lcd_gotoxy(12,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_ADC7();
              } 
              delay_ms(150);   
              lcd_clear();  
                
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto def;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto ADC6;            
            }
            if(!sw_cancel) goto exit; else goto ADC7; 
       exit:       
   }
   
   void disp_menu_pid()
   {
       lcd_gotoxy(0,0);
       lcd_putsf(" Kp: Ki: Kd:");       
       dtadc=Kp;                       
       lcd_gotoxy(1,1);                
       sprintf(buf,"%d  ",dtadc);        
       lcd_puts(buf);             
       dtadc=Ki;                       
       lcd_gotoxy(5,1);                
       sprintf(buf,"%d  ",dtadc);        
       lcd_puts(buf);                
       dtadc=Kd;                       
       lcd_gotoxy(9,1);                
       sprintf(buf,"%d  ",dtadc);        
       lcd_puts(buf);
   }
   void menu_pid()
   {
           menukp:
            delay_ms(150);
            disp_menu_pid();
            lcd_gotoxy(0,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_Kp();
              } 
              delay_ms(150);   
              lcd_clear();  
                
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menuki;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menukd;            
            }
            if(!sw_cancel) goto exit; else goto menukp;
            
         menuki:
            delay_ms(150);
            disp_menu_pid();
            lcd_gotoxy(4,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_Ki();
              } 
              delay_ms(150);   
              lcd_clear();  
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menukd;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menukp;            
            }
            if(!sw_cancel) goto exit; else goto menuki;
            
         menukd:
            delay_ms(150);
            disp_menu_pid();
            lcd_gotoxy(8,0);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_Kd();
              } 
              delay_ms(150);   
              lcd_clear();  
               
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menukp;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menuki;            
            }
            if(!sw_cancel) goto exit; else goto menukd;      
   exit:
   }
   void disp_menu_debug1()
   {
       lcd_gotoxy(0,0);
       lcd_putsf(" ADC WRN MAX MIN");
       lcd_gotoxy(1,1);
       lcd_putsf("|||");
                     
       dtadc=MAXPWM;                       
       lcd_gotoxy(9,1);                
       sprintf(buf,"%d  ",dtadc);        
       lcd_puts(buf);
                    
       dtadc=MINPWM;                       
       lcd_gotoxy(13,1);                
       sprintf(buf,"%d  ",dtadc);        
       lcd_puts(buf);
       
        if (warna == 1)
       {
            lcd_gotoxy(6,1);
            lcd_putsf("P"); 
       }
       else
       {
            lcd_gotoxy(6,1);
            lcd_putsf("H");
       }  
   
   }
  
   
   void menu_debug()
   {
     menuadc:
            delay_ms(150);
            disp_menu_debug1();
            lcd_gotoxy(0,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              lcd_clear();
              while(sw_cancel)
              {
                set_ADC();
              } 
              delay_ms(150);   
              lcd_clear();  
                 
            }
            if(!sw_kanan)
            {
            lcd_clear();
             goto menuwarna;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menumin;            
            }
            if(!sw_cancel) goto exit; else goto menuadc;
    
       menumax:
            delay_ms(150);
            disp_menu_debug1();
            lcd_gotoxy(8,0);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_MAX();
              } 
              delay_ms(150);   
              lcd_clear();  
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menumin;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menuwarna;            
            } 
            if(!sw_cancel) goto exit; else goto menumax;
       menumin:
            delay_ms(150);
            disp_menu_debug1();
            lcd_gotoxy(12,0);
            lcd_putchar(0);  
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_MIN();
              } 
              delay_ms(150);   
              lcd_clear();  
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menuadc;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menumax;            
            }
            if(!sw_cancel) goto exit; else goto menumin;
      menuwarna:
            delay_ms(150);
            disp_menu_debug1();
            lcd_gotoxy(4,0);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              delay_ms(150);
              
              while(sw_ok)
              {
                set_WRN();
              } 
              delay_ms(150);   
              lcd_clear();  
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menumax;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menuadc;             
            } 
            if(!sw_cancel) goto exit; else goto menuwarna;
    
   exit:                      
   }
   void disp_menu()
   {
     lcd_gotoxy(1,0);
     lcd_putsf("D E M E N T O R");
     lcd_gotoxy(0,1);
     lcd_putsf(" DEB PID X PLAN");
   } 
   
   
      
   void disp_plan()
   {
     lcd_gotoxy(1,0);
     lcd_putsf("D E M E N T O R");
     lcd_gotoxy(0,1);
     lcd_putsf(" Part Mode Area ");
   }
  
void planning()
   {
     part:
            delay_ms(150);
            disp_plan();
            lcd_gotoxy(0,1);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              delay_ms(150);
              
              lcd_clear();
              while(sw_cancel)
              {
                Zsken();
              } 
              lcd_clear();
              delay_ms(150);   
                 
            }
            
            if(!sw_kanan)
            {
             lcd_clear();
             goto menumode;            
            }
            
            
            if(!sw_kiri)
            {
             lcd_clear();
             goto menuarea;            
            }
            if(!sw_cancel) goto exit; else goto part; ;
            
     menumode:
            delay_ms(150);
            disp_plan();
            lcd_gotoxy(5,1);
            lcd_putchar(0); 
            if(!sw_ok)
            if(!sw_ok)
            {
              delay_ms(150);
              
              lcd_clear();
              while(sw_cancel)
              {
                menu_mode();
              } 
              lcd_clear();
              delay_ms(150);   
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menuarea;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto part;            
            }
           
            if(!sw_cancel) goto exit; else goto menumode; 
            
     menuarea:
            delay_ms(150);
            disp_plan();
            lcd_gotoxy(10,1);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              delay_ms(150);
              
              lcd_clear();
              while(sw_cancel)
              {
                menu_area();
              } 
              lcd_clear();
              delay_ms(150);   
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto part;            
            }
            
            if(!sw_kiri)
            {
             lcd_clear();
             goto menumode;            
            }
           
            if(!sw_cancel) goto exit; else goto menuarea; 
     exit:
     }  
   void menu()
   {
     menudeb:
            delay_ms(150);
            disp_menu();
            lcd_gotoxy(0,1);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              delay_ms(150);
              
              lcd_clear();
              while(sw_cancel)
              {
                menu_debug();
              } 
              lcd_clear();
              delay_ms(150);   
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menupid;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menuplan;            
            }
            if(!sw_cancel) goto exit; else goto menudeb; 
            
     menupid:
            delay_ms(150);
            disp_menu();
            lcd_gotoxy(4,1);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              delay_ms(150);
              
              lcd_clear();
              while(sw_cancel)
              {
                menu_pid();
              } 
              lcd_clear();
              delay_ms(150);   
              
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menux;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menudeb;            
            }
            if(!sw_cancel) goto exit; else goto menupid; 
            
     menux:
            delay_ms(150);
            disp_menu();
            lcd_gotoxy(8,1);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              delay_ms(150);
              lcd_clear();
              lcd_gotoxy(0,0);
              lcd_putsf("Status:");
              
              while(sw_ok)
              {
                lihat_adc();
              } 
              lcd_clear();
              delay_ms(150);   

            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menuplan;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menupid;            
            }
            if(!sw_cancel) goto exit; else goto menux;
            
     menuplan:
            delay_ms(200);
            disp_menu();
            lcd_gotoxy(10,1);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              delay_ms(200);
              
              lcd_clear();
              while(sw_cancel)
              {
                planning();
              } 
              lcd_clear();
              delay_ms(200);   
                 
            } 
            if(!sw_kanan)
            {
             lcd_clear();
             goto menudeb;            
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menux;            
            }  
            if(!sw_cancel) goto exit; else goto menuplan;   
      exit:                        
   } 
   void interface()
   {
     lcd_gotoxy(1,0);
     lcd_putsf("-");
     lcd_gotoxy(0,1);
     lcd_putsf(" CONFIG | START");
   }
   void menuinterface()
   {
     menusetting:
            delay_ms(200);
            interface();
            lcd_gotoxy(0,1);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              delay_ms(200);
              
              lcd_clear();
              while(sw_cancel)
              {
                menu();
              } 
              lcd_clear();
              delay_ms(200);   
                 
            }
            if(!sw_kanan)
            {
             lcd_clear();
             goto menustart;            
            }
            if(!sw_cancel) goto exit; else goto menusetting; 
            
     menustart:
            delay_ms(125);
            interface();
            lcd_gotoxy(9,1);
            lcd_putchar(0); 
            if(!sw_ok)
            {
              start=1;
              goto exit;
            }
            if(!sw_kiri)
            {
             lcd_clear();
             goto menusetting;            
            }
           
            if(!sw_cancel) goto exit; else goto menustart; 
     exit:
     }  
 
//---------------------END OF MENU----------------------------------------------------//
//------------------------------------------------------------------------------------//
//-------------------------ambil nilai ADC--------------------------------------------//                

unsigned int lihat_adc()
{
     x=0b00000000;
     lcd_gotoxy(2,1);     
     //if((read_adc(0))>(ADC0)){x+=0b10000000;lcd_putchar('1');} else lcd_putchar('0'); 
     if((read_adc(1))>(ADC1)){x+=0b01000000;lcd_putchar('1');} else lcd_putchar('0');
     if((read_adc(2))>(ADC2)){x+=0b00100000;lcd_putchar('1');} else lcd_putchar('0');
     if((read_adc(3))>(ADC3)){x+=0b00010000;lcd_putchar('1');} else lcd_putchar('0');
     if((read_adc(4))>(ADC4)){x+=0b00001000;lcd_putchar('1');} else lcd_putchar('0');
     if((read_adc(5))>(ADC5)){x+=0b00000100;lcd_putchar('1');} else lcd_putchar('0');
     if((read_adc(6))>(ADC6)){x+=0b00000010;lcd_putchar('1');} else lcd_putchar('0');
    // if((read_adc(7))>(ADC7)){x+=0b00000001;lcd_putchar('1');} else lcd_putchar('0');
    lcd_gotoxy(0,0) ; if ((read_adc(0))>(ADC0)){lcd_putsf("L:1");} else    lcd_putsf("L:0");
    lcd_gotoxy(3,0) ; if ((read_adc(7))>(ADC7)){lcd_putsf("R:1");} else    lcd_putsf("R:0");
     return x;             
}

//------------------------------------------------------------------------------------//

//-------------------------PROTEKSI MENTOK--------------------------------------------//
   

//--------------------------BACA GARIS n PENENTUAN------------------------------------//
int MV, P, I, D, PV, mem, error, last_error, rate;
int var_Kp, var_Ki, var_Kd;
unsigned char SP = 0;

void trace_garis()
{  

    int x=lihat_adc();
    switch(x)
    {
     
     //-----------nilai PV(error)-------//
    
     
     //-------------kanan----------------//

     case 0b01000000 :
                   PV=6;
                   mem=6;
                   //maju();
                   break;
   
     case 0b01100000 :
                   PV=5;
                   mem=5;
                   //maju();
                   break;
 
    
     case 0b00100000 :
                   PV=4;
                   mem=4;
                   //maju();
                   break;
   
     case 0b00110000 :
                   PV=3;
                   mem=3;
                   //maju();
                   break;
    
     //-------------TENGAH-----------------//
     case 0b00010000 :
                   PV= 2;
                   mem=2;
                   //maju();
                   break;
          
     case 0b00011000 :
                   PV= 0;
                   mem= 0;
                  // maju();
                   break;
         
     case 0b00001000 :
                   PV=-2;
                   mem=-2;
                   //maju();
                   break;
       //--------------KIRI------------------//
     case 0b00001100 :
                   PV=-3;
                   mem=-3;
                  // maju();
                   break;
  
     case 0b00000100 :
                   PV=-4;
                   mem=-4;
                   //maju();
                   break;
   
     case 0b00000110 :
                   PV=-5;
                   mem=-5;
                  // maju();
                   break;
    
     case 0b00000010 :
                   PV=-6;
                   mem=-6;
                  // maju();
                   break;
    
   
    
    // UNTUK KANAN
   
    //LOST OUT OF CONTROL 
    
    case 0b00000000 :
                    
                    if(mem == 0)
                    {motor(maju,maju,MAXPWM,MAXPWM);delay_ms(20);
                    break;
                    } 
                    
                    else if(mem > 0)
                    {                          
                    motor(maju,mundur,60,30);delay_ms(20);
                    break;
                                          
                    }
                    else if(mem < 0)
                    {
                    
                    motor(mundur,maju,30,60);delay_ms(20);
                    break;                          
                    }
                    
                     
                                                                 
      
    }
                     
                                                                 
      
    
    
    //---------------------------------------//
    //---------------P+I+D-------------------//
    //---------------------------------------//
 
    error = SP - PV;
    P = (var_Kp* error)/10 ;  
    
    I = I + error;
    I = (I * var_Ki)/10;
    
    rate = error - last_error;
    D    = (rate * var_Kd)/10;
    
    last_error = error; 
    
    MV = P+I+D;
    
    
   if (kondisiZ == 0)
        {DCPWM = 90;}
        else
        {DCPWM = MAXPWM;}
        
    
    if (MV == 0 ) 
    {             
       
         motor(maju,maju,DCPWM,DCPWM);      
           
    } 
     else if (MV > 0) //belok kanan
    { 
          
         VARKIRI= DCPWM + ( ( intervalPWM - 50 ) * MV);
         VARKANAN= DCPWM + ( ( intervalPWM * MV ) - 30 );
         
             
         if (VARKIRI > DCPWM) {PWMKIRI = DCPWM; }
        else if (VARKIRI < MINPWM) {VARKIRI = MINPWM; }
        else if (VARKIRI < DCPWM) {PWMKIRI = VARKIRI;}
        
        
        if (VARKANAN  > DCPWM) {PWMKANAN  = DCPWM; }
         else if (VARKANAN < MINPWM) {PWMKANAN = MINPWM; }
         else if (VARKANAN  < DCPWM) {PWMKANAN  = VARKANAN; }
       
       motor(maju,maju,PWMKANAN,PWMKIRI);
    } 
    
    else if (MV < 0)    //belok kiri
    { 
          
               
         VARKANAN= DCPWM - ((intervalPWM - 50) * MV);
         VARKIRI=(DCPWM - (intervalPWM * MV) - 30) ;
                                          
       
          if (VARKIRI > DCPWM) {PWMKIRI = DCPWM;}
        else if (VARKIRI < MINPWM) {PWMKIRI = MINPWM;}
        else if (VARKIRI < DCPWM ) {PWMKIRI = VARKIRI; }
        
 
        if (VARKANAN  > DCPWM) {PWMKANAN  = DCPWM; }
        else if (VARKANAN < MINPWM) {PWMKANAN = MINPWM; }
        else if (VARKANAN  < DCPWM) {PWMKANAN  = VARKANAN; }
        
        motor(maju,maju,PWMKANAN,PWMKIRI);
         
    } 
 
     
     
  //-----------------------------------------------//   
  //-------STATUS PWM WAKRU RACE-------------------//
  //-----------------------------------------------// 
             
     dtadc=PWMKANAN;                    
     lcd_gotoxy(11,0);                
     sprintf(buf,"L:%d  ",dtadc);        
     lcd_puts(buf);
     
     
     dtadc=PWMKIRI;                    
     lcd_gotoxy(11,1);                
     sprintf(buf,"R:%d  ",dtadc);        
     lcd_puts(buf);
                         
     dtadc=kondisiZ;                    
     lcd_gotoxy(0,1);                
     sprintf(buf,"z%d  ",dtadc);        
     lcd_puts(buf);
     
    
  //------------------------------------------------//
  //------------------------------------------------//
                   
}

//-----------------------------------------------------------------------------------------------------------//

 


void main(void)
{
unsigned char cnt,aksi,kondisi;
PORTD=0x00;
DDRD=0x78;
TCCR1A=0xA1;
TCCR1B=0x0D;
TCNT1H=0x00;
TCNT1L=0x00;
OCR1A=0;
OCR1B=0;
ACSR=0x80;
SFIOR=0x00;

ADMUX=ADC_VREF_TYPE & 0xff;
ADCSRA=0x84;
SFIOR&=0xEF;
SFIOR|=0x10;


// LCD module initialization
lcd_init(16);
/* define user character 0 */
define_char(char0,0);
while (kondisidahdiset !=1   )
 {   
    lcd_gotoxy(1,0);
     lcd_putsf("D E M E N T O R");
     lcd_gotoxy(0,1);
     lcd_putsf(" AWAL AKHIR SKEN");
      if(!sw_kiri)
            { 
              delay_ms(200);
               kondisiZ= 2;
              kondisidahdiset = 1;                             
              delay_ms(200);
              lcd_clear();                  
            }
      if(!sw_start)
            {
              delay_ms(200);
              kondisidahdiset = 1;
              kondisiZ= 2;               
              lcd_clear();
              delay_ms(200);     
            }
      if(!sw_kanan)
            {
              delay_ms(200);
              kondisidahdiset = 1;
              kondisiZ= 2;               
              lcd_clear();
              delay_ms(200);  
                 
            }
            
     
   
     
 }      
    delay_ms(100);

while (start!=1)
 {   
     menuinterface(); 
           
     
 } 
 

var_Kp  = Kp;
var_Ki  = Ki;
var_Kd  = Kd;   
intervalPWM = (MAXPWM - MINPWM) / 8; ; 
batas_sensor=batas_sensor;
PV = 0;
MV= 0;
error = 0;
last_error = 0;
  
lcd_clear();
lcd_gotoxy(5,0);
lcd_putsf("Allahuakbar...");
lcd_gotoxy(4,1);
lcd_putsf("Bismillah");
delay_ms(500);
lcd_clear();
 
  motor(maju,maju,0,0);
 
if (arr[99]==1 && arr[98] == 1) 
 {
//======== TULIS MANUAL=============================
arr[100] = 15;//jumlah skenario               //SKENARIO kanan
        arr[0] = 2; //1
 arr[50]= 3;
        arr[1] = 2;//2
 arr[51]= 2;
        arr[2] = 1;//3
 arr[52]= 1;
       arr[3] = 2; // 4
 arr[53]= 2;
        arr[4] =3;//5
 arr[54]= 2;
       arr[5] = 1;//6 CEK POINT 2 
 arr[55]= 1;
        arr[6] = 3;//7
 arr[56]= 2;
       arr[7] = 3;// 8
 arr[57]= 1;
        arr[8] = 1; //9  //disini belum fiiiix
 arr[58]= 1;
        arr[9] = 4; //10 ck 2
 arr[59]= 3;
        arr[10] = 4; //11 ck 3
 arr[60]= 3;
        arr[11] = 4; //11 ck 3
 arr[61]= 3;
        arr[12] = 4; //11 ck 3
 arr[62]= 3;      
 }
 
    
else if(arr[99]==1 && arr[98]==2)
 {
 arr[100] = 10;//jumlah skenario 
        arr[0] = 1; //6 CEK POINT 2
 arr[50]= 1;
        arr[1] = 3; // 
 arr[51]= 2;
       arr[2] = 3;//8
 arr[52]= 1;
        arr[3] = 1;//9
 arr[53]= 1;
        arr[4] = 4;//9
 arr[54]= 3;
        arr[5] = 4;//9
 arr[55]= 1;
 
  } 

 
 
 
 
 else if(arr[99]==1 && arr[98]==3)
{ 
arr[100] = 10;//jumlah skenario 
        arr[0] = 3; //6 CEK POINT 2
 arr[50]= 3;
        arr[1] = 3; // 
 arr[51]= 3;
       arr[2] = 4;//8
 arr[52]= 2;
        arr[3] = 4;//9
 arr[53]= 2;
  }  
 

 else if(arr[99]==1 && arr[98]==4)
{ 
arr[100] = 10;//jumlah skenario 
        arr[0] = 1; //6 CEK POINT 2
 arr[50]= 1;
        arr[1] = 3; // 
 arr[51]= 1;
       arr[2] = 3;//8
 arr[52]= 1;
        arr[3] = 3;//9
 arr[53]= 1;
        arr[4] = 4;//9
 arr[54]= 3;
        arr[5] = 4;//9
 arr[55]= 1;
  } 
  



else if(arr[99]==2 && arr[98]==1)
 {
 arr[100] = 15;//jumlah skenario               //SKENARIO KIRI
        arr[0] = 2; //1
 arr[50]= 3;
        arr[1] = 1;//2
 arr[51]= 1;
        arr[2] = 2;//3 // CEK POINT 
 arr[52]= 2;
       arr[3] = 1; // 4
 arr[53]= 1;
        arr[4] = 3;//5 
 arr[54]= 1;
       arr[5] = 2;//6 CEK POINT 1
 arr[55]= 2;
        arr[6] = 3;//7
 arr[56]= 1;
       arr[7] = 3;// 8
 arr[57]= 2;
       arr[8] = 2;// 9
arr[58]= 2;
        arr[9] = 4;// 9  //ck 2
arr[58]= 3;
        arr[10] = 4;// 9  //ck 3
arr[58]= 3;
 } 
  
 else if(arr[99]==2 && arr[98]==2)
 {
 arr[100] = 10;//jumlah skenario 
        arr[0] = 2; // 1 CEK POINT 2 dan 3
 arr[50]= 2;
        arr[1] = 3; //2
 arr[51]=1;
       arr[2] = 3; //3
 arr[52]= 2;
        arr[3] = 2;//4
 arr[53]= 2;
        arr[4] = 4;//4
 arr[54]= 3;
        arr[5] = 4;//4
 arr[55]= 3;
        arr[6] = 4;//4
 arr[56]= 3;
       
  } 
  
  else if(arr[99]==2 && arr[98]==3)
 {
 arr[100] = 5;//jumlah skenario 
        arr[0] = 4; // 1
 arr[50]= 3;
        arr[1] = 4; //2
 arr[51]= 3;
       arr[2] = 1; //3
 arr[52]= 1;
        arr[3] = 0;//4
 arr[53]= 1;
       
  } 
 
 else if(arr[99]==2 && arr[98]==4)
  {
 arr[100] = 10;//jumlah skenario 
        arr[0] = 2; // 1 CEK POINT 2 dan 3
 arr[50]= 2;
        arr[1] = 3; //2
 arr[51]= 2;
       arr[2] = 3; //3
 arr[52]= 2;
        arr[3] = 3;//4
 arr[53]= 2;
        arr[4] = 2;//4
 arr[54]= 2;
        arr[5] = 4;//4
 arr[55]= 3;
       
  } 
    
 
     
 
while(kondisiZ < 2)
 { 
         if(countZ >= 4)
        {kondisiZ =1;}     
        if (kondisiZ == 1)
 
 if((read_adc(3))>(ADC3)&& (read_adc(4))>(ADC4) )
      {
       motor(maju,maju,0,0);delay_ms(100);
       kondisiZ ++; 
      }
       trace_garis();
 } 
      
  cnt=0;
 while(cnt<arr[100])
  {   
      kondisi=0;       
       
      
      if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4))
      { 
      kondisi = 3;//01100110   
      }
      else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
      { 
      kondisi = 3;//01100110   
      } 
       else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
      { 
      kondisi = 3;//01100110   
      }  
       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(4)>(ADC4))
      { 
      kondisi = 3;//01100110   
      }  
       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3))
      { 
      kondisi = 3;//01100110   
      }
       else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3))
      { 
      kondisi = 3;//01100110   
      }  
       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5) && read_adc(6)>(ADC6))//kaanan
      { 
      kondisi = 3;//01100110   
      }
       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
      { 
      kondisi = 3;//01100110   
      }  
       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01100110   
      }  
       else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01100110   
      }  
       else if(read_adc(4)>(ADC4) && read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01100110   
      } 
       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01100110   
      } 
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01100110   
      }    
      else if(read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
      { 
      kondisi = 3;//01100110   
      } 
      else if(read_adc(1)>(ADC1)&& read_adc(6)>(ADC6)) //tengah
      { 
      kondisi = 3;//01100110   
      }
       else if(read_adc(2)>(ADC2) && read_adc(4)>(ADC4))
      { 
      kondisi = 3;//01100110   
      } 
        else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5))
      { 
      kondisi = 3;//01100110   
      } 
      else if(read_adc(3)>(ADC3) && read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01100110   
      } 
      else if(read_adc(1)>(ADC1) && read_adc(4)>(ADC4))
      { 
      kondisi =3;//01100110   
      }
      else if(read_adc(2)>(ADC2) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01100110   
      }
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5))
      { 
      kondisi = 3;//01100110   
      }
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01100110   
      }  
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01100110   
      }
    
      
      if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4))
      { 
      kondisi = 2;//01100110   
      }
      else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
      { 
      kondisi = 2;//01100110   
      } 
       else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
      { 
      kondisi = 2;//01100110   
      }  
       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(4)>(ADC4))
      { 
      kondisi = 2;//01100110   
      }  
       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3))
      { 
      kondisi = 2;//01100110   
      }
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5))
      { 
      kondisi = 2;//01100110   
      }
       else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3))
      { 
      kondisi = 2;//01100110   
      }
      else if(read_adc(2)>(ADC2) && read_adc(4)>(ADC4))
      { 
      kondisi = 2;//01100110   
      } 
      else if(read_adc(1)>(ADC1) && read_adc(4)>(ADC4))
      { 
      kondisi = 2;//01100110   
      }
    
    
       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5) && read_adc(6)>(ADC6))//kaanan
      { 
      kondisi = 1;//01100110   
      }          
       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
      { 
      kondisi = 1;//01100110   
      }  
       else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(6)>(ADC6))
      { 
      kondisi = 1;//01100110   
      }  
       else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 1;//01100110   
      } 
      else if(read_adc(2)>(ADC2) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 1;//01100110   
      }  
       else if(read_adc(4)>(ADC4) && read_adc(6)>(ADC6))
      { 
      kondisi = 1;//01100110   
      }
       else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5))
      { 
      kondisi = 1;//01100110   
      }  
      else if(read_adc(3)>(ADC3) && read_adc(6)>(ADC6))
      { 
      kondisi = 1;//01100110   
      }
      
      
      
      
      
      
      
      
      /*
        else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(4)>(ADC4) && read_adc(5)>(ADC5))
      { 
      kondisi = 3;//01100110   
      } 
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01111110   
      }
      else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4) && read_adc(5)>(ADC5))
      { 
      kondisi = 3;//00111100  
      }
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
      { 
      kondisi = 3; //01111100  
      }
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5) && read_adc(6)>(ADC6))
      { 
      kondisi = 3;//01100110   
      }
      else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      {   
        kondisi =3;//00111110
      }
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
      {   
        kondisi =3;//01111100
      }
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      {   
        kondisi =3;//00111110
      }
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      {   
        kondisi =3;//00111110
      }
       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
      {   
        kondisi =3;//01111000
      }
       else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
      {   
        kondisi =3;//01111000
      }       
      else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3))
      { 
      kondisi = 3;//01010000   
      }  
      else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      {   
        kondisi =3;//00110110
      }
       else if(read_adc(4)>(ADC4) && read_adc(6)>(ADC6))
      { 
      kondisi = 3;//00001010   
      } 
      else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;//00011010   
      } 
      else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3; //00011110  
      }
      else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3) && read_adc(4) >(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      {   
        kondisi =3;
      }
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
      {   
        kondisi =3;
      }      
     else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
      { 
      kondisi = 3;   
      }
      else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&&read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;   
      }
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) &&read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)> (ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 3;   
      }
      else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&&read_adc(5)>(ADC5) && read_adc(6)>(ADC6))
      { 
      kondisi = 3;   
      } 
      else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&&read_adc(4)>(ADC4) && read_adc(5)>(ADC5))
      { 
      kondisi = 3;   
      }   
      
      if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      {   
        kondisi =1;//00110110
      }
       else if(read_adc(4)>(ADC4) && read_adc(6)>(ADC6))
      { 
      kondisi = 1;//00001010   
      } 
      else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(6)>(ADC6))
      { 
      kondisi = 1;//00011010   
      } 
      else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
      { 
      kondisi = 1; //00011110  
      }
       else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
      {   
        kondisi =2;//01111000
      }
       else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
      {   
        kondisi =2;//01111000
      }       
      else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3))
      { 
      kondisi = 2;//01010000   
      }
      else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
      { 
      kondisi = 2;//01011000   
      }
     
     */ 
      
      if (kondisi == arr[cnt])
      {         
                
                if (arr[50+cnt] == 1)
                {
                      while((read_adc(7))<(ADC7)) 
                      {motor(maju,maju,20,20);} 
                      while(read_adc(5)<(ADC5)&& read_adc(6)<(ADC6))     
                       {motor(mundur,maju,60,30);}
                       motor(maju,maju,0,0);delay_ms(30);
                } 
                
                else if(arr[50+cnt] == 2)    
                {     while((read_adc(0))<(ADC0)) 
                      {motor(maju,maju,20,20);} 
                      while(read_adc(1)<(ADC1)&&read_adc(2)<(ADC2))     
                      {motor(maju,mundur,60,30);}
                      motor(maju,maju,0,0);delay_ms(30);   
                }
                else if(arr[50+cnt] == 3)    
                {    motor(maju,maju,10,10);delay_ms(100) ;  
                }    
           cnt++;     
                
      }  
      else
      {  
           trace_garis();
      }         
        lcd_gotoxy(9,0);
      sprintf(buf,"%d  ",cnt); 
      lcd_puts(buf);         
                 
      }
       
      
     

}
