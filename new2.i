
#pragma used+
sfrb TWBR=0;
sfrb TWSR=1;
sfrb TWAR=2;
sfrb TWDR=3;
sfrb ADCL=4;
sfrb ADCH=5;
sfrw ADCW=4;      
sfrb ADCSRA=6;
sfrb ADCSR=6;     
sfrb ADMUX=7;
sfrb ACSR=8;
sfrb UBRRL=9;
sfrb UCSRB=0xa;
sfrb UCSRA=0xb;
sfrb UDR=0xc;
sfrb SPCR=0xd;
sfrb SPSR=0xe;
sfrb SPDR=0xf;
sfrb PIND=0x10;
sfrb DDRD=0x11;
sfrb PORTD=0x12;
sfrb PINC=0x13;
sfrb DDRC=0x14;
sfrb PORTC=0x15;
sfrb PINB=0x16;
sfrb DDRB=0x17;
sfrb PORTB=0x18;
sfrb PINA=0x19;
sfrb DDRA=0x1a;
sfrb PORTA=0x1b;
sfrb EECR=0x1c;
sfrb EEDR=0x1d;
sfrb EEARL=0x1e;
sfrb EEARH=0x1f;
sfrw EEAR=0x1e;   
sfrb UBRRH=0x20;
sfrb UCSRC=0X20;
sfrb WDTCR=0x21;
sfrb ASSR=0x22;
sfrb OCR2=0x23;
sfrb TCNT2=0x24;
sfrb TCCR2=0x25;
sfrb ICR1L=0x26;
sfrb ICR1H=0x27;
sfrb OCR1BL=0x28;
sfrb OCR1BH=0x29;
sfrw OCR1B=0x28;  
sfrb OCR1AL=0x2a;
sfrb OCR1AH=0x2b;
sfrw OCR1A=0x2a;  
sfrb TCNT1L=0x2c;
sfrb TCNT1H=0x2d;
sfrw TCNT1=0x2c;  
sfrb TCCR1B=0x2e;
sfrb TCCR1A=0x2f;
sfrb SFIOR=0x30;
sfrb OSCCAL=0x31;
sfrb TCNT0=0x32;
sfrb TCCR0=0x33;
sfrb MCUCSR=0x34;
sfrb MCUCR=0x35;
sfrb TWCR=0x36;
sfrb SPMCR=0x37;
sfrb TIFR=0x38;
sfrb TIMSK=0x39;
sfrb GIFR=0x3a;
sfrb GICR=0x3b;
sfrb OCR0=0X3c;
sfrb SPL=0x3d;
sfrb SPH=0x3e;
sfrb SREG=0x3f;
#pragma used-

#asm
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
#endasm

typedef char *va_list;

#pragma used+

char getchar(void);
void putchar(char c);
void puts(char *str);
void putsf(char flash *str);
int printf(char flash *fmtstr,...);
int sprintf(char *str, char flash *fmtstr,...);
int vprintf(char flash * fmtstr, va_list argptr);
int vsprintf(char *str, char flash * fmtstr, va_list argptr);

char *gets(char *str,unsigned int len);
int snprintf(char *str, unsigned int size, char flash *fmtstr,...);
int vsnprintf(char *str, unsigned int size, char flash * fmtstr, va_list argptr);

int scanf(char flash *fmtstr,...);
int sscanf(char *str, char flash *fmtstr,...);

#pragma used-

#pragma library stdio.lib

#pragma used+

unsigned char cabs(signed char x);
unsigned int abs(int x);
unsigned long labs(long x);
float fabs(float x);
int atoi(char *str);
long int atol(char *str);
float atof(char *str);
void itoa(int n,char *str);
void ltoa(long int n,char *str);
void ftoa(float n,unsigned char decimals,char *str);
void ftoe(float n,unsigned char decimals,char *str);
void srand(int seed);
int rand(void);
void *malloc(unsigned int size);
void *calloc(unsigned int num, unsigned int size);
void *realloc(void *ptr, unsigned int size); 
void free(void *ptr);

#pragma used-
#pragma library stdlib.lib

#pragma used+

void delay_us(unsigned int n);
void delay_ms(unsigned int n);

#pragma used-

#asm
   .equ __lcd_port=0x15 ;PORTC
#endasm

#pragma used+

void _lcd_ready(void);
void _lcd_write_data(unsigned char data);

void lcd_write_byte(unsigned char addr, unsigned char data);

unsigned char lcd_read_byte(unsigned char addr);

void lcd_gotoxy(unsigned char x, unsigned char y);

void lcd_clear(void);
void lcd_putchar(char c);

void lcd_puts(char *str);

void lcd_putsf(char flash *str);

unsigned char lcd_init(unsigned char lcd_columns);

void lcd_control (unsigned char control);

#pragma used-
#pragma library lcd.lib

typedef unsigned char byte;
flash byte char0[8]={
0b1100000,
0b0011000,
0b0000110,
0b1111111,
0b1111111,
0b0000110,
0b0011000,
0b1100000};

void define_char(byte flash *pc,byte char_code)
{
byte i,a;
a=(char_code<<3) | 0x40;
for (i=0; i<8; i++) lcd_write_byte(a++,*pc++);
}

void menu();
void trace_garis();  
unsigned int lihat_adc();

eeprom unsigned char Kp = 10;
eeprom unsigned char z = 9; 
eeprom unsigned char Ki = 0;
eeprom unsigned char Kd = 30; 
eeprom unsigned int  batas_sensor=120;

eeprom unsigned int  ADC0=120;
eeprom unsigned int  ADC1=120;
eeprom unsigned int  ADC2=120;
eeprom unsigned int  ADC3=120;
eeprom unsigned int  ADC4=120;
eeprom unsigned int  ADC5=120;
eeprom unsigned int  ADC6=120;
eeprom unsigned int  ADC7=120;
eeprom int MAXPWM  = 230;
eeprom int MINPWM = 0;

eeprom int warna=0; 
eeprom unsigned char arr[150];

signed int VARKANAN;
signed int VARKIRI;
int x=0b00000000;
int counter=0,terus; 
int intervalPWM;
int deledele=100; 
int start=0;
unsigned char buf[33];
unsigned char dtadc;
int kondisidahdiset=0;
char countZ;
unsigned char kondisiZ = 0;
byte PWMZ = 40;
int DCPWM;

unsigned char read_adc(unsigned char adc_input)
{
ADMUX=adc_input | (0x60 & 0xff);

delay_us(10);

ADCSRA|=0x40;

while ((ADCSRA & 0x10)==0);
ADCSRA|=0x10;
return ADCH;
}

void motor(char xka,char xki,unsigned char Vkanan,unsigned char vkiri)
{
PORTD.6=xka;
PORTD.3=xki;
OCR1B  =Vkanan;
OCR1A=vkiri;

} 

void set_def()
{
lcd_gotoxy(8,0);
lcd_putsf(">");
dtadc=batas_sensor; 

lcd_gotoxy(13,0);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
batas_sensor--;
}
if(!PINB.3  )
{
delay_ms(deledele);
batas_sensor++;
}
}
void set_ADC0()
{
lcd_gotoxy(0,1);
lcd_putsf(">");
dtadc=ADC0; 

lcd_gotoxy(1,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
ADC0--;
}
if(!PINB.3  )
{
delay_ms(deledele);
ADC0++;
}
}
void set_ADC1()
{
lcd_gotoxy(4,1);
lcd_putsf(">");
dtadc=ADC1; 

lcd_gotoxy(5,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
ADC1--;
}
if(!PINB.3  )
{
delay_ms(deledele);
ADC1++;
}
}
void set_ADC2()
{
lcd_gotoxy(8,1);
lcd_putsf(">");
dtadc=ADC2; 

lcd_gotoxy(9,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
ADC2--;
}
if(!PINB.3  )
{
delay_ms(deledele);
ADC2++;
}
}
void set_ADC3()
{
lcd_gotoxy(12,1);
lcd_putsf(">");
dtadc=ADC3; 

lcd_gotoxy(13,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
ADC3--;
}
if(!PINB.3  )
{
delay_ms(deledele);
ADC3++;
}
}
void set_ADC4()
{
lcd_gotoxy(0,1);
lcd_putsf(">");
dtadc=ADC4; 

lcd_gotoxy(1,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
ADC4--;
}
if(!PINB.3  )
{
delay_ms(deledele);
ADC4++;
}
} 
void set_ADC5()
{
lcd_gotoxy(4,1);
lcd_putsf(">");
dtadc=ADC5; 

lcd_gotoxy(5,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
ADC5--;
}
if(!PINB.3  )
{
delay_ms(deledele);
ADC5++;
}
}
void set_ADC6()
{
lcd_gotoxy(8,1);
lcd_putsf(">");
dtadc=ADC6; 

lcd_gotoxy(9,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
ADC6--;
}
if(!PINB.3  )
{
delay_ms(deledele);
ADC6++;
}

}
void set_ADC7()
{
lcd_gotoxy(12,1);
lcd_putsf(">");
dtadc=ADC7; 

lcd_gotoxy(13,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
ADC7--;
}
if(!PINB.3  )
{
delay_ms(deledele);
ADC7++;
}

}

void set_Kp()
{
lcd_gotoxy(0,1);
lcd_putsf(">");
dtadc=Kp; 

lcd_gotoxy(1,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
Kp--;
}
if(!PINB.3  )
{
delay_ms(deledele);
Kp++;
}
}
void set_Ki()
{
lcd_gotoxy(4,1);
lcd_putsf(">");
dtadc=Ki; 

lcd_gotoxy(5,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
Ki--;
}
if(!PINB.3  )
{
delay_ms(deledele);
Ki++;
}
}
void set_Kd()
{
lcd_gotoxy(8,1);
lcd_putsf(">");
dtadc=Kd; 

lcd_gotoxy(9,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
Kd--;
}
if(!PINB.3  )
{
delay_ms(deledele);
Kd++;
}
} 

void aksi(char px)
{ 
terus=1;
while(terus==1)
{
lcd_gotoxy(5,1);
lcd_putsf("aksi=");
lcd_gotoxy(11,1);
sprintf(buf,"%d  ",arr[px+50]); 
lcd_puts(buf); 
if(!PINB.0)
{ 
delay_ms(150);
arr[px+50] = 1; 
lcd_gotoxy(11,1);
sprintf(buf,"%d  ",1); 
lcd_puts(buf); 
terus=0;
}
if(!PINB.1)
{ 
delay_ms(150);
arr[px+50] = 2; 
lcd_gotoxy(11,1);
sprintf(buf,"%d  ",2); 
lcd_puts(buf); 
terus=0;
}
if(!PINB.2 )
{ 
delay_ms(150);
arr[px+50] = 3;
lcd_gotoxy(11,1);
sprintf(buf,"%d  ",3); 
lcd_puts(buf); 
terus=0;
}

if(!PINB.3  )
{   
delay_ms(100);
terus = 0;     
}

}     
}
void set_SKN( char px)
{  
terus=1;
while(terus==1)
{          
dtadc=px;
delay_ms(150);
lcd_clear();
lcd_gotoxy(0,0);                
lcd_putchar(0);        
lcd_gotoxy(1,0);
lcd_putsf("SCN ke ");
lcd_gotoxy(7,0);
sprintf(buf,"%d  ",px); 
lcd_puts(buf);  
lcd_gotoxy(1,1);
sprintf(buf,"%d  ",arr[px]); 
lcd_puts(buf);  
if(!PINB.0)
{ 
delay_ms(150);
arr[px] = 1; 
lcd_gotoxy(1,1);
sprintf(buf,"%d  ",1); 
lcd_puts(buf); 
aksi(px);
terus=0;
}
if(!PINB.1)
{ 
delay_ms(150);
arr[px] = 2; 
lcd_gotoxy(1,1);
sprintf(buf,"%d  ",2); 
lcd_puts(buf); 
aksi(px);
terus=0;
}
if(!PINB.2 )
{ 
delay_ms(150);
arr[px] = 3;
lcd_gotoxy(1,1);
sprintf(buf,"%d  ",3); 
lcd_puts(buf); 
aksi(px);
terus=0;
}
if(!PINB.3  )
{ 
delay_ms(150);
arr[px] = 4;
lcd_gotoxy(1,1);
sprintf(buf,"%d  ",4); 
lcd_puts(buf); 
aksi(px);
terus=0;
}

if(!PINB.0)
{   
delay_ms(100);
terus = 0;     
}

}

}
void Zsken()
{
int i;
delay_ms(100);
dtadc=z;
lcd_clear();
lcd_gotoxy(0,0);                
lcd_putchar(0);        
lcd_gotoxy(1,0);
lcd_putsf("Jumlah = ");
lcd_gotoxy(1,1);
sprintf(buf,"%d  ",dtadc); 
lcd_puts(buf);    
arr[100] = z;
arr[99] = 3;   
if(!PINB.1)
{
delay_ms(deledele);                      
z--;
}
if(!PINB.3  )
{
delay_ms(deledele);
z++;
}
if(!PINB.2 )
{
delay_ms(150);              
for(i=0;i<z;i++)
{   
set_SKN(i);
}                
lcd_clear();   

}

}

void menu_area()
{   
delay_ms(150);
lcd_clear();
lcd_gotoxy(2,0);
lcd_putsf("AREA =");
lcd_gotoxy(2,1);
lcd_putsf("1");
lcd_gotoxy(5,1);
lcd_putsf("2");
lcd_gotoxy(8,1);
lcd_putsf("3");
lcd_gotoxy(12,1);
lcd_putsf("3");

lcd_gotoxy(8,0);                
sprintf(buf,"%d  ",arr[98]);        
lcd_puts(buf);             
if(!PINB.0)
{  

arr[98]=1;
lcd_putsf("1");

}
if(!PINB.1)
{   
arr[98]=2;
lcd_putsf("2");;                      
}
if(!PINB.2 )
{   
arr[98]=3;
lcd_putsf("3");;                      
}
if(!PINB.3  )
{   
arr[98]=4;
lcd_putsf("4");;                      
}

}  

void menu_mode()
{   
delay_ms(150);
lcd_clear();
lcd_gotoxy(2,0);
lcd_putsf("MODE =");
lcd_gotoxy(2,1);
lcd_putsf("A");
lcd_gotoxy(5,1);
lcd_putsf("B");
lcd_gotoxy(8,1);
lcd_putsf("C");
lcd_gotoxy(11,1);
lcd_putsf("D");

lcd_gotoxy(8,0);                
sprintf(buf,"%d  ",arr[99]);        
lcd_puts(buf);             
if(!PINB.0)
{  

arr[99]=1;
lcd_putsf("A");

}
if(!PINB.1)
{   
arr[99]=2;
lcd_putsf("B");;                      
}
if(!PINB.2 )
{   
arr[99]=3;
lcd_putsf("C");;                      
}
if(!PINB.3  )
{   
arr[99]=4;
lcd_putsf("D");;                      
}

}
void set_MAX()
{
lcd_gotoxy(8,1);
lcd_putsf(">");
dtadc=MAXPWM; 

lcd_gotoxy(9,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
MAXPWM--;
}
if(!PINB.3  )
{
delay_ms(deledele);
MAXPWM++;
}
}
void set_MIN()
{
lcd_gotoxy(12,1);
lcd_putsf(">");
dtadc=MINPWM; 

lcd_gotoxy(13,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
if(!PINB.1)
{
delay_ms(deledele);                      
MINPWM--;
}
if(!PINB.3  )
{
delay_ms(deledele);
MINPWM++;
}
}
void set_WRN()
{
lcd_gotoxy(4,1);
lcd_putsf(">");
if (!PINB.1)
{
delay_ms(150);
if (warna==1) 
{
warna = 0;
lcd_gotoxy(6,1);
lcd_putsf("H"); 
}
else
{
warna = 1;
lcd_gotoxy(6,1);
lcd_putsf("P"); 
}
}
}

void disp_set_ADC1()
{
if (warna == 1)
{
lcd_gotoxy(6,0);
lcd_putsf("P"); 
}
else
{
lcd_gotoxy(6,0);
lcd_putsf("H");
} 
lihat_adc();
lcd_gotoxy(9,0);
lcd_putsf("Def:");

dtadc=batas_sensor;                    
lcd_gotoxy(13,0);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);

}
void disp_set_ADC2()
{                                       
dtadc=read_adc(7);                     
lcd_gotoxy(1,0);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);

dtadc=read_adc(6);                     
lcd_gotoxy(5,0);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);        

dtadc=read_adc(5);                     
lcd_gotoxy(9,0);                
sprintf(buf,"%d  ",dtadc);    
lcd_puts(buf);

dtadc=read_adc(4);                      
lcd_gotoxy(13,0);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);              

dtadc=ADC7;                     
lcd_gotoxy(1,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);

dtadc=ADC6;                     
lcd_gotoxy(5,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);        

dtadc=ADC5;                     
lcd_gotoxy(9,1);                
sprintf(buf,"%d  ",dtadc);    
lcd_puts(buf);

dtadc=ADC4;                      
lcd_gotoxy(13,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);
}
void disp_set_ADC3()
{                                       
dtadc=read_adc(3);                     
lcd_gotoxy(1,0);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);

dtadc=read_adc(2);                     
lcd_gotoxy(5,0);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);        

dtadc=read_adc(1);                     
lcd_gotoxy(9,0);                
sprintf(buf,"%d  ",dtadc);    
lcd_puts(buf);

dtadc=read_adc(0);                      
lcd_gotoxy(13,0);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);              

dtadc=ADC3;                     
lcd_gotoxy(1,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);

dtadc=ADC2;                     
lcd_gotoxy(5,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);        

dtadc=ADC1;                     
lcd_gotoxy(9,1);                
sprintf(buf,"%d  ",dtadc);    
lcd_puts(buf);

dtadc=ADC0;                      
lcd_gotoxy(13,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);
}

void set_ADC()
{
def:
delay_ms(150);
disp_set_ADC1();
lcd_gotoxy(8,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_def();
} 
delay_ms(150); 
ADC0=ADC1=ADC2=ADC3=ADC4=ADC5=ADC6=ADC7=batas_sensor;  
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto ADC0;            
}
if(!PINB.1)
{
lcd_clear();
goto ADC7;            
}
if(!PINB.4) goto exit; else goto def;
ADC0:
delay_ms(150);
disp_set_ADC2();
lcd_gotoxy(0,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_ADC0();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto ADC1;            
}
if(!PINB.1)
{
lcd_clear();
goto def;            
}
if(!PINB.4) goto exit; else goto ADC0;
ADC1:
delay_ms(150);
disp_set_ADC2();
lcd_gotoxy(4,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_ADC1();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto ADC2;            
}
if(!PINB.1)
{
lcd_clear();
goto ADC0;            
}
if(!PINB.4) goto exit; else goto ADC1;
ADC2:
delay_ms(150);
disp_set_ADC2();
lcd_gotoxy(8,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_ADC2();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto ADC3;            
}
if(!PINB.1)
{
lcd_clear();
goto ADC1;            
}
if(!PINB.4) goto exit; else goto ADC2;
ADC3:
delay_ms(150);
disp_set_ADC2();
lcd_gotoxy(12,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_ADC3();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto ADC4;            
}
if(!PINB.1)
{
lcd_clear();
goto ADC2;            
}
if(!PINB.4) goto exit; else goto ADC3; 

ADC4:
delay_ms(150);
disp_set_ADC3();
lcd_gotoxy(0,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_ADC4();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto ADC5;            
}
if(!PINB.1)
{
lcd_clear();
goto ADC3;            
}
if(!PINB.4) goto exit; else goto ADC4;

ADC5:
delay_ms(150);
disp_set_ADC3();
lcd_gotoxy(4,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_ADC5();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto ADC6;            
}
if(!PINB.1)
{
lcd_clear();
goto ADC4;            
}
if(!PINB.4) goto exit; else goto ADC5;

ADC6:
delay_ms(150);
disp_set_ADC3();
lcd_gotoxy(8,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_ADC6();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto ADC7;            
}
if(!PINB.1)
{
lcd_clear();
goto ADC5;            
}
if(!PINB.4) goto exit; else goto ADC6;

ADC7:
delay_ms(150);
disp_set_ADC3();
lcd_gotoxy(12,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_ADC7();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto def;            
}
if(!PINB.1)
{
lcd_clear();
goto ADC6;            
}
if(!PINB.4) goto exit; else goto ADC7; 
exit:       
}

void disp_menu_pid()
{
lcd_gotoxy(0,0);
lcd_putsf(" Kp: Ki: Kd:");       
dtadc=Kp;                       
lcd_gotoxy(1,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);             
dtadc=Ki;                       
lcd_gotoxy(5,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);                
dtadc=Kd;                       
lcd_gotoxy(9,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);
}
void menu_pid()
{
menukp:
delay_ms(150);
disp_menu_pid();
lcd_gotoxy(0,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_Kp();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto menuki;            
}
if(!PINB.1)
{
lcd_clear();
goto menukd;            
}
if(!PINB.4) goto exit; else goto menukp;

menuki:
delay_ms(150);
disp_menu_pid();
lcd_gotoxy(4,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_Ki();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto menukd;            
}
if(!PINB.1)
{
lcd_clear();
goto menukp;            
}
if(!PINB.4) goto exit; else goto menuki;

menukd:
delay_ms(150);
disp_menu_pid();
lcd_gotoxy(8,0);
lcd_putchar(0); 
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_Kd();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto menukp;            
}
if(!PINB.1)
{
lcd_clear();
goto menuki;            
}
if(!PINB.4) goto exit; else goto menukd;      
exit:
}
void disp_menu_debug1()
{
lcd_gotoxy(0,0);
lcd_putsf(" ADC WRN MAX MIN");
lcd_gotoxy(1,1);
lcd_putsf("|||");

dtadc=MAXPWM;                       
lcd_gotoxy(9,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);

dtadc=MINPWM;                       
lcd_gotoxy(13,1);                
sprintf(buf,"%d  ",dtadc);        
lcd_puts(buf);

if (warna == 1)
{
lcd_gotoxy(6,1);
lcd_putsf("P"); 
}
else
{
lcd_gotoxy(6,1);
lcd_putsf("H");
}  

}

void menu_debug()
{
menuadc:
delay_ms(150);
disp_menu_debug1();
lcd_gotoxy(0,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

lcd_clear();
while(PINB.4)
{
set_ADC();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto menuwarna;            
}
if(!PINB.1)
{
lcd_clear();
goto menumin;            
}
if(!PINB.4) goto exit; else goto menuadc;

menumax:
delay_ms(150);
disp_menu_debug1();
lcd_gotoxy(8,0);
lcd_putchar(0); 
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_MAX();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto menumin;            
}
if(!PINB.1)
{
lcd_clear();
goto menuwarna;            
} 
if(!PINB.4) goto exit; else goto menumax;
menumin:
delay_ms(150);
disp_menu_debug1();
lcd_gotoxy(12,0);
lcd_putchar(0);  
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_MIN();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto menuadc;            
}
if(!PINB.1)
{
lcd_clear();
goto menumax;            
}
if(!PINB.4) goto exit; else goto menumin;
menuwarna:
delay_ms(150);
disp_menu_debug1();
lcd_gotoxy(4,0);
lcd_putchar(0); 
if(!PINB.0)
{
delay_ms(150);

while(PINB.0)
{
set_WRN();
} 
delay_ms(150);   
lcd_clear();  

}
if(!PINB.3  )
{
lcd_clear();
goto menumax;            
}
if(!PINB.1)
{
lcd_clear();
goto menuadc;             
} 
if(!PINB.4) goto exit; else goto menuwarna;

exit:                      
}
void disp_menu()
{
lcd_gotoxy(1,0);
lcd_putsf("D E M E N T O R");
lcd_gotoxy(0,1);
lcd_putsf(" DEB PID X PLAN");
} 

void disp_plan()
{
lcd_gotoxy(1,0);
lcd_putsf("D E M E N T O R");
lcd_gotoxy(0,1);
lcd_putsf(" Part Mode Area ");
}

void planning()
{
part:
delay_ms(150);
disp_plan();
lcd_gotoxy(0,1);
lcd_putchar(0); 
if(!PINB.0)
{
delay_ms(150);

lcd_clear();
while(PINB.4)
{
Zsken();
} 
lcd_clear();
delay_ms(150);   

}

if(!PINB.3  )
{
lcd_clear();
goto menumode;            
}

if(!PINB.1)
{
lcd_clear();
goto menuarea;            
}
if(!PINB.4) goto exit; else goto part; ;

menumode:
delay_ms(150);
disp_plan();
lcd_gotoxy(5,1);
lcd_putchar(0); 
if(!PINB.0)
if(!PINB.0)
{
delay_ms(150);

lcd_clear();
while(PINB.4)
{
menu_mode();
} 
lcd_clear();
delay_ms(150);   

}
if(!PINB.3  )
{
lcd_clear();
goto menuarea;            
}
if(!PINB.1)
{
lcd_clear();
goto part;            
}

if(!PINB.4) goto exit; else goto menumode; 

menuarea:
delay_ms(150);
disp_plan();
lcd_gotoxy(10,1);
lcd_putchar(0); 
if(!PINB.0)
{
delay_ms(150);

lcd_clear();
while(PINB.4)
{
menu_area();
} 
lcd_clear();
delay_ms(150);   

}
if(!PINB.3  )
{
lcd_clear();
goto part;            
}

if(!PINB.1)
{
lcd_clear();
goto menumode;            
}

if(!PINB.4) goto exit; else goto menuarea; 
exit:
}  
void menu()
{
menudeb:
delay_ms(150);
disp_menu();
lcd_gotoxy(0,1);
lcd_putchar(0); 
if(!PINB.0)
{
delay_ms(150);

lcd_clear();
while(PINB.4)
{
menu_debug();
} 
lcd_clear();
delay_ms(150);   

}
if(!PINB.3  )
{
lcd_clear();
goto menupid;            
}
if(!PINB.1)
{
lcd_clear();
goto menuplan;            
}
if(!PINB.4) goto exit; else goto menudeb; 

menupid:
delay_ms(150);
disp_menu();
lcd_gotoxy(4,1);
lcd_putchar(0); 
if(!PINB.0)
{
delay_ms(150);

lcd_clear();
while(PINB.4)
{
menu_pid();
} 
lcd_clear();
delay_ms(150);   

}
if(!PINB.3  )
{
lcd_clear();
goto menux;            
}
if(!PINB.1)
{
lcd_clear();
goto menudeb;            
}
if(!PINB.4) goto exit; else goto menupid; 

menux:
delay_ms(150);
disp_menu();
lcd_gotoxy(8,1);
lcd_putchar(0); 
if(!PINB.0)
{
delay_ms(150);
lcd_clear();
lcd_gotoxy(0,0);
lcd_putsf("Status:");

while(PINB.0)
{
lihat_adc();
} 
lcd_clear();
delay_ms(150);   

}
if(!PINB.3  )
{
lcd_clear();
goto menuplan;            
}
if(!PINB.1)
{
lcd_clear();
goto menupid;            
}
if(!PINB.4) goto exit; else goto menux;

menuplan:
delay_ms(200);
disp_menu();
lcd_gotoxy(10,1);
lcd_putchar(0); 
if(!PINB.0)
{
delay_ms(200);

lcd_clear();
while(PINB.4)
{
planning();
} 
lcd_clear();
delay_ms(200);   

} 
if(!PINB.3  )
{
lcd_clear();
goto menudeb;            
}
if(!PINB.1)
{
lcd_clear();
goto menux;            
}  
if(!PINB.4) goto exit; else goto menuplan;   
exit:                        
} 
void interface()
{
lcd_gotoxy(1,0);
lcd_putsf("-");
lcd_gotoxy(0,1);
lcd_putsf(" CONFIG | START");
}
void menuinterface()
{
menusetting:
delay_ms(200);
interface();
lcd_gotoxy(0,1);
lcd_putchar(0); 
if(!PINB.0)
{
delay_ms(200);

lcd_clear();
while(PINB.4)
{
menu();
} 
lcd_clear();
delay_ms(200);   

}
if(!PINB.3  )
{
lcd_clear();
goto menustart;            
}
if(!PINB.4) goto exit; else goto menusetting; 

menustart:
delay_ms(125);
interface();
lcd_gotoxy(9,1);
lcd_putchar(0); 
if(!PINB.0)
{
start=1;
goto exit;
}
if(!PINB.1)
{
lcd_clear();
goto menusetting;            
}

if(!PINB.4) goto exit; else goto menustart; 
exit:
}  

unsigned int lihat_adc()
{
x=0b00000000;
lcd_gotoxy(2,1);     

if((read_adc(1))>(ADC1)){x+=0b01000000;lcd_putchar('1');} else lcd_putchar('0');
if((read_adc(2))>(ADC2)){x+=0b00100000;lcd_putchar('1');} else lcd_putchar('0');
if((read_adc(3))>(ADC3)){x+=0b00010000;lcd_putchar('1');} else lcd_putchar('0');
if((read_adc(4))>(ADC4)){x+=0b00001000;lcd_putchar('1');} else lcd_putchar('0');
if((read_adc(5))>(ADC5)){x+=0b00000100;lcd_putchar('1');} else lcd_putchar('0');
if((read_adc(6))>(ADC6)){x+=0b00000010;lcd_putchar('1');} else lcd_putchar('0');

lcd_gotoxy(0,0) ; if ((read_adc(0))>(ADC0)){lcd_putsf("L:1");} else    lcd_putsf("L:0");
lcd_gotoxy(3,0) ; if ((read_adc(7))>(ADC7)){lcd_putsf("R:1");} else    lcd_putsf("R:0");
return x;             
}

int MV, P, I, D, PV, mem, error, last_error, rate;
int var_Kp, var_Ki, var_Kd;
unsigned char SP = 0;

void trace_garis()
{  

int x=lihat_adc();
switch(x)
{

case 0b01000000 :
PV=6;
mem=6;

break;

case 0b01100000 :
PV=5;
mem=5;

break;

case 0b00100000 :
PV=4;
mem=4;

break;

case 0b00110000 :
PV=3;
mem=3;

break;

case 0b00010000 :
PV= 2;
mem=2;

break;

case 0b00011000 :
PV= 0;
mem= 0;

break;

case 0b00001000 :
PV=-2;
mem=-2;

break;

case 0b00001100 :
PV=-3;
mem=-3;

break;

case 0b00000100 :
PV=-4;
mem=-4;

break;

case 0b00000110 :
PV=-5;
mem=-5;

break;

case 0b00000010 :
PV=-6;
mem=-6;

break;

case 0b00000000 :

if(mem == 0)
{motor(0,0,MAXPWM,MAXPWM);delay_ms(20);
break;
} 

else if(mem > 0)
{                          
motor(0,1,60,30);delay_ms(20);
break;

}
else if(mem < 0)
{

motor(1,0,30,60);delay_ms(20);
break;                          
}

}

error = SP - PV;
P = (var_Kp* error)/10 ;  

I = I + error;
I = (I * var_Ki)/10;

rate = error - last_error;
D    = (rate * var_Kd)/10;

last_error = error; 

MV = P+I+D;

if (kondisiZ == 0)
{DCPWM = 90;}
else
{DCPWM = MAXPWM;}

if (MV == 0 ) 
{             

motor(0,0,DCPWM,DCPWM);      

} 
else if (MV > 0) 
{ 

VARKIRI= DCPWM + ( ( intervalPWM - 50 ) * MV);
VARKANAN= DCPWM + ( ( intervalPWM * MV ) - 30 );

if (VARKIRI > DCPWM) {OCR1A = DCPWM; }
else if (VARKIRI < MINPWM) {VARKIRI = MINPWM; }
else if (VARKIRI < DCPWM) {OCR1A = VARKIRI;}

if (VARKANAN  > DCPWM) {OCR1B    = DCPWM; }
else if (VARKANAN < MINPWM) {OCR1B   = MINPWM; }
else if (VARKANAN  < DCPWM) {OCR1B    = VARKANAN; }

motor(0,0,OCR1B  ,OCR1A);
} 

else if (MV < 0)    
{ 

VARKANAN= DCPWM - ((intervalPWM - 50) * MV);
VARKIRI=(DCPWM - (intervalPWM * MV) - 30) ;

if (VARKIRI > DCPWM) {OCR1A = DCPWM;}
else if (VARKIRI < MINPWM) {OCR1A = MINPWM;}
else if (VARKIRI < DCPWM ) {OCR1A = VARKIRI; }

if (VARKANAN  > DCPWM) {OCR1B    = DCPWM; }
else if (VARKANAN < MINPWM) {OCR1B   = MINPWM; }
else if (VARKANAN  < DCPWM) {OCR1B    = VARKANAN; }

motor(0,0,OCR1B  ,OCR1A);

} 

dtadc=OCR1B  ;                    
lcd_gotoxy(11,0);                
sprintf(buf,"L:%d  ",dtadc);        
lcd_puts(buf);

dtadc=OCR1A;                    
lcd_gotoxy(11,1);                
sprintf(buf,"R:%d  ",dtadc);        
lcd_puts(buf);

dtadc=kondisiZ;                    
lcd_gotoxy(0,1);                
sprintf(buf,"z%d  ",dtadc);        
lcd_puts(buf);

}

void main(void)
{
unsigned char cnt,aksi,kondisi;
PORTD=0x00;
DDRD=0x78;
TCCR1A=0xA1;
TCCR1B=0x0D;
TCNT1H=0x00;
TCNT1L=0x00;
OCR1A=0;
OCR1B=0;
ACSR=0x80;
SFIOR=0x00;

ADMUX=0x60 & 0xff;
ADCSRA=0x84;
SFIOR&=0xEF;
SFIOR|=0x10;

lcd_init(16);

define_char(char0,0);
while (kondisidahdiset !=1   )
{   
lcd_gotoxy(1,0);
lcd_putsf("D E M E N T O R");
lcd_gotoxy(0,1);
lcd_putsf(" AWAL AKHIR SKEN");
if(!PINB.1)
{ 
delay_ms(200);
kondisiZ= 2;
kondisidahdiset = 1;                             
delay_ms(200);
lcd_clear();                  
}
if(!PINB.2 )
{
delay_ms(200);
kondisidahdiset = 1;
kondisiZ= 2;               
lcd_clear();
delay_ms(200);     
}
if(!PINB.3  )
{
delay_ms(200);
kondisidahdiset = 1;
kondisiZ= 2;               
lcd_clear();
delay_ms(200);  

}

}      
delay_ms(100);

while (start!=1)
{   
menuinterface(); 

} 

var_Kp  = Kp;
var_Ki  = Ki;
var_Kd  = Kd;   
intervalPWM = (MAXPWM - MINPWM) / 8; ; 
batas_sensor=batas_sensor;
PV = 0;
MV= 0;
error = 0;
last_error = 0;

lcd_clear();
lcd_gotoxy(5,0);
lcd_putsf("Allahuakbar...");
lcd_gotoxy(4,1);
lcd_putsf("Bismillah");
delay_ms(500);
lcd_clear();

motor(0,0,0,0);

if (arr[99]==1 && arr[98] == 1) 
{

arr[100] = 15;
arr[0] = 2; 
arr[50]= 3;
arr[1] = 2;
arr[51]= 2;
arr[2] = 1;
arr[52]= 1;
arr[3] = 2; 
arr[53]= 2;
arr[4] =3;
arr[54]= 2;
arr[5] = 1;
arr[55]= 1;
arr[6] = 3;
arr[56]= 2;
arr[7] = 3;
arr[57]= 1;
arr[8] = 1; 
arr[58]= 1;
arr[9] = 4; 
arr[59]= 3;
arr[10] = 4; 
arr[60]= 3;
arr[11] = 4; 
arr[61]= 3;
arr[12] = 4; 
arr[62]= 3;      
}

else if(arr[99]==1 && arr[98]==2)
{
arr[100] = 10;
arr[0] = 1; 
arr[50]= 1;
arr[1] = 3; 
arr[51]= 2;
arr[2] = 3;
arr[52]= 1;
arr[3] = 1;
arr[53]= 1;
arr[4] = 4;
arr[54]= 3;
arr[5] = 4;
arr[55]= 1;

} 

else if(arr[99]==1 && arr[98]==3)
{ 
arr[100] = 10;
arr[0] = 3; 
arr[50]= 3;
arr[1] = 3; 
arr[51]= 3;
arr[2] = 4;
arr[52]= 2;
arr[3] = 4;
arr[53]= 2;
}  

else if(arr[99]==1 && arr[98]==4)
{ 
arr[100] = 10;
arr[0] = 1; 
arr[50]= 1;
arr[1] = 3; 
arr[51]= 1;
arr[2] = 3;
arr[52]= 1;
arr[3] = 3;
arr[53]= 1;
arr[4] = 4;
arr[54]= 3;
arr[5] = 4;
arr[55]= 1;
} 

else if(arr[99]==2 && arr[98]==1)
{
arr[100] = 15;
arr[0] = 2; 
arr[50]= 3;
arr[1] = 1;
arr[51]= 1;
arr[2] = 2;
arr[52]= 2;
arr[3] = 1; 
arr[53]= 1;
arr[4] = 3;
arr[54]= 1;
arr[5] = 2;
arr[55]= 2;
arr[6] = 3;
arr[56]= 1;
arr[7] = 3;
arr[57]= 2;
arr[8] = 2;
arr[58]= 2;
arr[9] = 4;
arr[58]= 3;
arr[10] = 4;
arr[58]= 3;
} 

else if(arr[99]==2 && arr[98]==2)
{
arr[100] = 10;
arr[0] = 2; 
arr[50]= 2;
arr[1] = 3; 
arr[51]=1;
arr[2] = 3; 
arr[52]= 2;
arr[3] = 2;
arr[53]= 2;
arr[4] = 4;
arr[54]= 3;
arr[5] = 4;
arr[55]= 3;
arr[6] = 4;
arr[56]= 3;

} 

else if(arr[99]==2 && arr[98]==3)
{
arr[100] = 5;
arr[0] = 4; 
arr[50]= 3;
arr[1] = 4; 
arr[51]= 3;
arr[2] = 1; 
arr[52]= 1;
arr[3] = 0;
arr[53]= 1;

} 

else if(arr[99]==2 && arr[98]==4)
{
arr[100] = 10;
arr[0] = 2; 
arr[50]= 2;
arr[1] = 3; 
arr[51]= 2;
arr[2] = 3; 
arr[52]= 2;
arr[3] = 3;
arr[53]= 2;
arr[4] = 2;
arr[54]= 2;
arr[5] = 4;
arr[55]= 3;

} 

while(kondisiZ < 2)
{ 
if(countZ >= 4)
{kondisiZ =1;}     
if (kondisiZ == 1)

if((read_adc(3))>(ADC3)&& (read_adc(4))>(ADC4) )
{
motor(0,0,0,0);delay_ms(100);
kondisiZ ++; 
}
trace_garis();
} 

cnt=0;
while(cnt<arr[100])
{   
kondisi=0;       

if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4))
{ 
kondisi = 3;
}
else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
{ 
kondisi = 3;
} 
else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
{ 
kondisi = 3;
}  
else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(4)>(ADC4))
{ 
kondisi = 3;
}  
else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3))
{ 
kondisi = 3;
}
else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3))
{ 
kondisi = 3;
}  
else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5) && read_adc(6)>(ADC6))
{ 
kondisi = 3;
}
else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
{ 
kondisi = 3;
}  
else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(6)>(ADC6))
{ 
kondisi = 3;
}  
else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
{ 
kondisi = 3;
}  
else if(read_adc(4)>(ADC4) && read_adc(6)>(ADC6))
{ 
kondisi = 3;
} 
else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
{ 
kondisi = 3;
} 
else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
{ 
kondisi = 3;
}    
else if(read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
{ 
kondisi = 3;
} 
else if(read_adc(1)>(ADC1)&& read_adc(6)>(ADC6)) 
{ 
kondisi = 3;
}
else if(read_adc(2)>(ADC2) && read_adc(4)>(ADC4))
{ 
kondisi = 3;
} 
else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5))
{ 
kondisi = 3;
} 
else if(read_adc(3)>(ADC3) && read_adc(6)>(ADC6))
{ 
kondisi = 3;
} 
else if(read_adc(1)>(ADC1) && read_adc(4)>(ADC4))
{ 
kondisi =3;
}
else if(read_adc(2)>(ADC2) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
{ 
kondisi = 3;
}
else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5))
{ 
kondisi = 3;
}
else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
{ 
kondisi = 3;
}  
else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3)&& read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
{ 
kondisi = 3;
}

if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3) && read_adc(4)>(ADC4))
{ 
kondisi = 2;
}
else if(read_adc(2)>(ADC2) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
{ 
kondisi = 2;
} 
else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3)&& read_adc(4)>(ADC4))
{ 
kondisi = 2;
}  
else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(4)>(ADC4))
{ 
kondisi = 2;
}  
else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(3)>(ADC3))
{ 
kondisi = 2;
}
else if(read_adc(1)>(ADC1) && read_adc(2)>(ADC2)&& read_adc(5)>(ADC5))
{ 
kondisi = 2;
}
else if(read_adc(1)>(ADC1) && read_adc(3)>(ADC3))
{ 
kondisi = 2;
}
else if(read_adc(2)>(ADC2) && read_adc(4)>(ADC4))
{ 
kondisi = 2;
} 
else if(read_adc(1)>(ADC1) && read_adc(4)>(ADC4))
{ 
kondisi = 2;
}

else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5) && read_adc(6)>(ADC6))
{ 
kondisi = 1;
}          
else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(5)>(ADC5))
{ 
kondisi = 1;
}  
else if(read_adc(3)>(ADC3) && read_adc(4)>(ADC4)&& read_adc(6)>(ADC6))
{ 
kondisi = 1;
}  
else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
{ 
kondisi = 1;
} 
else if(read_adc(2)>(ADC2) && read_adc(5)>(ADC5)&& read_adc(6)>(ADC6))
{ 
kondisi = 1;
}  
else if(read_adc(4)>(ADC4) && read_adc(6)>(ADC6))
{ 
kondisi = 1;
}
else if(read_adc(3)>(ADC3) && read_adc(5)>(ADC5))
{ 
kondisi = 1;
}  
else if(read_adc(3)>(ADC3) && read_adc(6)>(ADC6))
{ 
kondisi = 1;
}

if (kondisi == arr[cnt])
{         

if (arr[50+cnt] == 1)
{
while((read_adc(7))<(ADC7)) 
{motor(0,0,20,20);} 
while(read_adc(5)<(ADC5)&& read_adc(6)<(ADC6))     
{motor(1,0,60,30);}
motor(0,0,0,0);delay_ms(30);
} 

else if(arr[50+cnt] == 2)    
{     while((read_adc(0))<(ADC0)) 
{motor(0,0,20,20);} 
while(read_adc(1)<(ADC1)&&read_adc(2)<(ADC2))     
{motor(0,1,60,30);}
motor(0,0,0,0);delay_ms(30);   
}
else if(arr[50+cnt] == 3)    
{    motor(0,0,10,10);delay_ms(100) ;  
}    
cnt++;     

}  
else
{  
trace_garis();
}         
lcd_gotoxy(9,0);
sprintf(buf,"%d  ",cnt); 
lcd_puts(buf);         

}

}
